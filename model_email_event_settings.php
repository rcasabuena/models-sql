<?php

class email_event_settings extends model_base_class
{
	# Class properties
	public $email_event_settings_log_id;
	public $email_event_settings_change_date;
	public $email_event_settings_solution;
	public $email_event_settings_api_identifier;
	public $email_event_settings_event_instiller;
	public $email_event_settings_event_kingpin;
	public $company_users_id;
	public $company_id;			

	/**
	 * This function will save new settings
	 *
	 * @return unknown
	 */
	function save_email_event_settings() {
		
		# Load the properties from the form submission
		$this->load_from_buffer($this->variables);

		if ($this->email_event_settings_solution == 'Kingpin') $this->email_event_settings_api_identifier = $this->email_event_settings_event_kingpin;
		if ($this->email_event_settings_solution == 'Instiller') $this->email_event_settings_api_identifier = $this->email_event_settings_event_instiller;

		# Add in the new record
		$sql = " INSERT INTO email_event_settings_log (company_users_id, company_id, " .
		       " email_event_settings_solution, email_event_settings_api_identifier, email_event_settings_change_date) VALUES (" .
		       $this->quote($this->company_users_id) . " , " . $this->quote($this->company_id) . " , " . $this->quote($this->email_event_settings_solution) .  " , " . $this->quote($this->email_event_settings_api_identifier) . ", NOW())";
		
		return $this->execsql($sql);
	}

	/**
	 * This function will return details of the current settings
	 */
	function get_current_settings() {
		# Query the database for the record
		$sql = "SELECT * FROM email_event_settings_log " . 
			   "ORDER BY email_event_settings_change_date DESC LIMIT 1 ";

		# check to make sure a record could be found
		$records = $this->execsql($sql);
		
		if ($record = $records->fetchRow()) {
			return $record;
		}
	}

	/**
	 * Function to get events for instiller and kingpin test
	 *
	 * @return unknown
	 */
	function get_events() {

		if (!isset($_SESSION['agencies']) || !$_SESSION['agencies']) {			
			$_SESSION['agencies'] = $this->get_agencies();
		}

		# Initialise the results
		$results = array();

		# Loop through all of the agencies
		foreach ($_SESSION['agencies'] as $agency) {

			if(trim($agency['agency_name']) == 'Kingpin' || trim($agency['agency_name']) == 'Instiller' ) {

				if (trim($agency['agency_name']) == 'Kingpin') $company_id = 152;
				if (trim($agency['agency_name']) == 'Instiller') $company_id = 45;

				$sql =  " SELECT site_detective_tracking_events_identifier, " . 
						" site_detective_tracking_events_description " .
						" FROM site_detective_tracking_events " . 
						" WHERE company_id = " . $this->quote($company_id) .
						" AND site_detective_tracking_events_setup_id IS NULL";

				# Ensure we have a connection to the right DB server 
				$connection = $this->select_db_connection($agency, true);
				$connection->SetFetchMode(ADODB_FETCH_ASSOC);

				# Load the results from this database
				$run_sql = str_replace('CONTROL_PANEL_URL',$agency['agency_control_panel_url'], $sql);
				$records = $connection->execute($run_sql);

				if($records === false) {
					neteffekt_error_handler('SQL Error',$connection->errorMsg());
				}

				while (($record = $records->FetchRow()) !== false) {
					# Store only if api id is set
					if(isset($record['site_detective_tracking_events_identifier']) && $record['site_detective_tracking_events_identifier'] != '') $results[$agency['agency_name']][] = $record;
				}
			}
		}
		return $results;
	}

	/**
	 * Function to get the up-to-date api keys before triggering the event
	 *
	 * @return unknown
	 */
	function get_instiller_api_keys($solution = 'Instiller') {

		$_SESSION['agencies'] = $this->get_agencies();

		# Initialise the results
		$rest_details = array();

		# Loop through all of the agencies
		foreach ($_SESSION['agencies'] as $agency) {

			if(trim($agency['agency_name']) == $solution) {

				if (trim($agency['agency_name']) == 'Kingpin') $company_id = 152;
				if (trim($agency['agency_name']) == 'Instiller') $company_id = 45;

				$sql = "SELECT * FROM rest_api_key WHERE company_id = " . $company_id . " LIMIT 1";

				# Ensure we have a connection to the right DB server 
				$connection = $this->select_db_connection($agency, true);
				$connection->SetFetchMode(ADODB_FETCH_ASSOC);

				# Load the results from this database
				$run_sql = str_replace('CONTROL_PANEL_URL',$agency['agency_control_panel_url'], $sql);
				$records = $connection->execute($run_sql);

				if($records === false) {
					neteffekt_error_handler('SQL Error',$connection->errorMsg());
				}

				$record = $records->FetchRow();

				$rest_details['rest_api_id'] = $record['rest_api_id'];
				$rest_details['rest_api_key'] = $record['rest_api_key'];
				$rest_details['agency_control_panel_url'] = $agency['agency_control_panel_url'];
			}
		}
		return $rest_details;
	}

	

}

?>