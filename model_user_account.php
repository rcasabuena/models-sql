<?php

class user_account extends model_base_class
{
	# Class properties
	public $company_users_id;
	public $company_id;
	public $company_users_email;
	public $company_users_password;
	public $company_users_name;
	public $company_users_type;
	public $company_users_manage_settings;
	public $company_users_manage_users;
	public $company_users_access;	
	public $company_users_row_count;
	public $login_time;

	
	/**
	 * Class constructor
	 *
	 */
	public function __construct( ) {	
		$this->company_users_row_count = 20;
	}
	
	/**
	 * This function should be used throughout the system to get hold of the current
	 * company ID, if this is a client the ID will be for the client otherwise it will
	 * be the ID for the agency or the company ther are editing
	 *
	 */
	function get_company_id()
	{
		return $this->company_id;	
	}

	/**
	 * This function will check the login parameters the user has provided and will initialise the
	 * the internal properties of the object
	 *
	 * @return unknown
	 */
	function check_login($auto_login = false) {
		
		if ($auto_login == true) {
			return $this->load_user(4); // support@neteffekt.co.uk
		} else {
			# Query the database for the user record
			$sql = "SELECT a.company_users_id FROM " .
				   "company_user a, company b WHERE " .
				   "a.company_id = b.company_id AND " .
				   "a.company_users_email = " . $this->quote($_POST['company_users_email']) . " AND " .
				   "a.company_users_password = " . $this->quote($_POST['company_users_password']);

			# Check to make sure a record could be found
			$records = $this->exec_eyetap_sql($sql);
			
			if (($record = $records->fetchRow()) !== false) {
	
				return $this->load_user($record['company_users_id']);
			} else {				
				# No user account could be found
				return false;
			}
		}
	}

	/**
	 * This function will list all of the user accounts for the current company
	 * based on the users search criteria.
	 *
	 * @return unknown
	 */
	function login_search($smart_filter_location, $login_type = '') {
		# Build the query to search through the user accounts
		$sql = '';

		if ($login_type == 'agency') {
			$sql = " SELECT * FROM company_user WHERE company_id = " . $_SESSION['user_account']->company_id;
		} else {
			$sql = " SELECT * FROM company_user WHERE company_id = " . $_SESSION['user_account']->get_company_id();
		}

		# Check to see if there are any smart filter criteria we need to apply
		if (isset($smart_filter_location) && is_object($smart_filter_location)) {
			# We have got a smart filter location we can see if we can apply the options from the user
			# Start with checking the brand name
			if (isset($smart_filter_location->controls['filter_keyword_search']->value) &&
			trim($smart_filter_location->controls['filter_keyword_search']->value) != '') {
				# We need to filter using the brand name
				$sql .= " AND (company_users_email LIKE '%" . addslashes($smart_filter_location->controls['filter_keyword_search']->value) . "%' OR ";
				$sql .= "company_users_name LIKE '%" . addslashes($smart_filter_location->controls['filter_keyword_search']->value) . "%')";
			}
		}

		# Order the results by email
		$sql .=  " ORDER BY company_users_email ";



		# Return the constructed SQL
		return $sql;
	}

	/**
	 * This function will list all of the user accounts for the current company
	 * based on the users search criteria.
	 *
	 * @return unknown
	 */
	function get_logins($login_type='') {
		# Build the query to search through the user accounts
		$sql = '';

		if ($login_type == 'agency') {
			$sql = " SELECT * FROM company_user WHERE company_id = " . $_SESSION['user_account']->company_id;
		} else {
			$sql = " SELECT * FROM company_user WHERE company_id = " . $_SESSION['user_account']->get_company_id();
		}

		# Check to see if there are any smart filter criteria we need to apply
		/*if (isset($smart_filter_location) && is_object($smart_filter_location)) {
			# We have got a smart filter location we can see if we can apply the options from the user
			# Start with checking the brand name
			if (isset($smart_filter_location->controls['filter_keyword_search']->value) &&
			trim($smart_filter_location->controls['filter_keyword_search']->value) != '') {
				# We need to filter using the brand name
				$sql .= " AND (company_users_email LIKE '%" . addslashes($smart_filter_location->controls['filter_keyword_search']->value) . "%' OR ";
				$sql .= "company_users_name LIKE '%" . addslashes($smart_filter_location->controls['filter_keyword_search']->value) . "%')";
			}
		}*/

		# Order the results by email
		$sql .=  " ORDER BY company_users_email ";
		$results = $this->exec_eyetap_sql( $sql );

		$logins = array();

		while (($result = $results->FetchRow()) !== false) {
			$logins[] = $result;
		}

		# Return logins
		return $logins;
	}

	/**
	 * This function will load the properties of the selected user account into the object
	 * the company ID is verified agains the currently logged in company
	 *
	 * @param int $company_users_id
	 * @return boolean
	 */
	function load_user($company_users_id) {
		# Handle creating a shell for new accounts
		if ($company_users_id == 'new') {
			return true;
		}

		# Query the database for the user record
		$sql = "SELECT * FROM " .
			   "company_user a, company b WHERE " .
			   "a.company_id = b.company_id AND " .
			   "a.company_users_id = " . $company_users_id;

		# check to make sure a record could be found
		$records = $this->exec_eyetap_sql($sql);
		if (($record = $records->fetchRow()) !== false) {
	
			# Load the properties of the object
			$this->login_time = date('d/m/Y @ H:i:s');	
			
			$this->load_from_buffer($record);
			
			return true;
		} else {
			return false;
		}
			
		
	}

	/**
	 * This function will load the buffer from from the form submission and then update the
	 * database, a check will be made to ensure the email address is unique
	 *
	 * @return unknown
	 */
	function save_login($variables) {

		# Load the properties from the form submission
		$this->load_from_buffer($variables);

		# Check for unique email
		if ($this->company_users_id == 'new') {
			$check_id = -1;
		} else {
			$check_id = $this->company_users_id;
		}

		$sql = "SELECT COUNT(*) AS counter FROM " .
			   "company_user WHERE " .
			   "company_users_email = " . $this->quote($this->company_users_email) . " AND " .
			   "company_users_id <> " . $check_id;
		$records = $this->exec_eyetap_sql($sql);

		$record = $records->FetchRow();
		if ($record['counter'] > 0) {
			$this->error_message = 'The email address you have entered is already in use';
			return false;
		}

		if (isset($variables['login_type']) && $variables['login_type'] == 'agency') {
			$company_users_type = 'ADMIN';
			$company_id = $_SESSION['user_account']->company_id;
		} else {
			$company_users_type = 'USER';
			$company_id = $_SESSION['user_account']->get_company_id();
		}

		$this->company_users_manage_settings = ($this->company_users_manage_settings != '' ? $this->company_users_manage_settings : 'N');
		$this->company_users_manage_users = ($this->company_users_manage_users != '' ? $this->company_users_manage_users : 'N');

		# Check to see if we are creating or updating
		if ($check_id == -1) {
			# Add in the new user account record
			$sql = "INSERT INTO company_user (" .
				   "company_users_name,company_users_password," .
				   "company_users_email,company_id,company_users_type," .
				   "company_users_manage_settings,company_users_manage_users," .
				   "company_users_access) VALUES (" .
			       $this->quote($this->company_users_name) . "," . $this->quote($this->company_users_password) . "," .
			       $this->quote($this->company_users_email) . "," . $this->quote($company_id) . "," .
			       $this->quote($company_users_type) . "," . $this->quote($this->company_users_manage_users) . "," . 
			       $this->quote($this->company_users_manage_settings) . "," . $this->quote($this->company_users_access) . ")";
			$this->exec_eyetap_sql($sql);

			#$this->company_users_id = $GLOBALS['DB']->Insert_Id( );

			return true;
		} else {
			# update the existing user account
			$sql = " UPDATE company_user SET " .
			       " company_users_email = " . $this->quote($this->company_users_email) . "," .
				   " company_users_name = " . $this->quote($this->company_users_name) . "," .
			       " company_users_password = " . $this->quote($this->company_users_password) . "," .
			       " company_users_manage_settings = " . $this->quote($this->company_users_manage_settings) . "," .
			       " company_users_manage_users = " . $this->quote($this->company_users_manage_users) . "," .
			       " company_users_access = " . $this->quote($this->company_users_access) .
			       " WHERE company_users_id = " . $this->company_users_id;
			$this->exec_eyetap_sql($sql);

			return true;
		}
	}

	/**
	 * This function will removed the specified user ID from the database, also link
	 * in the company id to avoid url hacking
	 *
	 */
	function delete_login($company_users_id = '') {

		if (!isset($company_users_id) && $company_users_id == '') return false;
		else {
			# Remove any event notification links
			#$this->exec_eyetap_sql("DELETE FROM site_detective_tracking_events_notifications WHERE " .
			#		       "company_users_id = " . $company_users_id);

			# Remove any system notifications
			#$this->exec_eyetap_sql("DELETE FROM company_user_notifications WHERE " .
			#		       "company_users_id = " . $company_users_id);
					       
			# Remove the login
			$this->exec_eyetap_sql("DELETE FROM company_user WHERE " .
				   		   "company_users_id = " . $company_users_id);

			return true;
		}
	}
	
	public function send_login_details( $company_id )
	{
		
		$result = true;
		
		// Check to make sure that we've got a user id to use
		if ( ( !isset( $this->company_users_id ) ) ||
		     ( !is_numeric( $this->company_users_id ) ) )
        {
        	
        	$this->error_message = 'No valid user to work with!';
        	$this->error_icon = 'ERROR';
        	$result = false;
        	
        }
        else 
        {
		
			// Reload the details to make sure they're in the system.
			$this->load_user( $this->company_users_id );
			
			// Get the e-mail details for this company
			$sql = ' SELECT company_email_smtp_server, '
			     . ' company_email_from_address, '
			     . ' company_email_from_name, '
			     . ' company_application_name '
			     . ' FROM company'
			     . ' WHERE company_id =' . $this->quote( $company_id);
            $results = $this->exec_eyetap_sql( $sql );
            
            if ( $results->rowCount( ) > 0 )
            {
            	
            	$record = $results->fetchRow( );
			
				// Load up the PHPMailer library 
				load_library('phpmailer');
	            $mail           = new PHPMailer();
	            $mail->Host     = $record['company_email_smtp_server'];
	            $mail->From     = $record['company_email_from_address'];
	            $mail->FromName = $record['company_email_from_name'];
	            $mail->isHTML( true );
	            $mail->IsSMTP();
	
                // Setup the subject for the email message
                if ($record['company_application_name'] != '') {
                	$mail->Subject  = translate( 'Your ' . $record['company_application_name'] . ' login details' );
                } else {                
                	$mail->Subject  = translate( 'Your login details' );
                }
                
                $body_text = '<p>' . translate( 'Hi there,' ) . '</p>'
                           . '<p>' . translate( 'Here are your account details for ' ) 
                           . $record['company_application_name'] . ':</p>' 
                           . '<p>' . translate( 'Email Address: ' ) . $this->company_users_email . '<br />'
                           . translate( 'Password: ' ) . $this->company_users_password . '</p>'
                           . '<p>' . translate( 'We advise you to change your '
                                      . 'password when you login to the system.' ) . '</p>'
                           . '<p>' . translate( 'To login now, please go to ' )
                           . '<a href="http://' . $_SERVER['HTTP_HOST'] . '">http://'
                           . $_SERVER['HTTP_HOST'] . '</a></p>'
                           . '<p>' . translate( 'Thanks, ') . '</p>'
                           . '<p>' . translate( 'The ' ) . $record['company_application_name'] . ' '
                           . translate( 'Team' ) . '</p>';
                
                $mail->Body = $body_text;
                                
                // Add the login to the mail
	            $mail->AddAddress( $this->company_users_email, $this->company_users_name );
	            
	            // Attempt to send it
                if ( !$mail->Send( ) ) 
                {
                    $this->error_message = $mail->ErrorInfo;
                    $result = false;
                }

            }
            else 
            {

            	$this->error_message = translate( 'Could not load the company e-mail details' );
            	$this->error_icon = 'ERROR';
            	$result = false;
            	
            }
            
        }
        
        return $result;
		
	}


	/**
	 * This function will return a list of users in an array
	 *
	 * @return array
	 */
	function get_user_list() {
		$users = array();

		$records = $this->exec_eyetap_sql($this->login_search(''));

		# Add all the users to the array
		while ($record = $records->fetchrow()) {
			$users[] = array('company_users_id' => $record['company_users_id'],
							 'company_users_email' => $record['company_users_email']);
		}


		# Return the users
		return $users;
	}


	/**
	 * This function loads the account info relating to this user
	 *
	 */
	function load_account_info() {
		$records = $this->exec_eyetap_sql("SELECT * FROM company WHERE " .
								  "company_id = " . $_SESSION['user_account']->get_company_id());

		if ($record = $records->fetchrow()) {
			$this->load_from_buffer($record);
		} else {
			return false;
		}
	}
	

	/**
	 * This function sends a password reminder to allow
	 * users that have forgotten to login
	 *
	 */
	function send_password_reminder() 
	{
		# Check the client is known by the system
		$sql = "select * from company_user where company_users_email = " . $this->quote($this->variables['company_users_email_forgotten']);
		$records = $this->exec_eyetap_sql($sql); 
		if ($record = $records->FetchRow()) {
			# Send the password to the client
			load_library('phpmailer');
			$mail           = new PHPMailer();
			$mail->Host     = 'localhost';
			$mail->FromName = 'Eyetap	';
			$mail->From = 'eyetap@neteffekt.co.uk';
			$mail->IsSMTP(true);
	    	$mail->isHTML( true );			
			$mail->Subject  = 'Your eyetap password reminder';
			$mail->AddAddress($this->variables['company_users_email_forgotten'], $this->variables['company_users_email_forgotten']);

      		$body_text = '<p>' . translate( 'Hi there,' ) . '</p>'
                       . '<p>' . translate( 'Here are your account details for eyetap' ) . ':</p>' 
                       . '<p>' . translate( 'Email Address: ' ) . $this->variables['company_users_email_forgotten'] . '<br />'
                       . translate( 'Password: ' ) . $record['company_users_password'] . '</p>'
                       . '<p>' . translate( 'We advise you to change your '
                                  . 'password when you login to the system.' ) . '</p>'
                       . '<p>' . translate( 'To login now, please go to ' )
                       . '<a href="http://' . $_SERVER['HTTP_HOST'] . '">http://'
                       . $_SERVER['HTTP_HOST'] . '</a></p>'
                       . '<p>' . translate( 'Thanks, ') . '</p>'
                       . '<p>' . translate( 'The eyetap Team' ) . '</p>';
            
    	    $mail->Body = $body_text;
			$mail->Send();
			return true;
		} else {
			return false;
		
		}
	}
}

?>