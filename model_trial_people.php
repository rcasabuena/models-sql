<?php

class trial_people extends model_base_class
{
	# Class properties
	public $trial_people_id;
	public $trial_client_id;
	public $trial_people_name;
	public $trial_people_email;
	public $trial_people_company;
	public $trial_people_campaign_event_triggered;


	/**
	 * This function will list all of the users for a trial client
	 *
	 * @return unknown
	 */
	function trial_people_search($trial_client_id) {
		# Build the query to search
		$sql = " SELECT * FROM trial_people WHERE trial_client_id = " . $trial_client_id . " ORDER BY trial_people_email ";
		# Return the constructed SQL
		return $sql;
	}

	/**
	 * This function will list all of the trial contacts
	 *
	 *
	 * @return unknown
	 */
	function get_all_trial_people($trial_client_id) {
		# Build the query to search
		$sql = " SELECT * FROM trial_people WHERE trial_client_id = " . $trial_client_id . " ORDER BY trial_people_name";
		
		$records = $this->execsql($sql);

		# Initialise profile list
		$trial_people_list = array();

		while (($record = $records->FetchRow()) !== false) {

			# Add trial client records to array
			$trial_people_list[] = $record;
		}

		# Return profile list
		return $trial_people_list;
	}

	/**
	 * This function will load the properties of the selected user into the object
	 *
	 * @param int $trial_people_id
	 * @return boolean
	 */
	function load_trial_people($trial_people_id) {
		# Handle creating a shell for new records
		if ($trial_people_id == 'new') {
			return true;
		}

		# Query the database for the record
		$sql = "SELECT * FROM trial_people " .
		       "WHERE trial_people_id = " . $trial_people_id;

		# check to make sure a record could be found
		$records = $this->execsql($sql);

		if (($record = $records->fetchRow()) !== false) {
			# Load the properties of the object
			$this->load_from_buffer($record);
			return true;
		}
	}

	/**
	 * This function will load the buffer from the form submission and then update the
	 * database, a check will be made to ensure the data is unique
	 *
	 * @return unknown
	 */
	function save_trial_people()
	{
		# Load the properties from the form submission
		$this->load_from_buffer($this->variables);

		# Check for unique email
		if ($this->trial_people_id == 'new') {
			$check_id = -1;
		} else {
			$check_id = $this->trial_people_id;
		}

		$sql = " SELECT COUNT(*) AS counter FROM trial_people " .
			   " WHERE trial_people_email = " . $this->quote($this->trial_people_email) . " AND " .
			   " trial_people_id <> " . $check_id;
		
		$records = $this->execsql($sql);
		$record = $records->FetchRow();

		if ($record['counter'] > 0) {
			$this->error_message = 'User email is already in use, please enter a unique user';
			return false;
		}

		if (isset($this->trial_people_company) && $this->trial_people_company != '') {
			$sql = 	" SELECT COUNT(*) AS counter FROM trial_clients " .
			   		" WHERE trial_client_name = " . $this->quote($this->trial_people_company);

			$records = $this->execsql($sql);
			$record = $records->FetchRow();
			
			if ($record['counter'] < 1) {
				# Insert new client record
				$sql = " INSERT INTO trial_clients (trial_client_name, " .
				       " trial_client_start_date, trial_client_end_date) VALUES (" .
				       $this->quote($this->trial_people_company) . ", NOW(), NOW() + INTERVAL 30 DAY)";
				$this->execsql($sql); 
				# Get trial id of last db inset
				$trial_client_id = $this->insert_id();
			} else {
				# Get trial_client_id
				$sql = " SELECT trial_client_id FROM trial_clients WHERE trial_client_name = " . $this->quote($this->trial_people_company) . " LIMIT 1";
				$record = $this->execsql($sql);
				$trial_client_id =  $record->FetchRow();
			}
		} else {
			$trial_client_id = $this->trial_client_id;
		}
		

		# Check to see if we are creating or updating
		if ($check_id == -1) {

			# Add in the new record
			$sql = " INSERT INTO trial_people (trial_client_id, " .
			       " trial_people_name, trial_people_email, trial_people_campaign_event_triggered) VALUES (" .
			       $trial_client_id . " , " . $this->quote($this->trial_people_name) . " , " . $this->quote($this->trial_people_email). ", 'N' )";

			$this->execsql($sql);
			
		} else {
			# update the existing user
			$sql = "UPDATE trial_people SET " .
				   "trial_people_name = " . $this->quote($this->trial_people_name) . ", " .
				   "trial_people_email = " . $this->quote($this->trial_people_email) . 
			       " WHERE trial_people_id = " . $check_id;
			$this->execsql($sql);
		}

		$sql =  " SELECT * FROM trial_people " .
				" WHERE trial_people_email=" . $this->quote($this->trial_people_email) .
				" AND trial_client_id=" . $trial_client_id .
				" LIMIT 1";

		$records = $this->execsql($sql);
		$record = $records->FetchRow();

		if (isset($this->trial_people_campaign_event_triggered) && $this->trial_people_campaign_event_triggered == 'Y') {

			$settings = load_model('email_event_settings');
			$email_event_settings = $settings->get_current_settings();

			# Trigger the event to send email to user
			$rest_details = $settings->get_instiller_api_keys($email_event_settings['email_event_settings_solution']);

			# Define the fields required for the rest request
			$fields = array();
			$fields['api_id'] = $rest_details['rest_api_id'];
			$fields['api_key'] = $rest_details['rest_api_key'];
			$fields['event_api_identifier'] = $email_event_settings['email_event_settings_api_identifier'];
			$fields['email_address'] = $record['trial_people_email'];
			$url = $rest_details['profile_control_panel_url'] . "/rest/users/trigger_event";

			# Make the request to the API
			$results = $settings->call_API('GET',$url, $fields);

			if ($results['valid'] === true) {
				# Update the database to show the the event campaign has been triggered
				$sql = "UPDATE trial_people SET trial_people_campaign_event_triggered = 'Y' WHERE trial_people_id =" . $record['trial_people_id'];
				$this->execsql($sql);
			} else {
				if (isset($results['response']['reason']) && $results['response']['reason'] != '') {
					$this->error_message = $results['response']['reason'];
				} else {
					$this->error_message = 'Email event has not been triggered because of unknown reason, check user contact details.';
				}
				return false;
			}
		}

		return $trial_client_id;
	}
	/**
	 * Function to delete a user after it has been loaded from the buffer
	 *
	 * @return unknown
	 */
	function delete_trial_people()
	{
		# Double-check the correct information has been assigned to profile_id
		if($this->trial_people_id == 'new' || trim($this->trial_people_id == '') || !isset($this->trial_people_id)) {
			return false;
		}
		
		# Delete the client...
		$sql = " DELETE FROM trial_people WHERE trial_people_id = " . $this->trial_people_id;	
		$this->execsql($sql);
		
		return true;
	}

}

?>