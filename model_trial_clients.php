<?php

class trial_clients extends model_base_class
{
	# Class properties
	public $trial_client_id;
	public $trial_client_name;
	public $trial_client_start_date;
	public $trial_client_end_date;
	public $trial_client_notes;
	public $trial_status;

	/**
	 * This function will list all of the configured trial clients
	 *
	 * @return unknown
	 */
	function trial_clients_search($smart_filter_location) {
		# Build the query to search
		$sql = " SELECT * FROM trial_clients ORDER BY trial_client_name ";
		# Return the constructed SQL
		return $sql;
	}

	/**
	 * This function will list all of the trial clients
	 *
	 *
	 * @return unknown
	 */
	function get_trial_clients_info() {
		# Build the query to search
		$sql_clients_list = " SELECT * FROM trial_clients ORDER BY trial_client_end_date ASC ";
		
		$records = $this->execsql($sql_clients_list);

		# Initialise profile list
		$data = array();

		while (($record = $records->FetchRow()) !== false) {

			# Check trial status
			# Check if trial status is active
			if ($record['trial_client_end_date'] > date('Y-m-d H:i:s') && $record['trial_client_end_date'] > date('Y-m-d H:i:s', strtotime('+3 days'))) $record['trial_status'] = 'ok';
			# Warn if trial only have 3 days or less left
			if ($record['trial_client_end_date'] > date('Y-m-d H:i:s') && $record['trial_client_end_date'] < date('Y-m-d H:i:s', strtotime('+3 days'))) $record['trial_status'] = 'warning';
			# Trial has now expired
			if ($record['trial_client_end_date'] < date('Y-m-d H:i:s')) $record['trial_status'] = 'critical';
			
			# Add trial client records to array
			$data['trial_clients'][] = $record;
			$data['autocomplete_list'][] = $record['trial_client_name'];
		}

		# Build the query to search
		$sql_people_list = " SELECT trial_people.*, trial_clients.trial_client_name FROM trial_people INNER JOIN trial_clients ON trial_people.trial_client_id=trial_clients.trial_client_id ";
		
		$records = $this->execsql($sql_people_list);

		while (($record = $records->FetchRow()) !== false) {

			# Add trial client records to array
			$data['trial_people'][] = $record;
		}
		# Return profile list
		return $data;
	}

	/**
	 * This function will load the properties of the selected trial client into the object
	 *
	 * @param int $profile_id
	 * @return boolean
	 */
	function load_trial_client($trial_client_id) {
		# Handle creating a shell for new records
		if ($trial_client_id == 'new') {
			return true;
		}

		# Query the database for the record
		$sql = "SELECT * FROM trial_clients " .
		       "WHERE trial_client_id = " . $trial_client_id;

		# check to make sure a record could be found
		$records = $this->execsql($sql);

		if (($record = $records->fetchRow()) !== false) {

			# Check trial status
			# Check if trial status is active
			if ($record['trial_client_end_date'] > date('Y-m-d H:i:s') && $record['trial_client_end_date'] > date('Y-m-d H:i:s', strtotime('+3 days'))) $record['trial_status'] = 'ok';
			# Warn if trial only have 3 days or less left
			if ($record['trial_client_end_date'] > date('Y-m-d H:i:s') && $record['trial_client_end_date'] < date('Y-m-d H:i:s', strtotime('+3 days'))) $record['trial_status'] = 'warning';
			# Trial has now expired
			if ($record['trial_client_end_date'] < date('Y-m-d H:i:s')) $record['trial_status'] = 'critical';

			# Load the properties of the object
			$this->load_from_buffer($record);
			return true;
		}
	}

	/**
	 * This function will load the buffer from the form submission and then update the
	 * database, a check will be made to ensure the data is unique
	 *
	 * @return unknown
	 */
	function save_trial_client()
	{

		# Load the properties from the form submission
		$this->load_from_buffer($this->variables);

		# Check for unique email
		if ($this->trial_client_id == 'new') {
			$check_id = -1;
		} else {
			$check_id = $this->trial_client_id;
		}

		$sql = "SELECT COUNT(*) AS counter FROM trial_clients " .
			   "WHERE trial_client_name = " . $this->quote($this->trial_client_name) . " AND " .
			   "trial_client_id <> " . $check_id;
		
		$records = $this->execsql($sql);
		$record = $records->FetchRow();

		if ($record['counter'] > 0) {
			$this->error_message = 'Client name is already in use, please enter a unique name';
			return false;
		}

		# Check to see if we are creating or updating
		if ($check_id == -1) {
			# Add in the new record
			$sql = "INSERT INTO trial_clients (trial_client_name, " .
			       "trial_client_start_date, trial_client_end_date, trial_client_notes) VALUES (" .
			       $this->quote($this->trial_client_name) . ", " . $this->quote(date("Y-m-d H:i:s",strtotime($this->trial_client_start_date))) . ", " . $this->quote(date("Y-m-d H:i:s",strtotime($this->trial_client_end_date))) . ", " . $this->quote($this->trial_client_notes) . ")";
			$this->execsql($sql);

			$trial_client_id = $this->insert_id();
		} else {
			# update the existing trial client
			$sql = " UPDATE trial_clients SET " .
				   " trial_client_name = " . $this->quote($this->trial_client_name) . "," .
				   " trial_client_start_date = " . $this->quote(date("Y-m-d H:i:s",strtotime($this->trial_client_start_date))) . "," .
				   " trial_client_end_date = " . $this->quote(date("Y-m-d H:i:s",strtotime($this->trial_client_end_date))) . "," .
				   " trial_client_notes = " . $this->quote($this->trial_client_notes) .
			       " WHERE trial_client_id = " . $check_id;
			$this->execsql($sql);
			$trial_client_id = $this->trial_client_id;
		}
		return $trial_client_id;
	}
	/**
	 * Function to delete a trial client after it has been loaded from the buffer
	 *
	 * @return unknown
	 */
	function delete_trial_client()
	{
		# Double-check the correct information has been assigned to trial_client_id
		if($this->trial_client_id == 'new' || trim($this->trial_client_id == '') || !isset($this->trial_client_id)) {
			return false;
		}
		
		# Delete the client...
		$sql = " DELETE FROM trial_clients WHERE trial_client_id = " . $this->trial_client_id;	
		$this->execsql($sql);

		# Also delete contacts associated with 
		$sql = " DELETE FROM trial_people WHERE trial_client_id = " . $this->trial_client_id;	
		$this->execsql($sql);
		
		return true;
	}
}

?>