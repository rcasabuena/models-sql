<?php

class svn_log extends model_base_class
{
	# Class properties
	public $svn_log_id;
	public $svn_log_date;
	public $svn_log_type;
	public $svn_log_comment;
	public $svn_log_tag;			

	/**
	 * This function will list svn log entries
	 *
	 * @return unknown
	 */
	function svn_log_search($smart_filter_location) {
		
		$sql =  " SELECT * FROM svn_log WHERE svn_log_type NOT IN ('VSTALL','VSTALL_TEMPLATE')";
		
		$where_started = false;
		# Check to see if there are any smart filter criteria we need to apply
		if (isset($smart_filter_location) && is_object($smart_filter_location)) {
			if (isset($smart_filter_location->controls['filter_keyword_search']->value) &&
				trim($smart_filter_location->controls['filter_keyword_search']->value) != '' && 
				$smart_filter_location->controls['filter_keyword_search']->value != '') {
				$sql .= " WHERE svn_log_type = " . $this->quote($smart_filter_location->controls['filter_keyword_search']->value);
				$where_started = true;
			}
		}

		$sql .= " ORDER BY svn_log_date DESC ";
		return $sql;
	}

	/**
	 * This function will return details of the current release
	 */
	function get_current_release($svn_log_type) {
		# Query the database for the record
		$sql = "SELECT svn_log_id, svn_log_date, svn_log_tag, " . 
		       "svn_log_comment, svn_log_type FROM " . 
			   "svn_log WHERE svn_log_type NOT IN ('VSTALL','VSTALL_TEMPLATE') " .
			   "ORDER BY svn_log_date DESC LIMIT 0,1 ";

		# check to make sure a record could be found
		$records = $this->exec_eyetap_sql($sql);
		if ($record = $records->fetchRow()) {
			# Load the properties of the object
			$this->load_from_buffer($record);
			return true;
		}
	}

}

?>