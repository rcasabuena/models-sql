<?php

class agencies extends model_base_class
{
	# Class properties
	public $agency_id;
	public $agency_name;
	public $agency_type;	
	public $agency_enabled;
	public $agency_date_created;
	public $agency_date_updated;
	public $agency_database_host;
	public $agency_database_name;
	public $agency_control_panel_url;	
	public $agency_direct_relationship;
	public $agency_freshdesk_custid;
	public $agency_freshdesk_client_name;
	public $agency_freshbooks_clientid;
	public $agency_post_invoices;
	public $agency_freshbooks_client_name;
	public $agency_freshbooks_sendto_list;
	public $agency_billing_plan;
	public $agency_billing_status_active;
	public $agency_contacts_ignored;
	public $agency_status;
	public $agency_notes;
	public $agency_ssl_renewal_date;
	public $agency_ssl_provider;
	public $agency_ssl_notes;
	public $agency_ssl_error;
	public $agency_ssl_managed;
	public $ssl_status;
	public $agency_additional_instances_litmus;
	public $agency_additional_instances_cost;
	public $agency_vat;
	public $agency_litmus_allocation;
	public $agency_pb_details;
	public $agency_additional_transactions;
	public $additional_transactions;
	public $agency_minimum_commitment;
	public $agency_solution_service_fee;
	public $agency_solution_service_fee_discount;
	public $agency_solution_service_fee_growing;
	public $agency_solution_service_fee_unlimited;
	public $agency_logins;
	public $instiller_logins;
	public $agency_contact_id;
	public $agency_contact_email;
	public $agency_contact_name;
	public $agency_contact_campaign_event_triggered;
	public $agency_contact_events_triggered;
	public $agency_contact_date_created;
	public $freshdesk_login_created;
	public $freshdesk_id;
	public $agency_login_created;
	public $agency_contacts_monitoring;
	public $contacts_list_view;
	public $contacts_list_edit;

	/**
	 * This function will list all of the configured agencies
	 *
	 * @return unknown
	 */
	function agency_search($smart_filter_location) {
		# Build the query to search
		$sql = " SELECT * FROM agencies ORDER BY agency_type, agency_name ";
		# Return the constructed SQL
		return $sql;
	}

	/**
	 * 
	 * This function will retrieves all unassigned freshbooks clients
	 *
	 * @return unassigned freshbooks clients
	 */
	function get_all_unassigned_freshbooks_clients($freshbooks_id, $filter = 'unassigned') {

		# Get all freshbooks client
		$client = new FreshBooks_Client();
		
		$client->listing($rows, $resultInfo, 1, 999, array('folder' => 'active'));

		$freshbooks_clientIds = array();

		foreach ($rows as $client) {
			$freshbooks_clientIds[$client->clientId]['clientId'] = $client->clientId;
			$freshbooks_clientIds[$client->clientId]['agency'] = $client->organization;
		}
		
		# sort alphabetically by name
		uasort($freshbooks_clientIds, function($a, $b) {
			return strcasecmp($a['agency'], $b['agency']);
		});

		# If agency has no direct relationship to instiller,
		# return all clients to associate the billing records to a reseller
		if ($filter == 'all') return $freshbooks_clientIds;
		
		# Get all assigned freshbooks ids
		$assigned_freshbooks_clientids = $this->get_all_agency_freshbooks_ids($freshbooks_id);

		# Workout the the unassigned freshbooks client ids
		$unassigned_freshbooks_clientids = array_diff_key($freshbooks_clientIds, $assigned_freshbooks_clientids);

	    return !isset($unassigned_freshbooks_clientids) ? 0 : $unassigned_freshbooks_clientids; 

	}

	/**
	 * 
	 * This function will retrieves all freshbooks client contacts list
	 *
	 * @return freshbooks client contacts list
	 */
	function get_client_freshbooks_contacts($freshbooks_id) {

		# Get all freshbooks client
		$client = new FreshBooks_Client();
		
		$client->get($freshbooks_id);

		$contacts_list = array( '0' => array('contact_id' => '0', 'email' => $client->email, 'name' => $client->firstName . ' ' . $client->lastName, 'selected' => 'N'));

		
		$alt_contacts_decoded = json_decode($client->contacts); 

		if (isset($alt_contacts_decoded->contact)) {
			$alt_contacts = $alt_contacts_decoded->contact;
			if (is_array($alt_contacts)) {
				foreach ($alt_contacts as $contact) {
					$full_name = '';
					if (is_string($contact->first_name)) $full_name .=  $contact->first_name;
					if (is_string($contact->last_name)) $full_name .=  ' ' . $contact->last_name;
					$contacts_list[$contact->contact_id] = array('contact_id' => $contact->contact_id, 'email' => $contact->email, 'name' => $full_name, 'selected' => 'N');	
				}
			} else {
				$full_name = '';
				if (is_string($alt_contacts->first_name)) $full_name .=  $alt_contacts->first_name;
				if (is_string($alt_contacts->last_name)) $full_name .=  ' ' . $alt_contacts->last_name;
				$contacts_list[$alt_contacts->contact_id] = array('contact_id' => $alt_contacts->contact_id, 'email' => $alt_contacts->email, 'name' => $full_name, 'selected' => 'N');
			}
		}

		return $contacts_list;
	}

	/**
	 * 
	 * This function processes micro edit to agency details
	 *
	 * @return result array
	 */
	function handle_micro_edit($agency_id) {

		# Initialise the results of the edit 
		$result = array(); 
		$result['saved'] = true;
		$result['error_message'] = '';
		$result['refresh_required'] = false;

		# Check to see which of the fields is included within the variables
		if (isset($this->variables['agency_name'])) {
			
			# Update the agency name
			$sql = 	" UPDATE agencies SET " .
					" agency_name = " . $this->quote($this->variables['agency_name']) .   
					" WHERE agency_id = " . $this->quote($agency_id);
			$this->execsql($sql);

		}
		
		if (isset($this->variables['agency_type'])) {
			
			# Update the agency type
			$sql = 	" UPDATE agencies SET " .
					" agency_type = " . $this->quote($this->variables['agency_type']) .   
					" WHERE agency_id = " . $this->quote($agency_id);
			$this->execsql($sql);
		}

		if (isset($this->variables['agency_freshdesk_custid'])) {
			
			# Update the agency type
			$sql = 	" UPDATE agencies SET ";
			
			# Check if freshdesk customer id is not set
			if (trim($this->variables['agency_freshdesk_custid']) != '') $sql .= " agency_freshdesk_custid = " . $this->quote($this->variables['agency_freshdesk_custid']);   
			else {
				$sql .= " agency_freshdesk_custid = NULL ";
			}

			$sql .=	" WHERE agency_id = " . $this->quote($agency_id);
			
			$this->execsql($sql);
		}

		if (isset($this->variables['agency_database_host'])) {
			
			# Update the agency database host
			$sql = 	" UPDATE agencies SET " .
					" agency_database_host = " . $this->quote($this->variables['agency_database_host']) .   
					" WHERE agency_id = " . $this->quote($agency_id);
			$this->execsql($sql);
		}
		
		if (isset($this->variables['agency_database_name'])) {
			
			# Update the agency database name
			$sql = 	" UPDATE agencies SET " .
					" agency_database_name = " . $this->quote($this->variables['agency_database_name']) .   
					" WHERE agency_id = " . $this->quote($agency_id);
			$this->execsql($sql);
		}
		
		if (isset($this->variables['agency_control_panel_url'])) {
			
			# Update the agency control panel url
			$sql = 	" UPDATE agencies SET " .
					" agency_control_panel_url = " . $this->quote($this->variables['agency_control_panel_url']) .   
					" WHERE agency_id = " . $this->quote($agency_id);
			$this->execsql($sql);
		}
		
		if (isset($this->variables['field']) && $this->variables['field'] == 'agency_direct_relationship') {
			
			if ($this->variables['value'] == 'Y') {
				$agency_contacts_ignored = 'N';
			} else {
				$agency_contacts_ignored = 'Y';
			}

			# Update the agency direct relationship
			$sql = 	" UPDATE agencies SET " .
					$this->variables['field'] . " = " . $this->quote($this->variables['value']) . " , " .
					" agency_contacts_ignored = " . $this->quote($agency_contacts_ignored) . " , " .
					" agency_freshdesk_custid = NULL " .
					" WHERE agency_id = " . $this->quote($agency_id);
			
			$this->execsql($sql);

			$result['agency_direct_relationship'] = $this->variables['value'];
			$result['agency_contacts_ignored'] = $agency_contacts_ignored;
			$result['agency_in_freshdesk'] = 'N';
		}

		if (isset($this->variables['field']) && $this->variables['field'] == 'agency_contacts_ignored') {
			
			# Update the agency contacts ignored
			$sql = 	" UPDATE agencies SET " .
					$this->variables['field'] . " = " . $this->quote($this->variables['value']) .   
					" WHERE agency_id = " . $this->quote($agency_id);
			$this->execsql($sql);

			if ($this->variables['field'] == 'agency_contacts_ignored') $result['agency_contacts_ignored'] = $this->variables['value'];
		}

		if (isset($this->variables['field']) && $this->variables['field'] == 'agency_contact_freshdesk_id') {

			# Restore deleted/Create new Freshdesk contact
			if ($this->variables['value'] == 'Y') {
				if ($this->variables['id_status'] == 'Deleted') {
					# Restore contact
					$jsondata = array (
						'user[customer_id]' => trim($this->variables['customer_id']),
						'user[client_manager]' => 'true',
						'user[deleted]' => 'false'
					);
					# Call Freshdesk API - put
					$this->call_freshdesk_API("contacts", "PUT", $jsondata, $this->variables['id']);

					# Get updated Freshdesk login
					$response = $this->call_freshdesk_API("contacts", "GET", $jsondata, $this->variables['id']);
					
					$result['freshdesk_id'] = $this->variables['id'];
					$result['customer_id'] = $this->variables['customer_id'];
					$result['performed_action'] = 'restored';
					if ($response->user->active == 'TRUE') $result['id_status'] = "Verified";
					else $result['id_status'] = "Unverified";

				} else {
					# Create new contact
					$jsondata = array (
						'user[name]' => rawurldecode(trim($this->variables['name'])),
						'user[email]' => trim($this->variables['email']),
						'user[customer_id]' => trim($this->variables['customer_id']),
						'user[client_manager]' => 'true'
					);
					# Call Freshdesk API - post
					$response = $this->call_freshdesk_API("contacts", "POST", $jsondata, $this->variables['id']);

					$result['freshdesk_id'] = $response->user->id;
					$result['customer_id'] = $response->user->customer_id;
					$result['performed_action'] = 'created';
					if ($response->user->active == 'TRUE') $result['id_status'] = "Verified";
					else $result['id_status'] = "Unverified";
				}
			}
			# Delete Freshdesk Contact
			else {
				$jsondata = "";
				# Call Freshdesk API - delete
				$this->call_freshdesk_API("contacts", "DELETE", $jsondata, $this->variables['id']);

				$result['freshdesk_id'] = $this->variables['id'];
				$result['customer_id'] = $this->variables['customer_id'];
				$result['performed_action'] = 'deleted';
				$result['id_status'] = "Deleted";
			}

			# Update agency contact freshdesk id in database
			$sql = 	" UPDATE agency_contacts SET " .
					$this->variables['field'] . " = " . $this->quote($result['freshdesk_id']) . " , " . 
					" agency_contact_freshdesk_customer_id = " . $this->quote($result['customer_id']) . " , " .
					" agency_contact_freshdesk_id_status = " . $this->quote($result['id_status']) .
					" WHERE agency_contact_id = " . $this->quote($this->variables['agency_contact_id']);
			$this->execsql($sql);
		} 
		
		# return the results of the array
		return $result;
		
	}

	
	/**
	 * Function to delete all associated Freshdesk logins to an agency 
	 *
	 * @return unknown
	 */
	function delete_freshdesk_logins($customer_id) 
	{
		# Delete all associated Freshdesk logins to this agency
		foreach ($this->agency_logins as $login) {
			# Delete all active logins
			# Deleted logins are dissociated from the agency
			if ($login['id_status'] != 'Deleted' && $login['agency_custid'] == $customer_id) {
				$this->call_freshdesk_API('contacts','DELETE','',$login['freshdesk_id']);
			}
		}

		return true;
	}


	/**
	 * Function to do API calls to freshdesk
	 *
	 * @return unknown
	 */
	function call_freshdesk_API($call_type, $process, $jsondata = "",$id = "") {

		# Initialise Freshdesk credentials and customer data
		$email = FRESHDESK_LOGIN_EMAIL;
		$password = FRESHDESK_LOGIN_PASSWORD;

		$header[] = "Content-type: multipart/form-data";
		 
		if ($call_type == 'customers') {
			if (!is_null($this->agency_freshdesk_custid)) {
				$id = "/" . $this->agency_freshdesk_custid;
			} else {
				$id = "";
			}
		} 
		
		if ($call_type == 'contacts') {
			if ($id != -1) {
				$id = "/" . $id;
			} else {
				$id = "";
			}
		}

		$url = FRESHDESK_DOMAIN . "/" . $call_type . $id . ".json";

		$ch = curl_init ($url);
		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $process);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsondata);
		 
		curl_setopt($ch, CURLOPT_USERPWD, "$email:$password");
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_HEADER, false);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		
		$server_output = curl_exec ($ch);
		
		$response = json_decode($server_output);
		$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close ($ch);

		# Update database records
		if ($http_status == 201 && $process == 'POST') {
			if ($call_type == 'customers') {
				# Store Freshdesk customer_id in agencies 
				$sql = "UPDATE agencies SET " .
					   "agency_freshdesk_custid = " . $this->quote($response->customer->id) . " WHERE " .
					   "agency_id = " . $this->quote($this->agency_id);
				$this->execsql($sql);

				return $response;
			} 			
		} 
		if ($http_status == 200 && $process == 'DELETE') {
			
			if ($call_type == 'customers') {
				# Delete customer id Freshdesk in the agencies table
				$sql = "UPDATE agencies SET " .
					   "agency_freshdesk_custid = NULL WHERE " .
					   "agency_id = " . $this->quote($this->agency_id);
				$this->execsql($sql);
			} 
		}
		
		if ($call_type == 'contacts') return $response;
		
		return true;	
	}

	/**
	 * Function to check if customer is setup in Freshdesk
	 *
	 * @return customer id
	 */
	function in_freshdesk($search_type = 'contacts', $search_value, $search_value_type = 'email')
	{
		# Initialise Freshdesk log-in credentials
		$email = FRESHDESK_LOGIN_EMAIL;
		$password = FRESHDESK_LOGIN_PASSWORD;
		
		$header[] = "Content-type: multipart/form-data";

		# Construct url
		if ($search_value_type == 'email') {
			$condition = "email is " . $search_value;
			$url = FRESHDESK_DOMAIN . "/" . $search_type . ".json?query=" . urlencode($condition) . "&state=all";
		} else {
			$url = FRESHDESK_DOMAIN . "/" . $search_type . "/" . $search_value . ".json?state=all";
		}
		
		# Make the API call
		$ch = curl_init ($url);

		curl_setopt($ch, CURLOPT_HTTPGET, true);
		 
		curl_setopt($ch, CURLOPT_USERPWD, "$email:$password");
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_HEADER, false);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		
		$server_output = curl_exec ($ch);
		$response = json_decode($server_output);

		if (count($response) < 1 || isset($response->errors)) return false;
		if ($search_value_type == 'email') return $response;
		else return true;
	}


	/**
	 * Function gets a collection of freshbooks contacts belonging to a client
	 * via Freshbooks API
	 *
	 * @return array
	 */
	function get_freshbooks_contacts($clientId)
	{
		#FreshBooks_HttpClient::init(FRESHBOOKS_URL_TEST,FRESHBOOKS_TOKEN_TEST);
		FreshBooks_HttpClient::init(FRESHBOOKS_URL,FRESHBOOKS_TOKEN);

		# Get list of all freshbooks contacts per client 
		$client = new FreshBooks_Client();
		
		# Initialise array
		$contacts = array();

		# Get an instance of this client via an API call
		$client->get($clientId);

		# Process Primary Contact
		$primary_email = strtolower(trim($client->email));
		$contacts[$primary_email]['contact_id'] = 0;
		$contacts[$primary_email]['first_name'] = (trim($client->firstName) == '' ? '' : ucfirst(strtolower(trim($client->firstName))));
		$contacts[$primary_email]['last_name'] = (trim($client->lastName) == '' ? '' : ucfirst(strtolower(trim($client->lastName))));
		$contacts[$primary_email]['email'] =  $primary_email;
		$contacts[$primary_email]['name'] =  trim($contacts[$primary_email]['first_name'] . ' ' . $contacts[$primary_email]['last_name']);

		$secondary_contacts = json_decode($client->contacts);

		if (isset($secondary_contacts->contact)) {

			if (count($secondary_contacts->contact) == 1) {
				$secondary_email = strtolower(trim($secondary_contacts->contact->email));
				$contacts[$secondary_email]['contact_id'] = $secondary_contacts->contact->contact_id;
				$contacts[$secondary_email]['first_name'] = (is_object($secondary_contacts->contact->first_name) ? '' : ucfirst(strtolower(trim($secondary_contacts->contact->first_name))));
				$contacts[$secondary_email]['last_name'] = (is_object($secondary_contacts->contact->last_name) ? '' : ucfirst(strtolower(trim($secondary_contacts->contact->last_name))));
				$contacts[$secondary_email]['email'] = $secondary_email;
				$contacts[$secondary_email]['name'] = trim($contacts[$secondary_email]['first_name'] . ' ' . $contacts[$secondary_email]['last_name']);
			} else {
				foreach ($secondary_contacts->contact as $contact) {
					$secondary_email = strtolower(trim($contact->email));
					$contacts[$secondary_email]['contact_id'] = $contact->contact_id;
					$contacts[$secondary_email]['first_name'] = (is_object($contact->first_name) ? '' : ucfirst(strtolower(trim($contact->first_name))));
					$contacts[$secondary_email]['last_name'] = (is_object($contact->last_name) ? '' : ucfirst(strtolower(trim($contact->last_name))));
					$contacts[$secondary_email]['email'] = $secondary_email;
					$contacts[$secondary_email]['name'] = trim($contacts[$secondary_email]['first_name'] . ' ' . $contacts[$secondary_email]['last_name']);
				}
			}
		}
		return $contacts;
	}

	
	
	/**
	 * Function to update all the agency contacts records in the database via the scheduled task
	 *
	 * @return array
	 */
	function update_contacts_records($agencies) {

		# Get all contacts/agency logins stored in the database
		$all_db_contacts = $this->get_agency_contacts();
		
		$reporting = load_model('reporting');

		# Go through all agencies to get logins and process records
		foreach ($agencies as $record) {

			# If we monitor contacts for this agency
			# Store contacts in db
			if ($record['agency_direct_relationship'] == 'Y' && $record['agency_contacts_ignored'] == 'N') {
				
				# Test with Kingpin
				#if ($record['agency_name'] == 'Kingpin') {	

					# Initialise contacts lists
					$temp_contacts_list = array();
					$current_contacts_list = array();
					$db_contacts_list = array();
					
					if (isset($all_db_contacts[$record['agency_id']])) {
						
						$db_contacts_list = $all_db_contacts[$record['agency_id']];

						# Assume all users are deleted for now
						# Actual status will be updated later
						foreach ($db_contacts_list as &$db_contact) {
							$db_contact['agency_contact_login_status'] = 'deleted';						 	
						} 
					}

					# Process freshbooks contacts records
					$freshbooks_contacts = array();

					if (isset($record['agency_freshbooks_clientid']) && $record['agency_freshbooks_clientid'] != '') {
						
						# Get all freshbooks contacts for this agency
						$freshbooks_contacts_list = $this->get_freshbooks_contacts($record['agency_freshbooks_clientid']);

						# Process each Freshbooks contacts to add to the collection
						foreach ($freshbooks_contacts_list as $key => $contact) {

							# Reset contact user id to either 0 or negative so it does not conflict with agency login ids
							$id = $contact['contact_id'];
							if ($id != 0)  $id = -1 * abs($id);
							
							$freshbooks_contacts[$key]['agency_id'] = $record['agency_id'];
							$freshbooks_contacts[$key]['agency_contact_instiller_instance'] = $record['agency_control_panel_url'];
							$freshbooks_contacts[$key]['agency_contact_email'] = $contact['email'];
							$freshbooks_contacts[$key]['agency_contact_name'] = $contact['name'];
							$freshbooks_contacts[$key]['agency_contact_freshbooks_clientid'] = $record['agency_freshbooks_clientid'];
							$freshbooks_contacts[$key]['agency_contact_freshbooks_client_name'] = $record['agency_freshbooks_client_name'];
							$freshbooks_contacts[$key]['agency_contact_freshbooks_id'] = $contact['contact_id'];
							$freshbooks_contacts[$key]['agency_contact_company_user_id_temp'] = $id;
							$freshbooks_contacts[$key]['agency_contact_first_name'] = $contact['first_name'];
							
							if ($freshbooks_contacts[$key]['agency_contact_freshbooks_id'] == 0) $freshbooks_contacts[$key]['agency_contact_freshbooks_primary'] = 'Y';
							else $freshbooks_contacts[$key]['agency_contact_freshbooks_primary'] = 'N';
						}
					}

					# Get agency login query
					$sql = $reporting->get_agency_logins_sql();

					# Create a solution db connection
					$connection = $this->select_db_connection($record);
					$connection->SetFetchMode(ADODB_FETCH_ASSOC);

					# Load the results from this database
					$run_sql = str_replace('CONTROL_PANEL_URL',$record['agency_control_panel_url'], $sql);
					$login_records = $connection->execute($run_sql);

					if($login_records === false) {
						neteffekt_error_handler('SQL Error',$connection->errorMsg());
					}
					
					# Process agency login records
					# Initialise this agency up-to-date logins
					$agency_logins = array();

					# Process each record to add to the collection
					while (($login = $login_records->FetchRow()) !== false) {

						$login['company_users_email'] = strtolower(trim($login['company_users_email']));
						$agency_logins[$login['company_users_email']]['agency_id'] = $record['agency_id'];
						$agency_logins[$login['company_users_email']]['agency_contact_instiller_instance'] = $record['agency_control_panel_url'];
						$agency_logins[$login['company_users_email']]['agency_contact_email'] = $login['company_users_email'];
						$agency_logins[$login['company_users_email']]['agency_contact_name'] = ucwords(strtolower($login['company_users_name']));
						$agency_logins[$login['company_users_email']]['agency_contact_solution_login'] = 'Y';
						$agency_logins[$login['company_users_email']]['agency_contact_company_user_id'] = $login['company_users_id'];
						$agency_logins[$login['company_users_email']]['agency_contact_first_name'] = $this->get_first_name($login['company_users_name']);
					
					}

					$temp_contacts_list = array_replace_recursive($agency_logins, $freshbooks_contacts);


					# Initialise contact template array
					$template = array();
					$template['agency_id'] = NULL;
					$template['agency_contact_instiller_instance'] = $record['agency_control_panel_url'];
					$template['agency_contact_email'] = NULL;
					$template['agency_contact_name'] = NULL;
					$template['agency_contact_campaign_event_triggered'] = 'N';
					$template['agency_contact_freshbooks_clientid'] = NULL;
					$template['agency_contact_freshbooks_client_name'] = NULL;
					$template['agency_contact_freshbooks_id'] = NULL;
					$template['agency_contact_freshbooks_primary'] = NULL;
					$template['agency_contact_solution_login'] = 'N';
					$template['agency_contact_company_user_id'] = NULL;
					$template['agency_type'] = $record['agency_type'];
					$template['agency_billing_plan'] = $this->translate_billing_plan($record['agency_billing_plan']);
					$template['agency_control_panel_url'] = $record['agency_control_panel_url'];
					$template['agency_contact_instiller_user_details'] = NULL;
					$template['agency_contact_events_triggered'] = json_encode(json_decode("{}"));

					# Process each contact on this temporary list
					foreach ($temp_contacts_list as $contact) {
						
						if (!isset($contact['agency_contact_company_user_id'])) {
							$contact['agency_contact_company_user_id'] = $contact['agency_contact_company_user_id_temp'];
							unset($contact['agency_contact_company_user_id_temp']);
						}

						# Pass the contact information through the contact template array
						$contact = array_replace($template,$contact);

						# Check if the user exists in the current list and then process accordingly					
						if (isset($db_contacts_list[$contact['agency_contact_company_user_id']])) {
							# The user exist, check if there are any changes to it
							$contact['agency_contact_login_status'] = 'active';
							$contact['agency_contact_campaign_event_triggered'] = $db_contacts_list[$contact['agency_contact_company_user_id']]['agency_contact_campaign_event_triggered'];
							$contact['agency_contact_events_triggered'] = $db_contacts_list[$contact['agency_contact_company_user_id']]['agency_contact_events_triggered'];

							# Check if name/email address have been changed then flag for update
							if ($db_contacts_list[$contact['agency_contact_company_user_id']]['agency_contact_email'] != $contact['agency_contact_email'] || $db_contacts_list[$contact['agency_contact_company_user_id']]['agency_contact_name'] != $contact['agency_contact_name']) {
								$contact['agency_contact_login_status'] = 'updated';

								# Check if the contact email has been updated
								if ($db_contacts_list[$contact['agency_contact_company_user_id']]['agency_contact_email'] != $contact['agency_contact_email']) {
									
									# Initialise API fields
									# Globally opt-out user to stop the event campaign
									$fields = array();
									$fields['email_address'] = $db_contacts_list[$contact['agency_contact_company_user_id']]['agency_contact_email'];
									$fields['global_opt_out'] = 'Y';

									# Globally opt-out old email in instiller without updating the db
									$this->agency_contact_call_API($contact, 'POST', 'add_or_update', $fields, false);

									# Reset agency_contact_campaign_event_triggered to 'N' so process will trigger the campaign event again
									$contact['agency_contact_campaign_event_triggered'] = 'N';
								}
							}
						
						} else {
							# This a new contact, insert entry to the database
							$contact['agency_contact_login_status'] = 'new';
						}

						# Add up-to-date contact to 
						$current_contacts_list[$contact['agency_contact_company_user_id']] = $contact;
					}

					# Create the final list of users with up-to-date data/status
					$current_contacts_list = array_replace_recursive($db_contacts_list, $current_contacts_list);

					# Process final list of contacts accordingly
					foreach ($current_contacts_list as &$contact) {

						# Initialise API fields
						$fields = array();
						$fields['email_address'] = $contact['agency_contact_email'];
						$fields['first_name'] = $contact['agency_contact_first_name'];
						$fields['variable_data_3'] = $contact['agency_control_panel_url'];
						$fields['variable_data_4'] = $contact['agency_billing_plan'];


						# If the user has been deleted, globally 
						if ($contact['agency_contact_login_status'] == 'deleted') $fields['global_opt_out'] = 'Y';

						# Update user details in db
						$contact['agency_contact_instiller_user_details'] = json_encode($this->agency_contact_call_API($contact, 'POST', 'add_or_update', $fields, false));
						
						# Process each up-to-date contacts dependent on the current status
						$this->process_agency_contact($contact);
					}
				#}
			}
		}
		return true;
	}

	/**
	 * This function triggers the learning resource event
	 * Max triggers - 50 contacts
	 * User opt-in prefs will be respected
	 *
	 * @param none
	 * @return (bool) true
	 */
	function trigger_learning_resource_event() {
		
		# Get active contacts in db from any agency 
		# that has not been triggered the event EVENT_API_IDENTIFIER Limit to 50 results
		$contacts = array();
		$contacts = $this->get_agency_contacts('active','',50,EVENT_API_IDENTIFIER);

		# If there no contacts to process return false
		if (count($contacts) < 1) return true; 

		# Loop through all agency contacts
		foreach ($contacts as &$contact) {
			
			# Decode instiller user details
			$agency_contact_instiller_user_details = json_decode($contact['agency_contact_instiller_user_details'],true);
			
			# Unset the instiller user details as this is no longer needed and will be updated at some point
			#unset($contact['agency_contact_instiller_user_details']);
			
			# Get instiller user details
			$user = $agency_contact_instiller_user_details['response'];

			# Initialise user opt-in preference
			$contact['agency_contact_opt_in_pref']['global'] = 'N';
			$contact['agency_contact_opt_in_pref']['list'] = 'N';

			# Analyse user opt-in pref based on current user details 
			if ($user['global_opt_out'] == 'N') {

				$contact['agency_contact_opt_in_pref']['global'] = 'Y';
				
				if (isset($user['lists']) && count($user['lists']) > 0) {
					
					foreach ($user['lists'] as $list) {
						
						if ($list['list_api_identifier'] == LIST_API_IDENTIFIER && $list['subscribed'] == 'Y') {
							
							$contact['agency_contact_opt_in_pref']['list'] = 'Y';
							break;

						}

					}

				} else {
					
					$contact['agency_contact_opt_in_pref']['list'] = 'Y';

				}

			} 

			# If the user is currently opted-in globally and subscribed to agency contacts list, trigger the email campaign
			if ($contact['agency_contact_opt_in_pref']['global'] == 'Y' && $contact['agency_contact_opt_in_pref']['list'] == 'Y') {

				# Trigger email event
				# Initialise fields
				$fields = array();
				$fields['email_address'] = $contact['agency_contact_email'];
				$fields['event_api_identifier'] = EVENT_API_IDENTIFIER;
				$fields['first_name'] = $contact['agency_contact_first_name'];
				$fields['variable_data_3'] = $contact['agency_control_panel_url'];
				$fields['variable_data_4'] = $contact['agency_billing_plan'];

				# Trigger the learning resource campaign event
				$this->agency_contact_call_API($contact, 'POST', 'trigger_event', $fields, false);

				# Email event has been triggered
				$contact['agency_contact_campaign_event_triggered'] = 'Y';
			
			}

			# Update agency contact's events triggered list
			# Initialise this event details based on the analysis of this users opt-in preferences
			$this_event = array();
			$this_event['event_api_id'] = EVENT_API_IDENTIFIER; 
			$this_event['triggered'] = $contact['agency_contact_campaign_event_triggered'];

			# Initialise this contact's events ready for update
			$events_triggered = array();
			$agency_contacts_events_triggered = json_decode($contact['agency_contact_events_triggered'],true);
			if (!is_null($agency_contacts_events_triggered) && count($agency_contacts_events_triggered) > 0) $events_triggered = $agency_contacts_events_triggered;

			# Update this contact's events 
			array_push($events_triggered,$this_event);
			$contact['agency_contact_events_triggered'] = json_encode((object)$events_triggered);

			$this->process_agency_contact($contact);
		}

		# Create a log of this process
		return $this->log_agency_event_trigger($contacts, EVENT_API_IDENTIFIER);
	}

	
	/**
	 * This function logs the campaign event triggered
	 *
	 * @param process details (array), event api identifier
	 * @return (bool) true
	 */
	function log_agency_event_trigger($details, $event_api_id) {
		
		# Initialise log record
		$log = array();
		$log['api_id'] = $event_api_id;

		# Generate a log reference for this event
		# Use current timestamp for reference
		$log['reference'] = 'EV-' . time();  
		
		# Get the event description to store to db for display
		$log['description'] = $this->translate_event($event_api_id);
				
		# JSON encode process details to store in db
		$log['details'] = json_encode($details);

		# Add the new record in to the campaign_events_log table
		$sql = "INSERT INTO campaign_events_log (" .
			   "campaign_event_reference, " . 
			   "campaign_event_description, " .
			   "campaign_event_api_id, " . 
			   "campaign_event_date_triggered, " .
		       "campaign_event_trigger_details " . 
		       ") VALUES (" .
		       $this->quote($log['reference']) . " , " .
		       $this->quote($log['description']) . " , " .
		       $this->quote($log['api_id']) . " , " .
		       " NOW() , " .
		       $this->quote($log['details']) . 
		       " )";
		
		$this->execsql($sql);

		return true;
	}

	/**
	 * This function gets all the campaign event history for display
	 *
	 * @param unknown
	 * @return array
	 */
	function get_campaign_event_history() {

		# Get all campaign events log for display
		$sql = " SELECT * FROM campaign_events_log ORDER BY campaign_event_id DESC ";
		$records = $this->execsql($sql);

		# Initilise array and go through all records
		$results = array();

		while (($record = $records->FetchRow()) !== false) {
			
			# Insert record into the results array
			$results[$record['campaign_event_id']] = $record;
			
			# Decode the campaign event for analysis 
			$results[$record['campaign_event_id']]['campaign_event_trigger_details'] = json_decode($record['campaign_event_trigger_details'],true);
		
			# Initialise results
			$event_results = array();
			$event_results['contacts'] = 0;
			$event_results['triggered'] = 0;
			$event_results['not_triggered'] = 0;

			# Analyse campaign event details
			# Check if there are records just to make sure
			if (count($results[$record['campaign_event_id']]['campaign_event_trigger_details']) > 0) {
				
				# Loop through all records to analyse how many event was/was not triggered based on user prefs 
				foreach ($results[$record['campaign_event_id']]['campaign_event_trigger_details'] as $contact) {
					$event_results['contacts']++;
					if ($contact['agency_contact_campaign_event_triggered'] == 'Y') $event_results['triggered']++;
					if ($contact['agency_contact_campaign_event_triggered'] == 'N') $event_results['not_triggered']++;
				}
			}

			# Assign high level results to render in pop-up
			$results[$record['campaign_event_id']]['campaign_event_high_level_results'] = $event_results;
		}
		
		# Return all records
		return $results;
	}

	/**
	 * This function will return the event description based on the event api identifier
	 * The function will use the Instiller API params as defined in configure.php
	 *
	 * @param event api identifier
	 * @return string
	 */
	function translate_event($event_api_id) {

		# Initialise API parameters
		$method = 'GET';
		$url = API_URL . 'events/list';
		$data = array();
		$data['api_id'] = INSTILLER_API_ID;
		$data['api_key'] = INSTILLER_API_KEY;

		# Make the API call
		$results = $this->call_API($method, $url, $data);

		# Initialise the description
		$event_description = '';

		# Check if there are events returned
		if ($results['valid'] === true) {

			# Check for result count just to make sure we are looping through a result
			if (count($results['response']) > 0) {

				# Loop through the array to get the corresponding description
				foreach ($results['response'] as $event) {
					
					# If a match is found, assign the description, then break the loop
					if ($event['event_api_identifier'] == $event_api_id) {

						$event_description = $event['event_description'];
						break;

					}
				}
			}
		}

		# Return the event description
		return $event_description;
	}

	/**
	 * This function will return all agency based on filter
	 *
	 * @param filter
	 * @return array
	 */
	function get_all_agencies($filter = 'all') {
		
		# Build the query to search
		$sql = " SELECT * FROM agencies ";

		$filter_description = "All";

		if ($filter == 'direct') {
			$sql .= " WHERE agency_direct_relationship = 'Y' AND agency_status = 'Active' ";
			$filter_description = "Direct Relationship";
		}
		if ($filter == 'indirect') {
			$sql .= " WHERE agency_direct_relationship = 'N' AND agency_status = 'Active' ";
			$filter_description = "Indirect Relationship";
		}
		if ($filter == 'deleted') {
			$sql .= " WHERE agency_status = 'Deleted' ";
			$filter_description = "Deleted";
		}
		if ($filter == 'active') {
			$sql .= " WHERE agency_status = 'Active' ";
			$filter_description = "Active";
		}
		if ($filter == 'rapidssl') {
			$sql .= " WHERE agency_status = 'Active' AND agency_ssl_provider = 'RAPIDSSL' ";
			$filter_description = "RapidSSL";
		}
		if ($filter == 'godaddy') {
			$sql .= " WHERE agency_status = 'Active' AND agency_ssl_provider = 'GODADDY' ";
			$filter_description = "GoDaddy";
		}
		if ($filter == 'own') {
			$sql .= " WHERE agency_status = 'Active' AND agency_ssl_provider = 'OWN' ";
			$filter_description = "Managed by Agency";
		}

		$sql .= " ORDER BY agency_name ";
		
		$records = $this->execsql($sql);

		# Initialise agencies list
		$agencies_data = array();
		$agencies_data['agency_list'] = array();

		while (($record = $records->FetchRow()) !== false) {

			# Assign solution owners to each agency for display
			if (strtoupper($record['agency_type']) == 'INSTILLER') {
				$record['solution_owner'] = 'Instiller';
			} elseif (strtoupper($record['agency_type']) == 'INBOX_WARRIORS') {
				$record['solution_owner'] = 'Inbox Warriors';
			} elseif (strtoupper($record['agency_type']) == 'PROFUSION') {
				$record['solution_owner'] = 'Profusion';			
			} elseif (strtoupper($record['agency_type']) == 'BLACKSUN') {
				$record['solution_owner'] = 'Black Sun';
			} elseif (strtoupper($record['agency_type']) == 'BLUETOAD') {
				$record['solution_owner'] = 'Bluetoad';
			} elseif (strtoupper($record['agency_type']) == 'ELOCATION') {
				$record['solution_owner'] = 'Elocation';
			} elseif (strtoupper($record['agency_type']) == 'RESPONSE_GENIUS') {
				$record['solution_owner'] = 'Response Genius';
			} else {
				$record['solution_owner'] = 'Instiller';			
			}
			
			$record['agency_ssl'] = $this->translate_ssl_status($record);
			
			# Add updated agency records to array
			$agencies_data['agency_list'][] = $record;
		}
		
		$agencies_data['filter'] = $filter;
		$agencies_data['filter_description'] = $filter_description;

		# Return agency list
		return $agencies_data;
	}

	/**
	 * This function will return the ssl status of the solution instance
	 *
	 * @param expiry date
	 * @return array
	 */
	function translate_ssl_status($solution)
	{
		$ssl['ssl_status'] = 'ok';
		$ssl['days_remaining'] = floor((strtotime($solution['agency_ssl_renewal_date'])-strtotime(date("Y-m-d",time())))/(60*60*24));
		$ssl['ssl_status_info'] = 'Valid';

		if ($ssl['days_remaining'] <= 30) {
			$ssl['ssl_status'] = 'warning';
			$ssl_status_info = 'Peer&apos;s Certificate is about to expire : ';
			$ssl['ssl_status_info'] = $ssl_status_info . $ssl['days_remaining'] . ' days remaining.';
			if ($ssl['days_remaining'] == 1) $ssl['ssl_status_info'] = $ssl_status_info . '1 day remaining.';
		}

		if (isset($solution['agency_ssl_error']) && !is_null($solution['agency_ssl_error']) && trim($solution['agency_ssl_error']) != '') {
			$ssl['ssl_status'] = 'critical';
			$ssl['ssl_status_info'] = $solution['agency_ssl_error'];
		}
		return $ssl;
	}

	/**
	 * This function will load the properties of the selected agency into the object
	 *
	 * @param int $agency_id
	 * @return boolean
	 */
	function load_agency($agency_id) 
	{
		# Handle creating a shell for new records
		if ($agency_id == 'new') {
			$this->agency_direct_relationship = 'Y';
			$this->agency_status = 'Active';
			$additional_transactions = array();
			return true;
		}

		# Query the database for the record
		$sql = "SELECT * FROM agencies " .
		       "WHERE agency_id = " . $agency_id;

		# check to make sure a record could be found
		$records = $this->execsql($sql);
		
		if (($record = $records->fetchRow()) !== false) {
			
			$agency_list = $this->get_agency_contacts('active', $agency_id);
			
			$record['agency_logins'] = array();
			if (isset($agency_list[$agency_id])) $record['agency_logins'] = $agency_list[$agency_id];
			
			$transactions = json_decode($record['agency_pb_details']);

			$record['additional_transactions']['max_transaction_value'] = $record['agency_minimum_commitment'];
			$record['additional_transactions']['additional_transaction_items'] = array();

			if (!is_null($transactions)) {
				foreach ($transactions as $key => $transaction) {
					$item = array();
					$item['item'] = $transaction->item;
					$item['description'] = 'Break ' . $key;
					$item['volume'] = $transaction->volume;
					$item['volume_formatted'] = number_format($transaction->volume);
					$item['unit_cost'] = $transaction->unit_cost;
					$record['additional_transactions']['additional_transaction_items'][] = $item;
					$record['additional_transactions']['max_transaction_value'] = intval($transaction->volume) + 1000000;

				}
			}

			$record['ssl_status'] = NULL;
			if (isset($record['agency_ssl_renewal_date']) && strtotime($record['agency_ssl_renewal_date']) > time()) $record['ssl_status'] = 'ok';
			if (isset($record['agency_ssl_renewal_date']) && strtotime($record['agency_ssl_renewal_date']) < time()) $record['ssl_status'] = 'error';

			$record['contacts_list_view'] = array();
			$record['contacts_list_edit'] = array();
			
			if ($record['agency_freshbooks_clientid'] != NULL) {
				$contacts_list = json_decode($record['agency_freshbooks_sendto_list']);
				$record['contacts_list_edit'] = $this->get_client_freshbooks_contacts($record['agency_freshbooks_clientid']);

				if (!is_null($contacts_list)) {
					foreach ($contacts_list as $contact) {
						$record['contacts_list_view'][] = (array)$contact;
						$record['contacts_list_edit'][$contact->contact_id]['selected'] = 'Y';
					}
				}
			}
			
			# Load the properties of the object
			$this->load_from_buffer($record);
			return true;
		}
	}

	/**
	 * This function will retrieve all agency contact stored in the database
	 *
	 * @return agency contacts
	 */
	function get_agency_contacts($login_status = '', $agency_id = '', $limit = '', $event = '')
	{
		# Initilise contacts
		$contacts = array();
		
		# Query the database for the records
		$sql = " SELECT a.*, b.agency_name, b.agency_type, b.agency_control_panel_url, b.agency_billing_plan FROM agency_contacts a " .
			   " LEFT JOIN agencies b ON a.agency_id = b.agency_id " ;
		       
		if ($agency_id != '' || $login_status != '') $sql .= " WHERE ";

		if ($agency_id != '') $sql .= " a.agency_id = " . $this->quote($agency_id);
		
		if ($agency_id != '' && $login_status != '') $sql .= " AND ";

		if ($login_status != '') $sql .= " a.agency_contact_login_status = " . $this->quote($login_status);

		if (intval(trim($limit)) != '' || trim($limit) == 'all') {
			
			if (trim($event) != '') $sql .= " AND a.agency_contact_instiller_user_details NOT LIKE " . $this->quote('%' . $event . '%') . " AND a.agency_contact_events_triggered NOT LIKE " . $this->quote('%' . $event . '%') . " AND a.agency_contact_solution_login = 'Y' ";

			if (intval(trim($limit)) != '') $sql .= " LIMIT " . intval($limit);
		}

		$records = $this->execsql($sql);

		while (($contact = $records->FetchRow()) !== false) {

			# Translate billing plan
			$contact['agency_billing_plan'] = $this->translate_billing_plan($contact['agency_billing_plan']);

			# Get contact first name
			$contact['agency_contact_first_name'] = $this->get_first_name($contact['agency_contact_name']);

			# Events
			$contact['agency_contact_events'] = array();
			$user_details = json_decode($contact['agency_contact_instiller_user_details'],true);

			if (isset($user_details['response']['events']) && count($user_details['response']['events']) > 0) {
				$contact['agency_contact_events'] = $user_details['response']['events'];
				$contact['agency_contact_campaign_event_triggered'] = 'Y';
			}
			
			# Just an extra sanitasation just to make sure the URL do not contain a slash at the end or it will likely to break the link
			if (substr($contact['agency_contact_instiller_instance'], -1) == '/') {
				$contact['agency_contact_instiller_instance'] = substr($contact['agency_contact_instiller_instance'], 0, -1);
				$contact['agency_control_panel_url'] = $contact['agency_contact_instiller_instance'];
			}

			# Add contact to collection
			if (intval(trim($limit)) != '' || trim($limit) == 'all') $contacts[] = $contact;
			else $contacts[$contact['agency_id']][strtolower(trim($contact['agency_contact_company_user_id']))] = $contact;
		}

		# Return contacts 
		return $contacts;

	}

	/**
	 * This function returns first name of contact
	 * This evaluates whether the name is a generic/departmental name or a person name
	 * Also sanitises double barrelled first names like 'Anne-Marie' etc
	 *
	 * @return string name
	 */
	function get_first_name($name) {

		# Initialise generic/departmental name
		$generic_exceptions = array('support','finance','department','administrator','test','helpdesk','invoicing','accounts','labs','tech','admin','team','data','purchase','purchasing','account','dept.','dept');
		
		$name_components = explode(' ', $name);
			
		$generic = false;
		
		# Check if name seem generic 
		foreach ($name_components as $component) {
			if (in_array(strtolower(trim($component)),$generic_exceptions)) {
				$generic = true;
				break;
			}
		}
		
		# Sanitise First Names
		$first_name = ucfirst(strtolower($name_components[0]));
		
		# If the name is seem a generic name like 'Accounts' etc. Leave as is
		if ($generic) $first_name = ucwords(strtolower($name));
		else {
			# Check if name is hyphenated then make sure that the second name is capitalised
			# Like Anne-Marie etc. Limited to double barrelled names only
			if (strpos($first_name, '-') != false) {
				$hypenated_name = substr($first_name, strpos($first_name, '-') + 1); 
				$first_name = str_replace($hypenated_name, ucfirst($hypenated_name), $first_name);
			}
		}

		return $first_name;
	}


	/**
	 * This function translates agency billing plan
	 *
	 * @return string plan
	 */
	function translate_billing_plan($plan) {

		if (strtoupper($plan) == 'UNLIMITED') return 'HIGH_VOLUME';
		if (strtoupper($plan) == 'GROWING') return 'GROWTH';

		return 'GROWTH';
	}

	
	/**
	 * This function will retrieves all unassigned Freshdesk customers
	 *
	 * @return unassigned freshdesk customers
	 */
	function get_all_unassigned_freshdesk_customers($freshdesk_id) {

		# Get all freshdesk customers
		$freshdesk_customers = $this->get_all_freshdesk_customers();
		
		# Get all assigned freshdesk ids
		$assigned_freshdesk_custids = $this->get_all_agency_freshdesk_ids($freshdesk_id);

		# Workout the the unassigned freshdesk customer ids
		$unassigned_freshdesk_ids = array_diff_key($freshdesk_customers, $assigned_freshdesk_custids);

	    return !isset($unassigned_freshdesk_ids) ? 0 : $unassigned_freshdesk_ids; 

	}

	/**
	 * This function will retrieves all Freshdesk customers
	 *
	 * @return freshdesk customers 
	 */
	function get_all_freshdesk_customers()
	{
		# Initialise logins array
		$customers = array();

		# Initialise Freshdesk credentials
		$email = FRESHDESK_LOGIN_EMAIL;
		$password = FRESHDESK_LOGIN_PASSWORD;
		
		$header[] = "Content-type: multipart/form-data";
		$state = "all";
		
		# Get all verified and unverified logins
		for ($i = 1; ; $i++) {
		     
			# Construct url to retrieve logins per page
			# Freshdesk limit each query to 50 results per page
			# Freshdesk have no option to override the 50 result limits - used loop to work around this limitation.
			$url = 'https://neteffekt.freshdesk.com/customers.json?page=' . $i;
			
			# Initilise and make API call
			$ch = curl_init ($url);

			curl_setopt($ch, CURLOPT_HTTPGET, true);
			 
			curl_setopt($ch, CURLOPT_USERPWD, "$email:$password");
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_HEADER, false);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			
			$server_output = curl_exec ($ch);
			$responses = json_decode($server_output);

			# Process response for for display in UI
			foreach ($responses as $response) {

				# Initialise fd customer for processing
				$fd_customer = array();

				$fd_customer['name'] = $response->customer->name;
				$fd_customer['customer_id'] = $response->customer->id;
				$fd_customer['domains'] = $response->customer->domains;
				$fd_customer['created'] = $response->customer->created_at;
				$fd_customer['updated'] = $response->customer->updated_at;
				
				$customers[$response->customer->id] = $fd_customer;
			}
			
		   	# This conditions are set to retrieve active customers (verified/unverified) first 
		   	# and then the deleted ones
		   	if (count($response) < 50) {
		    	break;
		    }
		}

		# Return processed logins
		return $customers;
	}

	
	/**
	 * This function will retrieves all Freshdesk customer ids associated with agencies
	 *
	 * @return freshdesk customer ids
	 */
	function get_all_agency_freshdesk_ids($freshdesk_id) {

		# Initialise array of freshdesk ids
		$agency_freshdesk_ids = array();

		# Get all agencies with freshdesk account 
		# Query the database for the record
		$sql = " SELECT agency_freshdesk_custid FROM agencies " .
		       " WHERE agency_freshdesk_custid IS NOT NULL ";

		# If there is an assigned freshdesk ID, do not select it so it is included as on option for this agency view
		if (!is_null($freshdesk_id)) $sql .= " AND agency_freshdesk_custid <> " . $this->quote($freshdesk_id);

		$records = $this->execsql($sql);

		while (($record = $records->FetchRow()) !== false) {

			# Add agency records to array
			$agency_freshdesk_ids[$record['agency_freshdesk_custid']] = $record['agency_freshdesk_custid'];
		}
		return $agency_freshdesk_ids;
	}

	/**
	 * This function will retrieves all Freshbooks client ids associated with agencies
	 *
	 * @return freshbooks client ids
	 */
	function get_all_agency_freshbooks_ids($freshbooks_id) {

		# Initialise array of freshbooks client ids
		$agency_freshbooks_ids = array();

		# Get all agencies that are setup in freshbooks 
		# Query the database for the record
		$sql = " SELECT agency_freshbooks_clientid FROM agencies " .
		       " WHERE agency_freshbooks_clientid IS NOT NULL ";

		# If there is an assigned freshbooks client ID, do not select it so it is included as on option for this agency view
		if (!is_null($freshbooks_id)) $sql .= " AND agency_freshbooks_clientid <> " . $this->quote($freshbooks_id);

		$records = $this->execsql($sql);

		while (($record = $records->FetchRow()) !== false) {

			# Add agency records to array
			$agency_freshbooks_ids[$record['agency_freshbooks_clientid']] = $record['agency_freshbooks_clientid'];
		}
		return $agency_freshbooks_ids;
	}
	

	/**
	 * This function will retrieves all Freshdesk logins including verified, unverified and deleted logins
	 *
	 * @return freshdesk logins
	 */
	function get_all_freshdesk_logins()
	{
		# Initialise logins array
		$logins = array();

		# Initialise Freshdesk credentials
		$email = FRESHDESK_LOGIN_EMAIL;
		$password = FRESHDESK_LOGIN_PASSWORD;
		
		$header[] = "Content-type: multipart/form-data";
		$state = "all";
		
		# Get all verified and unverified logins
		for ($i = 1; ; $i++) {
		     
			# Construct url to retrieve logins per page
			# Freshdesk limit each query to 50 results per page
			# Freshdesk have no option to override the 50 result limits - used loop to work around this limitation.
			$url = 'https://neteffekt.freshdesk.com/contacts.json?state=' . $state . '&page=' . $i;
			
			# Initilise and make API call
			$ch = curl_init ($url);

			curl_setopt($ch, CURLOPT_HTTPGET, true);
			 
			curl_setopt($ch, CURLOPT_USERPWD, "$email:$password");
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_HEADER, false);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			
			$server_output = curl_exec ($ch);
			$response = json_decode($server_output);
			
			# Process response for for display in UI
			foreach ($response as $login) {

				# Initialise fd login for processing
				$fd_login = array();

				$fd_login['email'] = strtolower(trim($login->user->email));
				$fd_login['name'] = $login->user->name;
				$fd_login['freshdesk_id'] = $login->user->id;
				$fd_login['customer_id'] = $login->user->customer_id;
				$fd_login['active'] = $login->user->active;
				$fd_login['deleted'] = $login->user->deleted;
				$fd_login['created'] = $login->user->created_at;
				$fd_login['updated'] = $login->user->updated_at;
				
				if ($fd_login['customer_id'] != NULL) $logins[$fd_login['customer_id']][$fd_login['email']] = $fd_login;
			}
			
		   	# This conditions are set to retrieve active logins (verified/unverified) first 
		   	# and then the deleted ones
		   	if ($state == 'deleted' && count($response) < 50) {
		    	break;
		    }
		    if ($state == 'all' && count($response) < 50) {
		    	# Restarting the loop to retrieve deleted logins
		    	$i = 0;
		        $state = "deleted";
		    }
		}

		# Return processed logins
		return $logins;
	}

	
	/*
	 * Update the direct relationship and enabled status of the agency
	 */
	function set_agency_direct_relationship($field, $value, $agency_id) 
	{
		$sql = "UPDATE agencies SET " . $field . " = " . $this->quote($value) . " WHERE " .
			   "agency_id = " . $this->quote($agency_id);
		$this->execsql($sql);
		return true;
	}


	function clear_agency_billing_details()
	{

		# Unset all values before updating the table
		$agency_billing_plan = NULL;
		$agency_freshbooks_clientid = NULL;
		$agency_freshbooks_client_name = NULL;		
		$agency_minimum_commitment = NULL;
		$agency_vat = NULL;
		$agency_solution_service_fee = NULL;
		$agency_additional_instances_cost = NULL;
		$agency_pb_details = NULL;
		$agency_litmus_allocation = NULL;

		# Update the agency notes
		$sql = 	" UPDATE agencies SET " .
				" agency_billing_plan = " . $this->quote($agency_billing_plan) . " , " .
				" agency_freshbooks_clientid = " . $this->quote($agency_freshbooks_clientid) . " , " .
				" agency_freshbooks_client_name = " . $this->quote($agency_freshbooks_client_name) . " , " .				
				" agency_minimum_commitment = " . $this->quote($agency_minimum_commitment) . " , " .
				" agency_solution_service_fee = " . $this->quote($agency_solution_service_fee) . " , " .
				" agency_additional_instances_cost = " . $this->quote($agency_additional_instances_cost) . " , " .
				" agency_vat = " . $this->quote($agency_vat) . " , " .
				" agency_litmus_allocation = " . $this->quote($agency_litmus_allocation) . " , " .
				" agency_pb_details = " . $this->quote($agency_pb_details) . 
				" WHERE agency_id = " . $this->quote($this->variables['agency_id']);
		$this->execsql($sql);
		return true;
	}

	
	/**
	 * Function to handle api agency contacts api calls to Instiller Solution
	 *
	 * @return true
	 */
	function agency_contact_call_API($contact, $method, $action, $additional_fields, $update_db = true)
	{
		# Define the fields required for the rest request
		$fields = array();
		$fields['api_id'] = INSTILLER_API_ID;
		$fields['api_key'] = INSTILLER_API_KEY;
		
		# Merge additional fields for a specific request
		$fields = array_merge($fields, $additional_fields);

		$url = API_URL . "users/" . $action;

		# Make the request to the API
		$results = $this->call_API($method ,$url, $fields);

		if ($results['valid'] === true && $update_db) {
			
			# Update agency contacts
			$sql = " UPDATE agency_contacts SET ";
			
			if ($action == 'add_or_update') {
				$sql .= " agency_contact_instiller_user_details = " . $this->quote(json_encode($results)); 
			}	   
			
			$sql .= " WHERE agency_contact_id = " . $this->quote($contact['agency_contact_id']);

			# Check if we need to update the db
			$this->execsql($sql);
		}

		if ($action == 'add_or_update' || $action == 'details') return $results; 
		else return true;
	}


	/**
	 * This function will load the buffer from the form submission and then update the
	 * database, a check will be made to ensure the data is unique
	 *
	 * @return unknown
	 */
	function save_agency()
	{
		# Load the properties from the form submission
		$this->load_from_buffer($this->variables);

		if ($this->agency_id == 'new' || $this->agency_id == '') {
			$check_id = -1;
		} else {
			$check_id = $this->agency_id;
		}

		if (isset($this->agency_billing_plan) && $this->agency_billing_plan != '') {
			if (trim($this->agency_billing_plan) == 'growing') {
				$this->agency_solution_service_fee = $this->agency_solution_service_fee_growing;
				$this->agency_solution_service_fee_unlimited = NULL;
				$this->agency_additional_instances_cost = NULL;
				$this->agency_pb_details = NULL;
			} else {
				$this->agency_solution_service_fee = $this->agency_solution_service_fee_unlimited;
				$this->agency_solution_service_fee_growing = NULL;
				$this->agency_additional_instances_cost = $this->agency_additional_instances_cost;
				$this->agency_pb_details = $this->agency_additional_transactions;
			}
		}
		
		if ($this->agency_ssl_renewal_date != '') $this->agency_ssl_renewal_date = date("Y-m-d H:i:s",strtotime($this->agency_ssl_renewal_date));
		else $this->agency_ssl_renewal_date = NULL;


		if ($this->agency_direct_relationship == 'N') {
			$this->agency_freshdesk_custid = NULL;
		} 

		if ($this->agency_contacts_monitoring == 'Y') {
			$this->agency_contacts_ignored = 'N';
		} else {
			$this->agency_contacts_ignored = 'Y';
		}

		$this->agency_ssl_managed = NULL;
		if ($this->agency_ssl_provider == 'OWN') $this->agency_ssl_managed = 'N';
		else $this->agency_ssl_managed = 'Y';

		# Process if the agency is new
		if ($check_id == -1) {
			
			$sql = "SELECT COUNT(*) AS counter FROM agencies " .
				   "WHERE agency_name = " . $this->quote($this->agency_name) . " AND " .
				   "agency_id <> " . $check_id;
			
			$records = $this->execsql($sql);
			$record = $records->FetchRow();

			# Check if the agency has a unique name
			if ($record['counter'] > 0) {
				$this->error_message = 'agency name is already in use, please enter a unique name';
				return false;
			}

			# Add in the new record
			$sql = "INSERT INTO agencies (" .
				   "agency_name, " . 
				   "agency_type, " . 
				   "agency_date_created, " .
				   "agency_direct_relationship, " .
			       "agency_database_host, " .
			       "agency_database_name, " . 
			       "agency_control_panel_url, " .
			       "agency_contacts_ignored, " .
			       "agency_freshdesk_custid, " .
			       "agency_freshdesk_client_name, " .
			       "agency_freshbooks_clientid, " .
			       "agency_freshbooks_client_name, " .
			       "agency_freshbooks_sendto_list, " .	
			       "agency_post_invoices, " .		        
			       "agency_billing_plan, " .
			       "agency_billing_status_active, " .
			       "agency_status, " .
			       "agency_notes, " .
			       "agency_ssl_renewal_date, " .
			       "agency_ssl_provider, " .
			       "agency_ssl_notes, " .
			       "agency_ssl_managed, " .
			       "agency_additional_instances_litmus, " .
			       "agency_additional_instances_cost, " .
			       "agency_vat, " .
			       "agency_litmus_allocation, " .
			       "agency_pb_details, " .
			       "agency_minimum_commitment, " .
			       "agency_solution_service_fee_discount, " .
			       "agency_solution_service_fee " . 
			       ") VALUES (" .
			       $this->quote($this->agency_name) . " , " .
			       $this->quote($this->agency_type) . " , " . 
			       "NOW() ," . 
			       $this->quote($this->agency_direct_relationship) . " , " .
			       $this->quote($this->agency_database_host) . " , " . 
			       $this->quote($this->agency_database_name) . " , " .
			       $this->quote($this->agency_control_panel_url) . " , " . 
			       $this->quote($this->agency_contacts_ignored) . " , " . 
			       $this->quote($this->agency_freshdesk_custid) . " , " .
			       $this->quote($this->agency_freshdesk_client_name) . " , " .
			       $this->quote($this->agency_freshbooks_clientid) . " , " .
				   $this->quote($this->agency_freshbooks_client_name) . " , " .	
				   $this->quote($this->agency_freshbooks_sendto_list) . " , " .	
				   $this->quote($this->agency_post_invoices) . " , " .				       
			       $this->quote($this->agency_billing_plan) . " , " .
			       $this->quote($this->agency_billing_status_active) . " , " .
			       $this->quote($this->agency_status) . " , " .
			       $this->quote($this->agency_notes) . " , " .
			       $this->quote($this->agency_ssl_renewal_date) . " , " .
			       $this->quote($this->agency_ssl_provider) . " , " .
			       $this->quote($this->agency_ssl_notes) . " , " .
			       $this->quote($this->agency_ssl_managed) . " , " .
			       $this->quote($this->agency_additional_instances_litmus) . " , " .
			       $this->quote($this->agency_additional_instances_cost) . " , " .
			       $this->quote($this->agency_vat) . " , " .
			       $this->quote($this->agency_litmus_allocation) . " , " .
			       $this->quote($this->agency_pb_details) . " , " .
			       $this->quote($this->agency_minimum_commitment) . " , " .
			       $this->quote($this->agency_solution_service_fee_discount) . " , " .
			       $this->quote($this->agency_solution_service_fee) . 
			       ")";
			$this->execsql($sql);
			$this->agency_id = $this->insert_id();
		} else { 

			# Update record in database
			$sql = " UPDATE agencies SET " .
				   " agency_name = " . $this->quote($this->agency_name) . " , " .
				   " agency_type = " . $this->quote($this->agency_type) . " , " . 
				   " agency_date_updated = NOW(), " . 
				   " agency_direct_relationship = " . $this->quote($this->agency_direct_relationship) . " , " .
			       " agency_database_host = " . $this->quote($this->agency_database_host) . " , " .
			       " agency_database_name = " . $this->quote($this->agency_database_name) . " , " . 
			       " agency_control_panel_url = " . $this->quote($this->agency_control_panel_url) . " , " .
			       " agency_contacts_ignored = " . $this->quote($this->agency_contacts_ignored) . " , " .
			       " agency_freshdesk_custid = " . $this->quote($this->agency_freshdesk_custid) . " , " .
			       " agency_freshdesk_client_name = " . $this->quote($this->agency_freshdesk_client_name) . " , " .
			       " agency_freshbooks_clientid = " . $this->quote($this->agency_freshbooks_clientid) . " , " .
			       " agency_freshbooks_client_name = " . $this->quote($this->agency_freshbooks_client_name) . " , " .
			       " agency_freshbooks_sendto_list = " . $this->quote($this->agency_freshbooks_sendto_list) . " , " .	
			       " agency_post_invoices = " . $this->quote($this->agency_post_invoices) . " , " .		        
			       " agency_billing_plan = " . $this->quote($this->agency_billing_plan) . " , " .
			       " agency_billing_status_active = " . $this->quote($this->agency_billing_status_active) . " , " .
			       " agency_notes = " . $this->quote($this->agency_notes) . " , " .
			       " agency_ssl_renewal_date = " . $this->quote($this->agency_ssl_renewal_date) . " , " .
			       " agency_ssl_provider = " . $this->quote($this->agency_ssl_provider) . " , " .
			       " agency_ssl_notes = " . $this->quote($this->agency_ssl_notes) . " , " .
			       " agency_ssl_managed = " . $this->quote($this->agency_ssl_managed) . " , " .
			       " agency_additional_instances_litmus = " . $this->quote($this->agency_additional_instances_litmus) . " , " .
			       " agency_additional_instances_cost = " . $this->quote($this->agency_additional_instances_cost) . " , " .
			       " agency_vat = " . $this->quote($this->agency_vat) . " , " .
			       " agency_litmus_allocation = " . $this->quote($this->agency_litmus_allocation) . " , " .
			       " agency_pb_details = " . $this->quote($this->agency_pb_details) . " , " .
			       " agency_minimum_commitment = " . $this->quote($this->agency_minimum_commitment) . " , " .
			       " agency_solution_service_fee_discount = " . $this->quote($this->agency_solution_service_fee_discount) . " , " .
			       " agency_solution_service_fee = " . $this->quote($this->agency_solution_service_fee) . 
			       " WHERE agency_id = " . $this->quote($check_id); 
			
			$this->execsql($sql);

			# If contacts are not monitored anymore, delete them from db
			if ($this->agency_contacts_ignored == 'Y') {
				
				$sql =  " DELETE FROM agency_contacts WHERE agency_id = " . $this->quote($check_id);
				$this->execsql($sql);
			
			}
		}
		return true;
	}

	
	/**
	 * Function to delete a agency after it has been loaded from the buffer
	 *
	 * @return unknown
	 */
	function update_agency_status($action)
	{
		# Double-check the correct information has been assigned to agency_id
		if($this->agency_id == 'new' || trim($this->agency_id == '') || !isset($this->agency_id)) {
			return false;
		}
		
		if ($action == 'delete') $agency_status = 'Deleted';
		else $agency_status = 'Active';
					
		# Mark agency appropriately
		$sql = 	" UPDATE agencies SET " .
				" agency_status = " . $this->quote($agency_status) . " , " .
				" agency_direct_relationship = 'N' , " .
				" agency_contacts_ignored = 'Y' " .
				" WHERE agency_id = " . $this->quote($this->agency_id);
		$this->execsql($sql);

		# Delete agency and logins in Freshdesk
		if ($action == 'delete' && $this->in_freshdesk('customers', $this->agency_freshdesk_custid, 'id')) {
			$this->call_freshdesk_API('customers','DELETE');
			$this->delete_freshdesk_logins($this->agency_freshdesk_custid);
		}

		return true;
	}


	/**
	 * Utility function to create a copy of a agency
	 *
	 * @return boolean
	 */
	function create_copy() {
		# Load the info of the item we're copying
		$this->load_agency($this->variables['agency_id']);

		# make the class think this is a new record
		$this->variables['agency_id'] = 'new';

		# change the name to be a 'copy of...'
		$this->agency_name = 'Copy of ' .  $this->agency_name;

		# save the new record
		return $this->save_agency();
	}

	
	/*
	 * Process contact dependent on the status
	 */
	function process_agency_contact($contact) {
		
		if ($contact['agency_contact_login_status'] == 'new') {
			# Insert record
			$sql =  " INSERT INTO agency_contacts ( " . 
					" agency_id, " . 
					" agency_contact_instiller_instance, " . 
					" agency_contact_email, " . 
					" agency_contact_name, " . 
					" agency_contact_campaign_event_triggered, " . 
					" agency_contact_freshbooks_clientid, " . 
					" agency_contact_freshbooks_client_name, " . 
					" agency_contact_freshbooks_id, " . 
					" agency_contact_freshbooks_primary, " .
					" agency_contact_company_user_id, " .
					" agency_contact_login_status, " . 
					" agency_contact_instiller_user_details, " . 
					" agency_contact_events_triggered, " . 
					" agency_contact_solution_login " . 
					" ) VALUES ( " .
			       	$this->quote($contact['agency_id']) . "," . 
			       	$this->quote($contact['agency_contact_instiller_instance']) . " , " . 
			      	$this->quote($contact['agency_contact_email']) . " , " . 
			      	$this->quote($contact['agency_contact_name']) . " , " .
			      	$this->quote($contact['agency_contact_campaign_event_triggered']) . " , " .
			       	$this->quote($contact['agency_contact_freshbooks_clientid']) . " , " .
			       	$this->quote($contact['agency_contact_freshbooks_client_name']) . " , " .
			       	$this->quote($contact['agency_contact_freshbooks_id']) . " , " .
			       	$this->quote($contact['agency_contact_freshbooks_primary']) . " , " . 
			       	$this->quote($contact['agency_contact_company_user_id']) . " , " . 
			       	" 'active' , " . 
			       	$this->quote($contact['agency_contact_instiller_user_details']) . " , " .
			       	$this->quote($contact['agency_contact_events_triggered']) . " , " .
			      	$this->quote($contact['agency_contact_solution_login']) . 
			      	" )"; 

			$this->execsql($sql);
			
			return $this->insert_id();
		
		} else {

			# Initialise contact login status
			$login_status = 'active';
			
			# Update contact login status 
			if ($contact['agency_contact_login_status'] == 'deleted') $login_status = 'deleted';
			
			# Update record in db
			$sql = 	" UPDATE agency_contacts SET " .
					" agency_contact_email = " . $this->quote($contact['agency_contact_email']) . " , " .
					" agency_contact_name = " . $this->quote($contact['agency_contact_name']) . " , " .
					" agency_contact_campaign_event_triggered = " . $this->quote($contact['agency_contact_campaign_event_triggered']) . " , " .
					" agency_contact_instiller_user_details = " . $this->quote($contact['agency_contact_instiller_user_details']) . " , " .
					" agency_contact_events_triggered = " . $this->quote($contact['agency_contact_events_triggered']) . " , " .
					" agency_contact_login_status = " . $this->quote($login_status) . 
					" WHERE agency_contact_id = " . $this->quote($contact['agency_contact_id']);

			$this->execsql($sql);

			return true;
		}
	}


	/*
	 * Delete agency contact from database
	 */
	function delete_agency_contact($contact) {

		# Delete record
		$sql =  " DELETE FROM agency_contacts WHERE agency_contact_id = " . $this->quote($contact['agency_contact_id']);
		$this->execsql($sql);

		return true;
	}

	/*
	 * This function will connect to each notifire agency and get a campaign
	 * list using the type parameter
	 */
	function notifire_campaigns2($campaign_status = '', $sort_type = 'SOLUTION') {

		$sql = "SELECT a.email_spooler_id AS campaign_id, " .
			   "a.email_spooler_description AS description, a.email_spooler_recipient_count AS recipient_count, " .
			   "a.email_spooler_delivery_date AS delivery_date, a.email_spooler_status AS status, b.company_name FROM " .
			   "email_spooler a, company b, email_spooler_build c WHERE " .
		       "a.email_spooler_message_type = 'CAMPAIGN' AND a.email_spooler_id = c.email_spooler_id and " .
			   "a.email_spooler_test_message = 'FALSE' AND " .
			   "a.company_id = b.company_id AND a.contacts_triggered_event_id IS NULL ";
		
		if ($campaign_status != 'QUEUED') {
			$sql .= " AND email_spooler_status <> 'QUEUED' ";
			
		} else {
			$sql .= " AND email_spooler_status = 'QUEUED' ";
		}
		
		$event_sql = "SELECT a.event_spooler_id AS campaign_id, " .
			   "a.event_spooler_description AS description, a.event_spooler_recipient_count AS recipient_count, " .
			   "a.event_spooler_delivery_date AS delivery_date, a.event_spooler_status AS status, b.company_name FROM " .
			   "event_spooler a, company b WHERE " .
		       "a.event_spooler_message_type = 'CAMPAIGN' AND " .
			   "a.company_id = b.company_id ";
		
		if ($campaign_status != 'QUEUED') {
			$event_sql .= " AND event_spooler_status <> 'QUEUED' AND a.event_spooler_delivery_date <= NOW() ";
			
		} else {
			$event_sql .= " AND (event_spooler_status = 'QUEUED' OR a.event_spooler_delivery_date > NOW()) ";
		}
		
		$sql .= " UNION " . $event_sql;
		
		
		if ($campaign_status != 'QUEUED') {
			$sql .= " ORDER BY delivery_date DESC LIMIT 0,20";
			
		} else {
			$sql .= " ORDER BY delivery_date ASC";
		}		

		$agencies = $this->exec_solution_sql('EMAIL_SOLUTIONS', 'INSTILLER', $sql);

		if($sort_type == 'SOLUTION')
		{
			foreach($agencies as $agency){

				$total_recipients = 0;
				$agency_type = $this->translate_db_identifier($agency['agency']['agency_type']);
				$agency_name = $agency['agency']['agency_name'];
					
				if ($campaign_status != 'QUEUED') {
					$notifire_campaigns[$agency_type][$agency_name]['agency_name'] = '';				
					$notifire_campaigns[$agency_type][$agency_name]['agency'] = '';					
				}					
				if(isset($agency['data'])){
					foreach($agency['data'] as $campaign){	
						if($campaign['recipient_count'] > 0) {
									
							$notifire_campaigns[$agency_type][$agency_name]['agency_name'] = $agency_name;					
							$notifire_campaigns[$agency_type][$agency_name]['items'][] = array('campaign_client' => $campaign['company_name'],					
																				                   'campaign_date' => $campaign['delivery_date'],
																			                   	   'campaign_description' => $campaign['description'],
																				                   'campaign_recipients' => $campaign['recipient_count']);
							$total_recipients += $campaign['recipient_count'];
						}					
					}
					$notifire_campaigns[$agency_type][$agency_name]['agency'] = $_SESSION['agencies'][$agency['agency']['agency_id']];				
				} else {				
					if ($campaign_status != 'QUEUED') {
						$notifire_campaigns[$agency_type][$agency_name]['agency_name'] = $agency_name;					
						$notifire_campaigns[$agency_type][$agency_name]['agency'] = $_SESSION['agencies'][$agency['agency']['agency_id']];					
					}
				}
				
				if ($campaign_status == 'QUEUED' && $total_recipients > 0) {
					$notifire_campaigns[$agency_type][$agency_name]['queue_recipients'] = number_format($total_recipients, 0, '.', ',');
				}
				
				
			}
			
			# Get a count of the queued event triggered messages
			if ($campaign_status == 'QUEUED') {
				$sql = "SELECT 'Event Triggered Messages' AS description, " . 
					   "SUM(email_spooler_recipient_count) AS recipient_count FROM " .
					   "email_spooler WHERE " .	
					   "email_spooler_message_type = 'TRIGGERED' AND " .
					   "email_spooler_status = 'QUEUED'";
				$agencies = $this->exec_agency_sql('EMAIL_SOLUTIONS', $sql);
				foreach($agencies as $agency){
					$agency_name = $agency['agency']['agency_name'];
					
					if(isset($agency['data'])){
						foreach($agency['data'] as $campaign){		
							if ($campaign['recipient_count'] > 0) {		
								$notifire_campaigns[$agency_type][$agency_name]['agency_name']  = $agency_name;					
								$notifire_campaigns[$agency_type][$agency_name]['event_queue'][] = array('description' => $campaign['description'],					
																						                          'count' => $campaign['recipient_count']);
							}
						}
						if (isset($notifire_campaigns[$agency_name]['agency_name'])) {
							$notifire_campaigns[$agency_type]['agency'] = $_SESSION['agencies'][$agency['agency']['agency_id']];
						}
					}				
				}		
			}
		}
		else
		{
			foreach($agencies as $agency)
			{
				$agency_type = $this->translate_db_identifier($agency['agency']['agency_type']);
				if(isset($agency['data']))
				{
					foreach($agency['data'] as $campaign)
					{
						$notifire_campaigns[$agency_type][$campaign['campaign_id']]['delivery_date'] = $campaign['delivery_date'];
						$notifire_campaigns[$agency_type][$campaign['campaign_id']]['recipient_count'] = $campaign['recipient_count'];
						$notifire_campaigns[$agency_type][$campaign['campaign_id']]['agency_name'] = $agency['agency']['agency_name'];
						$notifire_campaigns[$agency_type][$campaign['campaign_id']]['description'] = $campaign['description'];
						$notifire_campaigns[$agency_type][$campaign['campaign_id']]['agency_control_panel_url'] = $agency['agency']['agency_control_panel_url'];
					}
					
				}
				
				if(isset($notifire_campaigns[$agency_type]))
				{
					if($sort_type == 'RECIPIENTS')
					uasort($notifire_campaigns[$agency_type], 'agencies::campaign_size_compare');
					else
					uasort($notifire_campaigns[$agency_type], 'agencies::campaign_date_compare');
				}
			}
			
		}
	
		# Show the agencies in alphabetical order
		ksort($notifire_campaigns);
		return $notifire_campaigns;
	}

	/*
	 * This function will connect to each notifire agency and get a campaign
	 * list using the type parameter
	 */
	function notifire_campaigns($campaign_status = '', $sort_type = 'SOLUTION') {

		$start = date("Y-m-d H:i:s",strtotime('-3 days'));
		$end = date("Y-m-d H:i:s",time());

		$sql = "SELECT a.email_spooler_id AS campaign_id, " .
			   "a.email_spooler_description AS description, a.email_spooler_recipient_count AS recipient_count, " .
			   "a.email_spooler_delivery_date AS delivery_date, a.email_spooler_status AS status, b.company_name FROM " .
			   "email_spooler a, company b, email_spooler_build c WHERE " .
		       "a.email_spooler_message_type = 'CAMPAIGN' AND a.email_spooler_id = c.email_spooler_id and " .
			   "a.email_spooler_test_message = 'FALSE' AND " .
			   "a.company_id = b.company_id AND a.contacts_triggered_event_id IS NULL ";
		
		if ($campaign_status != 'QUEUED') {
			$sql .= " AND email_spooler_status <> 'QUEUED' AND (a.email_spooler_delivery_date BETWEEN " . $this->quote($start) . " AND " . $this->quote($end) . ") ";
			
		} else {
			$sql .= " AND email_spooler_status = 'QUEUED' ";
		}
		

		$event_sql = "SELECT a.event_spooler_id AS campaign_id, " .
			   "a.event_spooler_description AS description, a.event_spooler_recipient_count AS recipient_count, " .
			   "a.event_spooler_delivery_date AS delivery_date, a.event_spooler_status AS status, b.company_name FROM " .
			   "event_spooler a, company b WHERE " .
		       "a.event_spooler_message_type = 'CAMPAIGN' AND " .
			   "a.company_id = b.company_id ";
		
		if ($campaign_status != 'QUEUED') {
			$event_sql .= " AND event_spooler_status <> 'QUEUED' AND (a.event_spooler_delivery_date BETWEEN " . $this->quote($start) . " AND " . $this->quote($end) . ") ";
			
		} else {
			$event_sql .= " AND (event_spooler_status = 'QUEUED' OR a.event_spooler_delivery_date > NOW()) ";
		}
		
		$sql .= " UNION " . $event_sql;
		
		if ($campaign_status != 'QUEUED') {
			$sql .= " ORDER BY delivery_date DESC";
		} else {
			$sql .= " ORDER BY delivery_date ASC";
		}		

		$agencies = $this->exec_agency_sql('EMAIL_SOLUTIONS', $sql);

		if($sort_type == 'SOLUTION')
		{
			foreach($agencies as $agency){
				$total_recipients = 0;
				$agency_type = $this->translate_db_identifier($agency['agency']['agency_type']);
				$agency_db_server = $this->translate_db_server($agency['agency']['agency_database_host']);
				$agency_name = $agency['agency']['agency_name'];
					
				if ($campaign_status != 'QUEUED') {
					#$notifire_campaigns[$agency_type][$agency_name]['agency_name'] = '';				
					#$notifire_campaigns[$agency_type][$agency_name]['agency'] = '';					
				}					
				if(isset($agency['data'])){
					foreach($agency['data'] as $campaign){	
						if($campaign['recipient_count'] > 0) {
									
							#$notifire_campaigns[$agency_type][$agency_name]['agency_name'] = $agency_name;					
							$notifire_campaigns[$agency_type]['items'][strtotime($campaign['delivery_date'])] = array('campaign_solution' => $agency_name,
																				 'campaign_client' => $campaign['company_name'],					
															                     'campaign_date' => $campaign['delivery_date'],
														                   	     'campaign_description' => $campaign['description'],
															                     'campaign_recipients' => $campaign['recipient_count']);
							$notifire_campaigns[$agency_db_server]['items'][strtotime($campaign['delivery_date'])] = array('campaign_solution' => $agency_name,
																				 'campaign_client' => $campaign['company_name'],					
															                     'campaign_date' => $campaign['delivery_date'],
														                   	     'campaign_description' => $campaign['description'],
															                     'campaign_recipients' => $campaign['recipient_count']);
							$total_recipients += $campaign['recipient_count'];
						}					
					}
					#$notifire_campaigns[$agency_type][$agency_name]['agency'] = $_SESSION['agencies'][$agency['agency']['agency_id']];				
				} else {				
					if ($campaign_status != 'QUEUED') {
						#$notifire_campaigns[$agency_type][$agency_name]['agency_name'] = $agency_name;					
						#$notifire_campaigns[$agency_type][$agency_name]['agency'] = $_SESSION['agencies'][$agency['agency']['agency_id']];					
					}
				}
				
				if ($campaign_status == 'QUEUED' && $total_recipients > 0) {
					$notifire_campaigns[$agency_type][$agency_name]['queue_recipients'] = number_format($total_recipients, 0, '.', ',');
				}
				
				
			}
			
			# Get a count of the queued event triggered messages
			if ($campaign_status == 'QUEUED') {
				$sql = "SELECT 'Event Triggered Messages' AS description, " . 
					   "SUM(email_spooler_recipient_count) AS recipient_count FROM " .
					   "email_spooler WHERE " .	
					   "email_spooler_message_type = 'TRIGGERED' AND " .
					   "email_spooler_status = 'QUEUED'";
				$agencies = $this->exec_agency_sql('EMAIL_SOLUTIONS', $sql);
				foreach($agencies as $agency){
					$agency_name = $agency['agency']['agency_name'];
					
					if(isset($agency['data'])){
						foreach($agency['data'] as $campaign){		
							if ($campaign['recipient_count'] > 0) {		
								$notifire_campaigns[$agency_type][$agency_name]['agency_name']  = $agency_name;					
								$notifire_campaigns[$agency_type][$agency_name]['event_queue'][] = array('description' => $campaign['description'],					
																						                          'count' => $campaign['recipient_count']);
							}
						}
						if (isset($notifire_campaigns[$agency_name]['agency_name'])) {
							$notifire_campaigns[$agency_type]['agency'] = $_SESSION['agencies'][$agency['agency']['agency_id']];
						}
					}				
				}		
			}
		}
		else
		{
			foreach($agencies as $agency)
			{
				$agency_type = $this->translate_db_identifier($agency['agency']['agency_type']);
				if(isset($agency['data']))
				{
					foreach($agency['data'] as $campaign)
					{
						$notifire_campaigns[$agency_type][$campaign['campaign_id']]['delivery_date'] = $campaign['delivery_date'];
						$notifire_campaigns[$agency_type][$campaign['campaign_id']]['recipient_count'] = $campaign['recipient_count'];
						$notifire_campaigns[$agency_type][$campaign['campaign_id']]['agency_name'] = $agency['agency']['agency_name'];
						$notifire_campaigns[$agency_type][$campaign['campaign_id']]['description'] = $campaign['description'];
						$notifire_campaigns[$agency_type][$campaign['campaign_id']]['agency_control_panel_url'] = $agency['agency']['agency_control_panel_url'];
					}
					
				}
				
				if(isset($notifire_campaigns[$agency_type]))
				{
					if($sort_type == 'RECIPIENTS')
					uasort($notifire_campaigns[$agency_type], 'agencies::campaign_size_compare');
					else
					uasort($notifire_campaigns[$agency_type], 'agencies::campaign_date_compare');
				}
			}
			
		}
		
		foreach ($notifire_campaigns as $reseller) {
			ksort($reseller['items']);
		}

		# Show the agencies in alphabetical order
		ksort($notifire_campaigns);
		
		return $notifire_campaigns;
	}
	
	function campaign_size_compare($a, $b)
	{
		return $b['recipient_count'] - $a['recipient_count'];
	}
	
	function campaign_date_compare($a, $b)
	{
		return strtotime($b['delivery_date']) - strtotime($a['delivery_date']);
	}
	
	/*
	 * This function will connect to each notifire agency and get a summary
	 * for a key number of stats
	 */
	function notifire_stats_summary() {

		return true;
		
		$notifire_stats_summary = array();
		
		$month_1 = 0;
		$month_1_description = '';
		$month_1_label = date('Y-m');
		$month_2 = 0;
		$month_2_description = '';
		$month_2_label = date('Y-m', mktime(0,0,0,date('m')-1,1,date('Y')));		
		$month_3 = 0;
		$month_3_description = '';	
		$month_3_label = date('Y-m', mktime(0,0,0,date('m')-2,1,date('Y')));
		
		$agency_totals = array();
		
		# Get the messages sent
		$sql = "SELECT SUM(company_billing_stats_test_emails + " .
					      "company_billing_stats_email_messages + " .
						  "company_billing_stats_event_emails + " .
						  "company_billing_stats_event_triggers + " .
						  "company_billing_stats_event_forwards + " .
			              "company_billing_stats_api_impressions) AS value, company_billing_stats_period FROM " .
			   "company_billing_stats WHERE " .
		
			   "(company_billing_stats_period LIKE '" . $month_1_label . "%' OR " .
			   " company_billing_stats_period LIKE '" . $month_2_label . "%' OR " .
			   " company_billing_stats_period LIKE '" . $month_3_label . "%') " .						
		
			   "GROUP BY company_billing_stats_period " .		
			   "ORDER BY company_billing_stats_period DESC";
		
		$agencies = $this->exec_agency_sql('EMAIL_SOLUTIONS',$sql);

		foreach($agencies as $agency){
			if(isset($agency['data'])){
				foreach($agency['data'] as $stat){
					# Keep adding to the totals					
					if (strpos($stat['company_billing_stats_period'], $month_1_label) === 0) {
						$month_1 += str_replace(',', '', $stat['value']);						
					} elseif (strpos($stat['company_billing_stats_period'], $month_2_label) === 0) {
						$month_2 += str_replace(',', '', $stat['value']);												
					} elseif (strpos($stat['company_billing_stats_period'], $month_3_label) === 0) {
						$month_3 += str_replace(',', '', $stat['value']);										
					}
				}
				$agency_totals['ACTIVITY_MONTH_1'][$agency['agency']['agency_name']] = $month_1;
				$agency_totals['ACTIVITY_MONTH_2'][$agency['agency']['agency_name']] = $month_2;
				$agency_totals['ACTIVITY_MONTH_3'][$agency['agency']['agency_name']] = $month_3;												
			}		
		}		
		$notifire_stats_summary['ACTIVITY_MONTH_1'] = array('description' => $month_1_label,
													  	    'value' => $month_1);		
		$notifire_stats_summary['ACTIVITY_MONTH_2'] = array('description' => $month_2_label,
													    	'value' => $month_2);		
		$notifire_stats_summary['ACTIVITY_MONTH_3'] = array('description' => $month_3_label,
													     	'value' => $month_3);		
		
		
				
		foreach ($agency_totals as $key => $value) {
			foreach ($value as $subkey => $subvalue) {
				$notifire_stats_summary[$key][$subkey] = $subvalue;
			}
		}

		foreach ($notifire_stats_summary as $key => $value) {
			$notifire_stats_summary[$key] = str_ireplace('sent ', '', $value);
		}
		
		$notifire_stats_summary['ACTIVITY_MONTH_1']['value'] = number_format($notifire_stats_summary['ACTIVITY_MONTH_1']['value'], 0, '.', ',');		
		$notifire_stats_summary['ACTIVITY_MONTH_2']['value'] = number_format($notifire_stats_summary['ACTIVITY_MONTH_2']['value'], 0, '.', ',');		
		$notifire_stats_summary['ACTIVITY_MONTH_3']['value'] = number_format($notifire_stats_summary['ACTIVITY_MONTH_3']['value'], 0, '.', ',');
				
		return $notifire_stats_summary;
	}	
	
	/*
	 * This function will connect to each notifire agency and get the 
	 * a list of imports
	 */
	function notifire_import_log($file_import_upload_status) {
		$sql = "SELECT file_import_reference, file_import_date, " .
			   "file_import_type, file_import_upload_status FROM " .
			   "file_imports WHERE " .
			   "file_import_upload_status = '" . $file_import_upload_status . "' " . 
	   		   "ORDER BY file_import_date DESC";
		$agencies = $this->exec_agency_sql('EMAIL_SOLUTIONS', $sql);
		
		$notifire_import_log = array();
		foreach($agencies as $agency){
			if(isset($agency['data'])){
				foreach($agency['data'] as $import){	
					$notifire_import_log[$import['file_import_reference']] = array('agency_name' => $agency['agency']['agency_name'],
																				   'reference' => $import['file_import_reference'],
																				   'date' => $import['file_import_date'],
																				   'url' => $agency['agency']['agency_control_panel_url'],
																				   'type' => $import['file_import_type'], 
																				   'status' => $import['file_import_upload_status']);
				}
			}
		}
		return $notifire_import_log;
	}	
	

	/*
	 * This function will go through each agency solution activity
	 * for invoicing in Freshbooks
	 */
	function generate_invoices($solutions, $billing_cycle)  
	{
		# init singleton FreshBooks_HttpClient
		FreshBooks_HttpClient::init(FRESHBOOKS_URL,FRESHBOOKS_TOKEN);
		
		# init singleton FreshBooks_HttpClient [TESTING]
		#FreshBooks_HttpClient::init(FRESHBOOKS_URL_TEST,FRESHBOOKS_TOKEN_TEST);

		# Initialise array to store invoice objects
		$invoices = array();

		$charge_codes = $this->get_charge_codes();
		
		# Loop though solution activity and generate invoices
		foreach ($solutions as $solution) {
			# Check dataset
			if ($solution['dataset'] == 'solution' && $solution['name'] != 'Instiller' && $solution['name'] != 'All') {
				# If dataset is a solution but not Instiller, process invoice
				if (!is_null($solution['billing_details']['freshbooks_clientId']) || (isset($solution['billing_details']['freshbooks_clientId']) && trim($solution['billing_details']['freshbooks_clientId']) != '')) {
					# Process client invoice and store in array
					$invoice = $this->process_invoice($solution,$billing_cycle,$charge_codes);
					$invoices[] = $invoice;
				}
			} else {
				if ($solution['name'] == 'Instiller') {
					# If dataset belongs to instiller, loop through all instiller clients and process invoice for each
					foreach ($solution['solution_totals'] as $client) {
						if (!is_null($client['billing_details']['freshbooks_clientId']) || (isset($client['billing_details']['freshbooks_clientId']) && trim($solution['client']['freshbooks_clientId']) != '')) {
							# Process client invoice and store in array
							$invoice = $this->process_invoice($client,$billing_cycle,$charge_codes);
							$invoices[] = $invoice;
						}
					}
				}
			}
		
		}
		# Send invoice array for processing 
		# to create a log for this process in DB 
		# returns true or false dependent on analysis of invoices
		return $this->process_invoice_results($invoices,$billing_cycle);
	}

	/*
	 * Function to insert a billing log in DB
	 * @return true
	 */
	function process_invoice_results($invoices,$billing_cycle)
	{
		# Initialise process result
		$result = array();
		$result['billing_reference'] = sprintf('FB-%s%s',date('my',strtotime($billing_cycle)),date('His',time()));
		$result['billing_date_generated'] = date("Y-m-d H:i:s",time());
		$result['billing_cycle'] = $billing_cycle;
		$result['billing_process_status'] = 'ok';
		$result['billing_status_details'] = '';

		# Initilise process stats
		$billing_process_details = array();
		$billing_process_details['results'] = array();
		$billing_process_details['invoices_generated_count'] = 0;
		$billing_process_details['invoices_not_generated_count'] = 0;
		$billing_process_details['invoice_error_count'] = 0;
		$billing_process_details['email_error_count'] = 0;
		$billing_process_details['post_error_count'] = 0;

		# Process all invoice results
		foreach ($invoices as $invoice) {

			# Initialise each result
			$billing_process_details['results'][$invoice->clientId]['clientName'] = $invoice->clientDetails['name'];
			$billing_process_details['results'][$invoice->clientId]['invoice_number'] = '';
			$billing_process_details['results'][$invoice->clientId]['amount'] = '';
			$billing_process_details['results'][$invoice->clientId]['status'] = 'ok';
			$billing_process_details['results'][$invoice->clientId]['invoice_status'] = 'N/A';
			$billing_process_details['results'][$invoice->clientId]['invoice_error_messages'] = 'N/A';
			$billing_process_details['results'][$invoice->clientId]['email_status'] = 'ok';
			$billing_process_details['results'][$invoice->clientId]['email_error_message'] = 'N/A';
			$billing_process_details['results'][$invoice->clientId]['post_status'] = 'ok';
			$billing_process_details['results'][$invoice->clientId]['post_error_message'] = '';
			$billing_process_details['results'][$invoice->clientId]['sent_by_email'] = 'N';
			$billing_process_details['results'][$invoice->clientId]['sent_by_post'] = 'N';
			$billing_process_details['results'][$invoice->clientId]['create_attempts'] = $invoice->createAttempts;
						
			
			if (isset($invoice->invoiceId) && $invoice->invoiceId != '') {
				
				# If we get to this point, an invoice has been generated in Freshbooks
				# Log invoice status for reporting
				$billing_process_details['invoices_generated_count']++;
				$billing_process_details['results'][$invoice->clientId]['invoice_status'] = $invoice->status;
				$billing_process_details['results'][$invoice->clientId]['invoice_number'] = $invoice->number;
				$billing_process_details['results'][$invoice->clientId]['clientName'] = $invoice->organization;
				$billing_process_details['results'][$invoice->clientId]['amount'] = $invoice->amountOutstanding;

				# Invoices are sent by email automatically if send to list
				# Checking for this and log an error if sentByEmail is false 
				if ($invoice->clientDetails['billing_details']['agency_freshbooks_sendto_list'] != '' && $invoice->sentByEmail == '') {
					$billing_process_details['results'][$invoice->clientId]['status'] = 'error';
					$billing_process_details['results'][$invoice->clientId]['email_status'] = 'error';
					$billing_process_details['results'][$invoice->clientId]['email_error_message'] = 'The invoice has not been sent to one or more contacts.';
					$billing_process_details['email_error_count']++;
				} 
				
				# Invoices are sent by post automatically if set in billing details
				# Checking for this and log an error if sentByPost is false
				if ($invoice->clientDetails['billing_details']['agency_post_invoices'] == 'Y' && $invoice->sentBySnailMail == '') {
					$billing_process_details['results'][$invoice->clientId]['status'] = 'error';
					$billing_process_details['results'][$invoice->clientId]['post_status'] = 'error';
					$billing_process_details['results'][$invoice->clientId]['post_error_message'] = 'API request to send the invoice by post has failed.';
					$billing_process_details['post_error_count']++;
				} 

				# Check if the invoice is marked as sent and update log accordingly
				if ($invoice->status == 'sent') {
					if ($invoice->sentByEmail == TRUE) $billing_process_details['results'][$invoice->clientId]['sent_by_email'] = 'Y';
					else $billing_process_details['results'][$invoice->clientId]['email_status'] = 'N/A';
					
					if ($invoice->sentBySnailMail == TRUE) $billing_process_details['results'][$invoice->clientId]['sent_by_post'] = 'Y';
					else $billing_process_details['results'][$invoice->clientId]['post_status'] = 'N/A';
				}				
			
			} else {
				
				# Reset some log data
				$billing_process_details['results'][$invoice->clientId]['sent_by_email'] = 'N/A';
				$billing_process_details['results'][$invoice->clientId]['sent_by_post'] = 'N/A';
				$billing_process_details['results'][$invoice->clientId]['email_status'] = 'N/A';
				$billing_process_details['results'][$invoice->clientId]['post_status'] = 'N/A';

				if (isset($invoice->lastError) && $invoice->lastError != '') {
					# If $invoice->lastError is set, this means that the API call to create an invoice has failed
					# Log error for reporting
					$billing_process_details['results'][$invoice->clientId]['status'] = 'error';
					$billing_process_details['results'][$invoice->clientId]['invoice_status'] = 'error';
					$billing_process_details['results'][$invoice->clientId]['invoice_error_messages'] = $invoice->lastError;
										
					$billing_process_details['invoice_error_count']++;
				
				} else {
					
					# At this point, the invoice has not been created because there are no billable items
					$billing_process_details['results'][$invoice->clientId]['clientName'] = $invoice->clientDetails['name'];
					$billing_process_details['results'][$invoice->clientId]['invoice_status'] = 'not generated';
					$billing_process_details['results'][$invoice->clientId]['additional_info'] = 'No billable items. Invoice has not been created.';
				
					$billing_process_details['invoices_not_generated_count']++;
				}
			
			}
		}

		if ($billing_process_details['invoices_generated_count'] < 1) $result['billing_process_status'] = 'warning';
		if ($billing_process_details['invoice_error_count'] > 0 || $billing_process_details['email_error_count'] > 0 || $billing_process_details['post_error_count'] > 0) $result['billing_process_status'] = 'error';

		# JSON encode invoices details for reporting
		$result['billing_process_details'] = json_encode($billing_process_details);

		# Add the new record in to the billing_log table
		$sql = "INSERT INTO billing_log (" .
			   "billing_reference, " . 
			   "billing_date_generated, " . 
			   "billing_cycle, " .
			   "billing_process_status, " .
		       "billing_process_details " . 
		       ") VALUES (" .
		       $this->quote($result['billing_reference']) . " , " .
		       $this->quote($result['billing_date_generated']) . " , " .
		       $this->quote($result['billing_cycle']) . " , " .
		       $this->quote($result['billing_process_status']) . " , " .
		       $this->quote($result['billing_process_details']) . 
		       " )";
		$this->execsql($sql);

		# If there is an error, return false to display error is UI
		if ($result['billing_process_status'] == 'error') return false;

		# Else return true if no errors
		return true;
	}


	/*
	 * 
	 * This function processes an invoice in Freshbooks
	 * This makes an API call to freskbooks to create and automatically an send invoice
	 * Process responds to the each agency's billing profile 
	 *
	 * @return invoice details
	 */
	function process_invoice($solution, $billing_cycle, $charge_codes, $tax1Name = 'VAT', $tax1Percent = 20) {

		# Reset tax details if client is non-VAT
		if ($solution['billing_details']['agency_vat'] == 'N') {
			$tax1Name = '';
			$tax1Percent = 0;
		}

		# If solution total transactions has not been calculated, reset to 0
		if (!isset($solution['total_transactions'])) $solution['total_transactions'] = 0;

		# Initialise a new invoice
		$invoice = new FreshBooks_Invoice();
		$invoice->clientId = $solution['billing_details']['freshbooks_clientId'];
		
		$invoice->contacts = array();

		if (!is_null($solution['billing_details']['agency_freshbooks_sendto_list']) && $solution['billing_details']['agency_freshbooks_sendto_list'] != '') {
			$contacts_list = json_decode($solution['billing_details']['agency_freshbooks_sendto_list']);
			if (count($contacts_list) > 0) {
				foreach ($contacts_list as $contact) {
					array_push($invoice->contacts,array('contact_id' => $contact->contact_id));
				}
			}
			
		}

		$invoice->lines = array();

		# Insert an item for Growing agency plan.
		if (trim($solution['billing_details']['agency_billing_plan']) == 'Growing') {
			# Initialise invoice item array
			$commitment = number_format($solution['billing_details']['agency_minimum_commitment']) . ' transactions';
			if (($solution['billing_details']['agency_minimum_commitment']) == 0) $commitment = '0 transaction';
			$service_fee = array(
				'name'=>$charge_codes[104]['name'],
		        'description'=>sprintf($charge_codes[104]['description'],$billing_cycle,$commitment),
		        'unitCost'=>$solution['billing_details']['agency_solution_service_fee'],
		        'quantity'=>1,
		        'tax1Name'=>$tax1Name,
		        'tax1Percent'=>$tax1Percent,
		        'tax2Name'=>'',
		        'tax2Percent'=>0,
		        'amount'=>0,
			);
			# Insert item to end of array
			array_push($invoice->lines,$service_fee);

			# Insert a new line if there are transactions above the minimum commitment
			if ($solution['total_transactions'] > $solution['billing_details']['agency_minimum_commitment']) {
				# Initialise invoice item array
				$excess = $solution['total_transactions'] - $solution['billing_details']['agency_minimum_commitment'];
				$additional_transactions = array(
					'name'=>$charge_codes[105]['name'],
			        'description'=>sprintf($charge_codes[105]['description'],$excess),
			        'unitCost'=>0,
			        'quantity'=>$excess,
			        'tax1Name'=>$tax1Name,
			        'tax1Percent'=>$tax1Percent,
			        'tax2Name'=>'',
			        'tax2Percent'=>0,
			        'amount'=>0,
				);
				# Insert item to end of array
				array_push($invoice->lines,$additional_transactions);
			}

			# Insert a new line if discount should be applied
			if ($solution['billing_details']['agency_discount'] > 0) {
				$discount_value = $solution['billing_details']['agency_solution_service_fee'] * ($solution['billing_details']['agency_discount']/100);
				$discount = array(
					'name'=>$charge_codes[106]['name'],
			        'description'=>sprintf($charge_codes[106]['description'],$solution['billing_details']['agency_discount']),
			        'unitCost'=>-$discount_value,
			        'quantity'=>1,
			        'tax1Name'=>$tax1Name,
			        'tax1Percent'=>$tax1Percent,
			        'tax2Name'=>'',
			        'tax2Percent'=>0,
			        'amount'=>0,
				);
				# Insert item to end of array
				array_push($invoice->lines,$discount);
			}
		}

		# Process price breakpoints/cost per transaction based on monthly transaction commitment
		$price_breaks = array();

		# Reset total transactions to equal minimum monthly commitment if less.
		if ($solution['total_transactions'] < $solution['billing_details']['agency_minimum_commitment']) $solution['total_transactions'] = $solution['billing_details']['agency_minimum_commitment'];

		# Get price broeakpoints from records
		if (!is_null($solution['billing_details']['agency_pb_details'])) $price_breaks = json_decode($solution['billing_details']['agency_pb_details']);

		# Process each price breakspoints
		if (count($price_breaks) > 0) {

			# Initialise basic values
			$count = 0;
			$limit = '';
			$total_pb_value = 0;

			if ($solution['total_transactions'] > 0) {

				# Process each price breakpoints
				foreach ($price_breaks as $price_break) {
					$count++;
					# If client has no minimum transaction commitment,
					# calculate cost per email from 0 transaction
					if (intval($price_break->volume == 0)) {
						
						# Initialise item details
						$name = $charge_codes[100]['name'];
						$description = sprintf($charge_codes[100]['description'], $billing_cycle, intval($solution['billing_details']['agency_minimum_commitment'])/1000000);
						$unitCost = $price_break->unit_cost;
						
						if (isset($price_breaks[$count])) {
							$limit = $price_breaks[$count]->volume;
						} else {
							$limit = 40000000000000000;
						}

						$quantity = $limit;
						if ($limit >  intval($solution['total_transactions'])) {
							$quantity = intval($solution['total_transactions']);
							$solution['total_transactions'] = 0;
						} else {
							$solution['total_transactions'] = intval($solution['total_transactions']) - $limit;
						}

					} else {

						# Process client cost per email after minimum transactions has been taken off
						# Initialise item details
						$name = $charge_codes[101]['name'];
						$description = sprintf($charge_codes[101]['description'],intval($price_break->volume)/1000000);
						$unitCost = $price_break->unit_cost;
						
						if (isset($price_breaks[$count])) {
							$limit = $price_breaks[$count]->volume - $price_breaks[$count-1]->volume;
						} else {
							$limit = 40000000000000000;
						}
						
						$quantity = $limit;
						if ($limit >  intval($solution['total_transactions'])) {
							$quantity = intval($solution['total_transactions']);
							$solution['total_transactions'] = 0;
						} else {
							$solution['total_transactions'] = intval($solution['total_transactions']) - $limit;
						}
					}

					# Initialise invoice item array
					$item = array(
						'name'=>$name,
				        'description'=>$description,
				        'unitCost'=>$unitCost,
				        'quantity'=>$quantity,
				        'tax1Name'=>$tax1Name,
				        'tax1Percent'=>$tax1Percent,
				        'tax2Name'=>'',
				        'tax2Percent'=>0,
				        'amount'=>0,
					);
					array_push($invoice->lines,$item);

					$total_pb_value += $unitCost*$quantity;

					# Break out of loop if there are no more transactions to process
					if (intval($solution['total_transactions']) == 0) break;
				}
			}
			
			# Insert a new line if discount should be applied
			if ($solution['billing_details']['agency_discount'] > 0) {
				$discount_value = $total_pb_value * ($solution['billing_details']['agency_discount']/100);
				$discount = array(
					'name'=>$charge_codes[107]['name'],
			        'description'=>sprintf($charge_codes[107]['description'],$solution['billing_details']['agency_discount']),
			        'unitCost'=>-$discount_value,
			        'quantity'=>1,
			        'tax1Name'=>$tax1Name,
			        'tax1Percent'=>$tax1Percent,
			        'tax2Name'=>'',
			        'tax2Percent'=>0,
			        'amount'=>0,
				);
				# Insert item to end of array
				array_push($invoice->lines,$discount);
			}
		}


		# Check if client is signed up to unlimited plan
		if (trim($solution['billing_details']['agency_billing_plan']) == 'Unlimited') { 
			
			if (isset($solution['additional_instances'])) {
/*				
				# Add a line to show what the main solution instance used
				# invoice looks confusing without it as it doesn't total correctly
				$array_keys = array_keys($solution['solution_totals']);
				
				$description = sprintf($charge_codes[102]['description'], 
									   number_format($solution['solution_totals'][$array_keys[0]]['total_transactions']), 
									   $solution['billing_details']['agency_litmus_allocation']);				
				
				$item = array(
					'name'=>substr($solution['solution_totals'][$array_keys[0]]['control_panel_url'], stripos($solution['solution_totals'][$array_keys[0]]['control_panel_url'],'//')+2),
					'description'=>$description,
					'unitCost'=>0,
					'quantity'=>1,
					'tax1Name'=>$tax1Name,
					'tax1Percent'=>$tax1Percent,
					'tax2Name'=>'',
					'tax2Percent'=>0,
					'amount'=>0,
				);
				
				array_push($invoice->lines,$item);				
*/				
				
				# Insert additional instances limit
				# Additional instances are charge at £50 per instance
				$instances = $solution['additional_instances'];

				# Process if there any instance set up for the client
				if (count($instances) > 0) {
					foreach ($instances as $instance) {
						# Insert instances as item
						# Initialise item details
						$name = $charge_codes[102]['name'];
						$litmus = $instance['litmus_allocation'] . ' Litmus tests';
						if ($instance['litmus_allocation'] < 2) $litmus = $instance['litmus_allocation'] . ' Litmus test';
						$transactions = number_format($instance['transactions']) . ' transactions';
						if ($instance['transactions'] < 2) $transactions = number_format($instance['transactions']) . ' transaction';
						$description = sprintf($charge_codes[102]['description'], $transactions, $litmus);
						$unitCost = $solution['billing_details']['agency_additional_instances_cost'];

						# Initialise invoice item array
						$item = array(
							'name'=>$instance['name'],
					        'description'=>$description,
					        'unitCost'=>$unitCost,
					        'quantity'=>1,
					        'tax1Name'=>$tax1Name,
					        'tax1Percent'=>$tax1Percent,
					        'tax2Name'=>'',
					        'tax2Percent'=>0,
					        'amount'=>0,
						);
						array_push($invoice->lines,$item);
					}
				}
			}
		}
		

		# Insert solution litmus charges
		# Initialise item details
		$name = $charge_codes[103]['name'];
		if (isset($solution['litmus_instances_total_allocations'])) {
			$litmus_total_allocations = $solution['billing_details']['agency_litmus_allocation'] + $solution['litmus_instances_total_allocations'];
		} else {
			$litmus_total_allocations = $solution['billing_details']['agency_litmus_allocation'];			
		}
		$litmus = $litmus_total_allocations . ' tests';
		if ($litmus_total_allocations < 2) $litmus = $litmus_total_allocations . ' test';
		$description = sprintf($charge_codes[103]['description'],$litmus);
		$litmus_quantity = 0;
		$litmus_total = $solution['inbox_inspections'];
		if ($litmus_total > $litmus_total_allocations) $litmus_quantity = $litmus_total - $litmus_total_allocations;

		# Initialise invoice item array
		$solution_litmus_fee = array(
			'name'=>$name,
	        'description'=>$description,
	        'unitCost'=>1,
	        'quantity'=>$litmus_quantity,
	        'tax1Name'=>$tax1Name,
	        'tax1Percent'=>$tax1Percent,
	        'tax2Name'=>'',
	        'tax2Percent'=>0,
	        'amount'=>0,
		);
		
		array_push($invoice->lines,$solution_litmus_fee);
		
		# Initialise additional information to push in to the each invoice object
		$invoice->clientDetails = $solution;
		$invoice->sentByEmail = FALSE;
		$invoice->sentBySnailMail = FALSE;
		$invoice->createAttempts = 0;

		# Try to create new invoice with provided data on FB server if agency billing status is set to be active
		if ($solution['billing_details']['agency_billing_status_active'] == 'Y') {
			# Invoices are generated for only for agencies with service fee over £0 or has billable litmus tests 
			if ($solution['billing_details']['agency_solution_service_fee'] != 0 || $litmus_quantity != 0) {

				# Wrapping the invoice create function in a while loop 
				# to allow 3 tries to create an invoice if error occurs
				while ($invoice->invoiceId == '') {
					
					# Create invoice in freshbooks
					$invoice->create();

					$invoice->createAttempts++;

					# Attempt only 3 times.
					if ($invoice->createAttempts >= 3) break;

					# Delay 1/2 a second before trying again
					usleep(500000);
				}

				# Check if the invoice needs to be sent out automatically by email/post
				if (count($invoice->contacts) > 0) $invoice->sentByEmail = $invoice->sendByEmail();
				if ($solution['billing_details']['agency_post_invoices'] == 'Y') $invoice->sentBySnailMail = $invoice->sendBySnailMail();
			} 
		}

		# Load invoice details if it has been created
		# This is needed for reporting later
		if ($invoice->invoiceId != '') $invoice->get($invoice->invoiceId);

		# At this point, the $invoice object should contain all the data for reporting later
		return $invoice;
	}

	
	/*
	 * 
	 * This function will generate litmus allocation options
	 * 
	 * @return an array
	 */
	function generate_litmus_allocation_options()
	{
		$return_array = array();

		$max_value = 100;
		
		for ($i=0 ; ; $i++) {
			
			//$value = $i;
			//$description = $i . ' tests included as standard.';
			//if ($i == 1) $description = $i . ' test included as standard';
			
			$return_array['litmus_allocation_options'][] = array('identifier' => $i, 'description' => $i);
			
			if ($i == $max_value) break;

		}
		return $return_array;
	}

	/*
	 * 
	 * This function will generate a selection of monthly transactions
	 *
	 * @return an array
	 */
	function generate_commitment_selections()
	{
		$return_array = array();
		$factor = 100000;
		$max_value = 30000000;
		for ($i=0 ; ; $i++) {
			
			$value = $i*$factor;
			$description = number_format($value);
			$return_array['monthly_commitment'][] = array('identifier' => $value, 'description' => $description);
			
			if ($value >= 1000000) {
				$return_array['additional_transactions'][] = array('identifier' => $value, 'description' => '> ' .$description);
			}

			if ($value == 1000000) {
				$i = 1;
				$factor = 1000000;
			}

			if ($value >= 1000000 && $value <= 5000000) {
				$value = $value + 500000;
				$description = number_format($value);
				$return_array['monthly_commitment'][] = array('identifier' => $value, 'description' => $description);
				$return_array['additional_transactions'][] = array('identifier' => $value, 'description' => '> ' .$description);
			}

			if ($value == $max_value) break;

		}
		return $return_array;
	}

	/*
	 * This function will generate current service fees for growing agencies
	 *
	 * @return an array
	 */
	function get_current_instiller_service_fees()
	{
		$return_array = array();
		
		$return_array[] = array(
			'identifier' => 0, 
			'description' => htmlentities('£0.00'),
			'commitment' => 0
		);
		$return_array[] = array(
			'identifier' => 325, 
			'description' => htmlentities('£325.00'),
			'commitment' => 100000
		);
		$return_array[] = array(
			'identifier' => 650, 
			'description' => htmlentities('£650.00'),
			'commitment' => 200000
		);
		$return_array[] = array(
			'identifier' => 750, 
			'description' => htmlentities('£750.00'),
			'commitment' => 300000
		);
		$return_array[] = array(
			'identifier' => 800, 
			'description' => htmlentities('£800.00'),
			'commitment' => 400000
		);
		$return_array[] = array(
			'identifier' => 850, 
			'description' => htmlentities('£850.00'),
			'commitment' => 500000
		);
		$return_array[] = array(
			'identifier' => 900, 
			'description' => htmlentities('£900.00'),
			'commitment' => 600000
		);
		$return_array[] = array(
			'identifier' => 950, 
			'description' => htmlentities('£950.00'),
			'commitment' => 700000
		);
		$return_array[] = array(
			'identifier' => 1000, 
			'description' => htmlentities('£1,000.00'),
			'commitment' => 800000
		);
		$return_array[] = array(
			'identifier' => 1050, 
			'description' => htmlentities('£1,050.00'),
			'commitment' => 900000
		);
		$return_array[] = array(
			'identifier' => 1100, 
			'description' => htmlentities('£1,100.00'),
			'commitment' => 1000000
		);
		return $return_array;
	}

	function get_billing_details_selections()
	{
		$selection = array();
		$selection['service_fees'] = $this->get_current_instiller_service_fees();
		$selection['commitment'] = $this->generate_commitment_selections();
		$selection['litmus'] = $this->generate_litmus_allocation_options();
		return $selection;
	}

	/*
	 * This function will generate charge codes for billing
	 *
	 */
	function get_charge_codes() 
	{
		$return_array = array();
		
		$return_array[100] = array(
			'identifier' => '100', 
			'name' => 'Instiller',
			'description' => 'Service fee for %s. Based on minimum commitment of %.1f million transactions.'
		);
		$return_array[101] = array(
			'identifier' => '101', 
			'name' => 'Instiller+',
			'description' => 'Additional transactions over %.1f million.'
		);
		$return_array[102] = array(
			'identifier' => '102', 
			'name' => 'Additional Instance',
			'description' => '%s used. %d Litmus tests included as standard.'
		);
		$return_array[103] = array(
			'identifier' => '103', 
			'name' => 'Litmus',
			'description' => 'Additional Litmus tests. %s included as standard.'
		);
		$return_array[104] = array(
			'identifier' => '104', 
			'name' => 'Instiller',
			'description' => 'Service fee for %s. Including %s.'
		);
		$return_array[105] = array(
			'identifier' => '105', 
			'name' => 'Instiller+',
			'description' => 'Additional transactions used.'
		);
		$return_array[106] = array(
			'identifier' => '106', 
			'name' => 'Instiller+',
			'description' => '%d%% discount on service fee.'
		);
		$return_array[107] = array(
			'identifier' => '107', 
			'name' => 'Instiller+',
			'description' => '%d%% discount on total transactions fees.'
		);

		return $return_array;
	}
}



?>