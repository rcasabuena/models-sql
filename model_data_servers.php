<?php
class data_servers extends model_base_class
{
	# Class properties
	public $data_server_id;
	public $data_server_name;
	public $data_server_url;
	
	/*
	 * This function will return the PROCESSLIST information for each dataserver
	 */
	function data_server_status(){
		$data_set = array();
		
		$sql = "SELECT 
				  a.host_name AS master_name,
				  b.host_name AS slave_name 
				FROM
				  server_names a 
				  LEFT JOIN server_names b 
				    ON a.partner_id = b.host_id 
				WHERE a.host_type = 'MASTER' ";
		
		$hosts = $this->execsql($sql);
		
		while(($host = $hosts->FetchRow()) !== false){
			$profile_db = NewADOConnection(DATABASE_DRIVER);
			# Connect to the database
			# Get the information for the master
			if (!@$profile_db->Connect($host['master_name'], 
								   	DATABASE_USER, 
								   	DATABASE_PASSWORD, 
								   	'')) {
			} else {	
				# Initialise the data array
				$data_set[$host['master_name']] = array();
				$data_set[$host['master_name']]['host_name'] = $host['master_name'];
				$data_set[$host['master_name']]['host_type'] = 'MASTER';
				$data_set[$host['master_name']]['slave_name'] = $host['slave_name'];
				
				# Generate the SQL
				$sql = " SHOW FULL PROCESSLIST "; 
				$records = $profile_db->execute($sql);
				
				while(($record = $records->FetchRow()) !== false){
					# convert the time (in seconds into a more readable format
					$record['hms_time'] = date("H:i:s", -3600 + $record['Time']);
					$data_set[$host['master_name']][] = $record;
				}
			}	
			
			# Get the information for the slave
			if(isset($host['slave_name']))
			{ 
				if (!@$profile_db->Connect($host['slave_name'], 
									   	DATABASE_USER, 
									   	DATABASE_PASSWORD, 
									   	'')) {
				} else {	
					# Initialise the data array
					$data_set[$host['slave_name']] = array();
					$data_set[$host['slave_name']]['host_name'] = $host['slave_name'];
					$data_set[$host['slave_name']]['host_type'] = 'SLAVE';
					$data_set[$host['slave_name']]['slave_name'] = false;
					
					# Generate the SQL
					$sql = " SHOW FULL PROCESSLIST "; 
					$records = $profile_db->execute($sql);
					
					while(($record = $records->FetchRow()) !== false){
						# convert the time (in seconds into a more readable format
						$record['hms_time'] = date("H:i:s", -3600 + $record['Time']);
						$data_set[$host['slave_name']][] = $record;
					}
				}
			}
		}	
			
		return $data_set;
	}
	
	function get_slave_stats(){
		$data_set = array();
		$sql = "SELECT 
				  a.host_name, 
				  b.host_name AS master_name
				FROM
				  server_names a, server_names b
				WHERE a.partner_id = b.host_id
				AND a.host_type = 'SLAVE'";
		$hosts = $this->execsql($sql);
		
		while(($host = $hosts->FetchRow()) !== false){
			# Only connect to the slave servers
			$profile_db = NewADOConnection(DATABASE_DRIVER);
			# Connect to the database
			if (!@$profile_db->Connect($host['host_name'], 
								   	DATABASE_USER, 
								   	DATABASE_PASSWORD, 
								   	'')) {
			} else {	
				# Initialise the data array
				$data_set[$host['host_name']] = array();
				$data_set[$host['host_name']]['host_name'] = $host['host_name'];
				$data_set[$host['host_name']]['master_name'] = $host['master_name'];
				# Generate the SQL
				$sql = " SHOW SLAVE STATUS "; 
				$records = $profile_db->execute($sql);
				
				while(($record = $records->FetchRow()) !== false){
					$data_set[$host['host_name']][] = $record;	
				}
			}	
		}
		return $data_set;
	}
	
	function kill_process($process_id, $host_name){
		$profile_db = NewADOConnection(DATABASE_DRIVER);
		# Connect to the database
		if (!@$profile_db->Connect($host_name, 
							   	DATABASE_USER, 
							   	DATABASE_PASSWORD, 
							   	DATABASE_DB)) {
			return false;
		} else {
			$sql = "KILL " . $process_id;
			$profile_db->execute($sql);
			return true;
		}
	}
		
}
?>