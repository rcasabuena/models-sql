<?php
/*
 * DESCRIBE THE USE OF THE MODEL HERE
 */
class reporting extends model_base_class
{
	/*
	 * @return nothing
	 */
	function __construct(){
		# Call the parent constructor, this is VERY IMPORTANT as this will ensure all
		# of the relevant properties and classes are created as part of the core
		# framework, this minimises the code needed at the application level
		parent::__construct ();
	}
		
	/*
	 * Returns an array of all profile types and their translations
	 * 
	 * @return array
	 */
	function get_all_agency_types()
	{
		$return_array = array();
		$return_array[] = array('identifier' => 'EMAIL_SOLUTIONS', 'description' => 'All Clients');
		$return_array[] = array('identifier' => 'ALL', 'description' => 'All');
		$return_array[] = array('identifier' => 'BLACKSUN', 'description' => 'Blacksun');
		$return_array[] = array('identifier' => 'BLUETOAD', 'description' => 'Bluetoad');
		$return_array[] = array('identifier' => 'ELOCATION', 'description' => 'Elocation');		
		$return_array[] = array('identifier' => 'INBOX_WARRIORS', 'description' => 'Inbox Warriors');						
		$return_array[] = array('identifier' => 'INSTILLER', 'description' => 'Instiller');
		$return_array[] = array('identifier' => 'PROFUSION', 'description' => 'Profusion');
		$return_array[] = array('identifier' => 'RESPONSE_GENIUS', 'description' => 'Response Genius');		
		
		return $return_array;
	}

	/*
	 * Returns an array of all dataservers and their translations
	 * 
	 * @return array
	 */
	function get_all_database_servers()
	{
		$return_array = array();
		$return_array[] = array('identifier' => 'ANIMAL', 'description' => 'Animal / Rowlf');
		$return_array[] = array('identifier' => 'BUNSEN', 'description' => 'Bunsen / Beaker');
		$return_array[] = array('identifier' => 'HOGTHROB', 'description' => 'Hogthrob / Strangepork');	
		$return_array[] = array('identifier' => 'STATLER', 'description' => 'Statler / Waldorf');		
		
		return $return_array;
	}

	/**
	 * This function will load the triggered spam assassin rules across all of the 
	 * solutions allowing user friendly descriptions to be added 
	 */
	function sa_rules() 
	{
		# Initiailse the collection for the rules 
		$rules = array();

		# Load all of the email templates within the last year 
		$sql = 	" SELECT email_content_spam_check_information FROM email_content " . 
				" WHERE email_content_date_modified >= DATE_SUB(NOW(),INTERVAL 1 YEAR)";
		$activity = $this->exec_agency_sql('', $sql);

		$custom_spam_assassin_descriptions = array(); 
		
		# Composition
		$custom_spam_assassin_descriptions['MIME_HTML_ONLY'] = 'An increased number of emails may get filtered when not including a text version for users that cannot accept or view HTML. <a href="/help/templates/creating_testing_sending/email_template_checklist#composition">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['HTML_FONT_FACE_BAD'] = 'An unrecognisable font is referenced in the HTML code. <a href="/help/templates/creating_testing_sending/email_template_checklist#composition">Click here</a> for more info.';		
		$custom_spam_assassin_descriptions['MPART_ALT_DIFF_COUNT'] = 'The text and HTML versions look very different. This may lead to an increase in the number of filtered messages. <a href="/help/templates/creating_testing_sending/email_template_checklist#composition">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['MPART_ALT_DIFF'] = 'The text and HTML versions look very different. This may lead to an increase in the number of filtered messages. <a href="/help/templates/creating_testing_sending/email_template_checklist#composition">Click here</a> for more info.';		
		$custom_spam_assassin_descriptions['HTML_SHORT_LINK_IMG_3'] = 'HTML coding is very short and links to an image. Expand the content to reduce the risk of blocking and messages being filtered. <a href="/help/templates/creating_testing_sending/email_template_checklist#composition">Click here</a> for more info.';		
		$custom_spam_assassin_descriptions['HTML_COMMENT_SAVED_URL'] = 'HTML appears to be a saved web page. Email templates need to be coded properly to ensure they display correctly on desktop, web and mobile devices. <a href="/help/templates/creating_testing_sending/email_template_checklist#composition">Click here</a> for more info.';				
						
		# Images
		$custom_spam_assassin_descriptions['HTML_IMAGE_RATIO_02'] = 'Consider using more text. Users with images turned "off" may find it difficult to read your message. <a href="/help/templates/creating_testing_sending/email_template_checklist#images">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['HTML_IMAGE_RATIO_04'] = 'Consider using more text. Users with images turned "off" may find it difficult to read your message. <a href="/help/templates/creating_testing_sending/email_template_checklist#images">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['HTML_IMAGE_RATIO_06'] = 'There are way too many images being used. Users with images turned "off" will find it difficult to read your message. <a href="/help/templates/creating_testing_sending/email_template_checklist#images">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['HTML_IMAGE_RATIO_08'] = 'There are way too many images being used. Users with images turned "off" will find it difficult to read your message. <a href="/help/templates/creating_testing_sending/email_template_checklist#images">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['HTML_IMAGE_ONLY_04'] = 'One or more of your images are too large. Users on mobile or slow Internet connections may find downloading your message too slow. <a href="/help/templates/creating_testing_sending/email_template_checklist#images">Click here</a> for more info.';		
		$custom_spam_assassin_descriptions['HTML_IMAGE_ONLY_08'] = 'One or more of your images are too large. Users on mobile or slow Internet connections may find downloading your message too slow. <a href="/help/templates/creating_testing_sending/email_template_checklist#images">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['HTML_IMAGE_ONLY_12'] = 'One or more of your images are too large. Users on mobile or slow Internet connections may find downloading your message too slow. <a href="/help/templates/creating_testing_sending/email_template_checklist#images">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['HTML_IMAGE_ONLY_16'] = 'One or more of your images are too large. Users on mobile or slow Internet connections may find downloading your message too slow. <a href="/help/templates/creating_testing_sending/email_template_checklist#images">Click here</a> for more info.';		
		$custom_spam_assassin_descriptions['HTML_IMAGE_ONLY_18'] = 'One or more of your images are too large. Users on mobile or slow Internet connections may find downloading your message too slow. <a href="/help/templates/creating_testing_sending/email_template_checklist#images">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['HTML_IMAGE_ONLY_20'] = 'One or more of your images are too large. Users on mobile or slow Internet connections may find downloading your message too slow. <a href="/help/templates/creating_testing_sending/email_template_checklist#images">Click here</a> for more info.';		
		$custom_spam_assassin_descriptions['HTML_IMAGE_ONLY_24'] = 'One or more of your images are too large. Users on mobile or slow Internet connections may find downloading your message too slow. <a href="/help/templates/creating_testing_sending/email_template_checklist#images">Click here</a> for more info.';						
		$custom_spam_assassin_descriptions['HTML_IMAGE_ONLY_28'] = 'One or more of your images are too large. Users on mobile or slow Internet connections may find downloading your message too slow. <a href="/help/templates/creating_testing_sending/email_template_checklist#images">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['HTML_IMAGE_ONLY_32'] = 'One or more of your images are too large. Users on mobile or slow Internet connections may find downloading your message too slow. <a href="/help/templates/creating_testing_sending/email_template_checklist#images">Click here</a> for more info.';				
		
		# Subject line & content
		$custom_spam_assassin_descriptions['SUBJ_ALL_CAPS'] = 'Subject lines using ALL CAPITALS not ony look like Spam but could also make users feel as if they are being shouted at. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['PLING_QUERY'] = 'Subject line contains exclamation and question marks. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['T_LOTS_OF_MONEY'] = 'Consider rewording all content that describes large sums of money. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['LOTS_OF_MONEY'] = 'Consider rewording all content that describes large sums of money. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['LOW_PRICE'] = 'Consider rewording all content that describes really low prices. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';				
		$custom_spam_assassin_descriptions['HTML_TITLE_SUBJ_DIFF'] = 'HTML title and subject line are different. Use the same text on both to reduce the risk of messages being filtered. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['FILL_THIS_FORM_LONG'] = 'Requests to provide personal information through filling out forms look like scam or phising attempts. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['FILL_THIS_FORM_FRAUD_PHISH'] = 'Requests to provide personal information through filling out forms look like scam or phising attempts. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';		
		$custom_spam_assassin_descriptions['ACT_NOW_CAPS'] = 'Asking people to "ACT NOW" is very bad practice and will lead to messages being blocked or filtered. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';								
		$custom_spam_assassin_descriptions['FUZZY_AMBIEN'] = 'Words appear to be obfuscated to disguise Spam like phrases. Consider rewording to make the content sound more authentic. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['FUZZY_XPILL'] = 'Words appear to be obfuscated to disguise Spam like phrases. Consider rewording to make the content sound more authentic. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['PRICES_ARE_AFFORDABLE'] = 'Consider rewording all content that describes something as being unbelievably affordable. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['BAD_CREDIT'] = 'Consider rewording all content that describes being able to eliminate bad credit. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['DEAR_SOMETHING'] = '"Dear <something>" is used regularly by Spam emails, consider rewording and always use personalisation rather than generics such as Sir or Madam. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';						
		$custom_spam_assassin_descriptions['GAPPY_SUBJECT'] = 'Subject line contains "gappy" text that may be viewed as Spam by mail filters. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';		
		$custom_spam_assassin_descriptions['UPPERCASE_50_75'] = 'More than 50% of the content is uppercase. This may lead to an increased number of messages being filtered.. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';		
		$custom_spam_assassin_descriptions['MONEY_BACK'] = 'Consider rewording all content that describes a money back guarantee. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';		
		$custom_spam_assassin_descriptions['SUBJ_BUY'] = 'Subject lines starting with the word "Buy" sound like Spam. Consider rewording to reduce the risk of messages being filtered. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';		
		$custom_spam_assassin_descriptions['DEAR_FRIEND'] = 'If someone is your friend you would know their name. Avoid using phrases like "Dear Friend". <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';				
		$custom_spam_assassin_descriptions['BANG_GUAR'] = 'Content that describes an emphatic guarantee sounds very suspect. This could lead to an increase in the number of filtered messages. <a href="/help/templates/creating_testing_sending/email_template_checklist#content">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['HTTP_ESCAPED_HOST'] = 'Possible incorrectly encoded or escaped text detected within links. Search for any links that contain \'%\' symbols.';
		$custom_spam_assassin_descriptions['URL_ANCHORED_TEXT'] = 'Using a website address as text to link from is only advisable when click tracking is disabled. <a href="/help/templates/specific_features/link_customisation#click_tracking">Click here</a> for more info.';		
		
		# Sending domain
		$custom_spam_assassin_descriptions['NO_DNS_FOR_FROM'] = 'Sending domain is not configured correctly. Use the <a href="/reputation_monitor">Reputation Monitor</a> to resolve isses with DNS records. <a href="/help/templates/creating_testing_sending/email_template_checklist#fromaddress">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['DKIM_ADSP_ALL'] = 'Sending domain is not configured correctly. Use the <a href="/reputation_monitor">Reputation Monitor</a> to resolve isses with DNS records. <a href="/help/templates/creating_testing_sending/email_template_checklist#fromaddress">Click here</a> for more info.';		
		$custom_spam_assassin_descriptions['DKIM_ADSP_NXDOMAIN'] = 'Sending domain is not configured correctly. Use the <a href="/reputation_monitor">Reputation Monitor</a> to resolve isses with DNS records. <a href="/help/templates/creating_testing_sending/email_template_checklist#fromaddress">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['FH_FROM_GET_NAME'] = 'Avoid using the word "get" in the from name. Users may think it sounds like Spam. <a href="/help/templates/creating_testing_sending/email_template_checklist#fromaddress">Click here</a> for more info.';
		
		# External		
		$custom_spam_assassin_descriptions['URIBL_RHS_DOB'] = 'References a domain that is less than 5 days old detected. Mail servers treat new domains with caution and could lead to an increase in filtered messages. <a href="/help/templates/creating_testing_sending/email_template_checklist#external">Click here</a> for more info.';		
		$custom_spam_assassin_descriptions['URIBL_BLACK'] = 'A domain listed on the <a href="http://www.uribl.com">realtime URI blacklist</a> detected in a link to a website or an image. <a href="/help/templates/creating_testing_sending/email_template_checklist#external">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['URIBL_GREY'] = 'A domain listed on the <a href="http://www.uribl.com">realtime URI greylist</a> detected in a link to a website or an image. <a href="/help/templates/creating_testing_sending/email_template_checklist#external">Click here</a> for more info.';										
		$custom_spam_assassin_descriptions['URIBL_WS_SURBL'] = 'A domain listed on the <a href="http://www.surbl.org/surbl-analysis">SURBL blocklist</a> detected in a link to a website or an image. <a href="/help/templates/creating_testing_sending/email_template_checklist#external">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['URIBL_DBL_SPAM'] = 'A domain listed on the <a href="http://www.spamhaus.org/dbl/">Spamhaus DBL blocklist</a> detected in a link to a website or an image. <a href="/help/templates/creating_testing_sending/email_template_checklist#external">Click here</a> for more info.';
		$custom_spam_assassin_descriptions['URIBL_SBL'] = 'A domain listed on the <a href="http://www.spamhaus.org/sbl/">Spamhaus SBL blocklist</a> detected in a link to a website or an image. <a href="/help/templates/creating_testing_sending/email_template_checklist#external">Click here</a> for more info.';						
		
		
		# Loop through all of the agencies 
		$minimum_score = 0.1;
		foreach ($activity as $agency) {
			if (isset($agency['data'])) {
				foreach ($agency['data'] as $record) {

					# Make sure there is some content to be parsed
					if (trim($record['email_content_spam_check_information']) != '') {

						# We should loop through the spam assassin information and 
						# turn it into something easier to manuipulate within the templates
						$header_found = false;
						$sa_test_results = preg_split('/\n/', trim($record['email_content_spam_check_information']));
						foreach ($sa_test_results as $sa_test_result_detail) {
							if ($header_found == false) {
								# Check for the heading option 
								if (strpos($sa_test_result_detail, '-------') !== false) {
									$header_found = true;
								}
							} else {
								# We need to handle the scenario where there are continuation lines 
								# we should be able to detect this because the score is always the 
								# first item on the rule line... it not it's a continuation. 
								$parts = preg_split('/ /', trim($sa_test_result_detail));
								if (isset($parts[0]) && trim($parts[0]) != '') {
									# Check to see if this is numeric 
									if (is_numeric($parts[0]) == false) {
										# This is a continuation message! tag to the previous					
										#$previous_index = count($this->validation['spam_assassin']['feedback']) -1;
										#$this->validation['spam_assassin']['feedback'][$previous_index]['description'] .= ' ' . trim($sa_test_result_detail); 
									} else {							
										# Parse the contents of the rule
										$spam_assassin_test = array();
										$sa_test_result_detail = trim($sa_test_result_detail);
										$split_pos = strpos($sa_test_result_detail, ' ');
										
										$spam_assassin_test['score'] = trim(substr($sa_test_result_detail, 0, $split_pos));
										$sa_test_result_detail = trim(substr($sa_test_result_detail, $split_pos, strlen($sa_test_result_detail)));
										$split_pos = strpos ( $sa_test_result_detail, ' ' );
										
										$spam_assassin_test['rule'] = trim (substr($sa_test_result_detail, 0, $split_pos));
										$spam_assassin_test['description'] = trim(substr($sa_test_result_detail, $split_pos, strlen ($sa_test_result_detail)));
										
										# Check to see if we can find the optional section within the description 
										$spam_assassin_test['section'] = '';
										$split_pos = strpos($spam_assassin_test['description'], ' ');
										$possible_section = trim(substr($spam_assassin_test['description'], 0, $split_pos));
										$actual_description = trim(substr($spam_assassin_test['description'], $split_pos, strlen($spam_assassin_test['description'])));
										# The section must contain the : symbol and should be upper case
										if (strpos($possible_section,":") !== false) {
											if($possible_section == strtoupper($possible_section)) {
												# Looks like we can split out the section and the description 
												$spam_assassin_test['section'] = str_replace(":","",$possible_section);
												$spam_assassin_test['description'] = $actual_description;
											}
										}

										# Check to see if we need to add the rule to the collection 
										if ($spam_assassin_test['score'] > $minimum_score) {
											if (!isset($rules[$spam_assassin_test['rule']])) {
												# Store the rule in the collection
												$rules[$spam_assassin_test['rule']] = array();
												$rules[$spam_assassin_test['rule']]['rule'] = $spam_assassin_test['rule'];
												$rules[$spam_assassin_test['rule']]['description'] = $spam_assassin_test['description'];
												$rules[$spam_assassin_test['rule']]['count'] = 1; 
												$rules[$spam_assassin_test['rule']]['custom'] = false;
	
												# Check to see if we can apply a custom description to the rule 
												if (isset($custom_spam_assassin_descriptions[$spam_assassin_test['rule']])) {
													$rules[$spam_assassin_test['rule']]['custom'] = true;
													$rules[$spam_assassin_test['rule']]['description'] = $custom_spam_assassin_descriptions[$spam_assassin_test['rule']];
												}
											} else {
												# increment the appearance count
												$rules[$spam_assassin_test['rule']]['count']++; 
											}
										}
									}
								} 
							}
						}				
					}		
				}	
			}
		}

		# We should sort the array using a lam
		usort($rules, function ($rulea, $ruleb) {
			return (($rulea['count'] < $ruleb['count']) ? 1 : -1);
		});

		# Return the rule collection 
		return $rules;
	}
	
	/*
	 * Gets the e-mail activity as an array.
	 * 
	 * 
	 * Array structure:
	 * $return_array[] = 
	 * 'CLIENT' data view:  the key is the profile type, for example Instiller, Response Genius, Inbox Warriors.  An additional total is inserted as a profile type.
	 * 'SUMMARY' data view: the key is each month
	 * $return_array[agency_type]['sent_emails']
	 * $return_array[agency_type]['events_triggered']
	 * $return_array[agency_type]['requests_forwarded']
	 * $return_array[agency_type]['api_impressions']
	 * 'CLIENT' data view only:
	 * $return_array[agency_type]['solution_totals'][solution_name] - contains the totals for each solution, similar to each profile type
	 * 
	 * @param agency_type the type of profile to search for, e.g. EMAIL_SOLUTIONS
	 * @param date_period an array of date values
	 * @param data_view - 'CLIENT' view (default) returns information to each individual client
	 * 
	 * @return array
	 */
	function get_email_solution_activity($agency_type, $date_period, $data_view = 'CLIENT')
	{
		
		# Initialise solution activity array as empty shell
		$agency_type_totals = array();

		$agency_types = $this->get_all_agency_types();
		foreach ($agency_types as $agency) {
			if ($agency['identifier'] == 'EMAIL_SOLUTIONS') {
				$key = 'RESELLERS';
				$name = 'Resellers';
			} else {
				$key = $agency['description'];
				$name = $agency['description'];
			}
			$agency_type_totals[$key]['dataset'] = '';
			$agency_type_totals[$key]['name'] = $name;
			$agency_type_totals[$key]['sent_emails'] = 0;	
			$agency_type_totals[$key]['events_triggered'] = 0;	
			$agency_type_totals[$key]['requests_forwarded'] = 0;	
			$agency_type_totals[$key]['api_impressions'] = 0;	
			$agency_type_totals[$key]['inbox_inspections'] = 0;	
			$agency_type_totals[$key]['total_litmus_test_cost'] = 0;	
			$agency_type_totals[$key]['total_transactions'] = 0;
			$agency_type_totals[$key]['sent_emails_percentage_formatted'] = 0;
			$agency_type_totals[$key]['events_triggered_percentage_formatted'] = 0;
			$agency_type_totals[$key]['requests_forwarded_percentage_formatted'] = 0;	
			$agency_type_totals[$key]['api_impressions_percentage_formatted'] = 0;	
			$agency_type_totals[$key]['solution_totals'] = array();
		}
		
		$data_servers = $this->get_all_database_servers();
		foreach ($data_servers as $server) {
			$agency_type_totals[$server['description']]['dataset'] = '';
			$agency_type_totals[$server['description']]['name'] = $server['description'];
			$agency_type_totals[$server['description']]['sent_emails'] = 0;	
			$agency_type_totals[$server['description']]['events_triggered'] = 0;	
			$agency_type_totals[$server['description']]['requests_forwarded'] = 0;	
			$agency_type_totals[$server['description']]['api_impressions'] = 0;	
			$agency_type_totals[$server['description']]['inbox_inspections'] = 0;	
			$agency_type_totals[$server['description']]['total_litmus_test_cost'] = 0;	
			$agency_type_totals[$server['description']]['total_transactions'] = 0;
			$agency_type_totals[$server['description']]['sent_emails_percentage_formatted'] = 0;
			$agency_type_totals[$server['description']]['events_triggered_percentage_formatted'] = 0;
			$agency_type_totals[$server['description']]['requests_forwarded_percentage_formatted'] = 0;	
			$agency_type_totals[$server['description']]['api_impressions_percentage_formatted'] = 0;	
			$agency_type_totals[$server['description']]['solution_totals'] = array();
		}

		if ($data_view == 'CLIENT') {
			
			$sql = "SELECT a.company_id, a.company_name,   " . 
					"SUM(company_billing_stats_test_emails + company_billing_stats_email_messages + company_billing_stats_event_emails) AS sent_emails,  " . 
					"SUM(company_billing_stats_event_triggers) AS events_triggered,   " . 
					"SUM(company_billing_stats_event_forwards) AS requests_forwarded,   " . 
					"SUM(company_billing_stats_api_impressions) AS api_impressions,   " .
					"SUM(company_billing_stats_inbox_inspections) AS inbox_inspections   " . 
					"FROM company a LEFT JOIN company_billing_stats b ON (a.company_id = b.company_id)  " . 					
					"WHERE b.company_billing_stats_period LIKE '" . $date_period . "%' " . 
					"GROUP BY company_id " . 
					"ORDER BY sent_emails DESC, api_impressions DESC, a.company_name;";			
		} 
		else if($data_view == 'SUMMARY') 
		{
			$sql = "SELECT SUBSTRING(b.company_billing_stats_period, 1, 7) AS company_billing_stats_period, " . 
					"IFNULL(SUM(company_billing_stats_test_emails + " .
				       "company_billing_stats_email_messages + " .
				       "company_billing_stats_event_emails),0) AS sent_emails, " .
				   "IFNULL(SUM(company_billing_stats_event_triggers),0) AS events_triggered, " .
				   "IFNULL(SUM(company_billing_stats_event_forwards),0) AS requests_forwarded, " .
				   "IFNULL(SUM(company_billing_stats_api_impressions),0) AS api_impressions, " .
				   "IFNULL(SUM(company_billing_stats_inbox_inspections),0) AS inbox_inspections   " .						
				   "FROM company a, company_billing_stats b " .
				   "WHERE a.company_id = b.company_id AND " .
				   "b.company_billing_stats_period LIKE '" . $date_period . "%' " .	 
				   " GROUP BY company_billing_stats_period ";	
		}
		else
		{
			die("model_reporting::get_email_solution_activity invalid data_view provided.");
		}

		$activity = $this->exec_agency_sql($agency_type, $sql);

		$sql = " SELECT resellers_litmus_test_cost, resellers_litmus_test_default_cost, resellers_litmus_test_default_cost_currency FROM resellers LIMIT 1 ";
		$litmus_costs = $this->exec_agency_sql($agency_type, $sql);

		# Sum up all of the activity for each solution
		$agency_type_totals['RESELLERS']['dataset'] = 'summary';
		$current_key = '';
		$current_server_key = '';
		
		# Add up all of the providers totals for a yearly total
		if($data_view == 'CLIENT')
		$this->set_name($agency_type_totals['RESELLERS'], 'Resellers');

		foreach($activity as $solution)
		{
			$resellers_litmus_test_cost = $litmus_costs[$solution['agency']['agency_id']]['data'][0]['resellers_litmus_test_cost'];

			if($data_view == 'CLIENT' && $current_key != $this->translate_db_identifier($solution['agency']['agency_type']))
			{
				$current_key = $this->translate_db_identifier($solution['agency']['agency_type']);
				$this->set_name($agency_type_totals[$current_key], $current_key);
				$agency_type_totals[$current_key]['dataset'] = 'solution';
			}
			
			if($data_view == 'CLIENT' && $current_key != $this->translate_db_server($solution['agency']['agency_database_host']))
			{
				$current_server_key = $this->translate_db_server($solution['agency']['agency_database_host']);
				$this->set_name($agency_type_totals[$current_server_key], $current_server_key);
				$agency_type_totals[$current_server_key]['dataset'] = 'db servers';
			}
			
			$agency_id = $solution['agency']['agency_id'];
			$solution_name = $solution['agency']['agency_name'];
			$control_panel_url = $solution['agency']['agency_control_panel_url'];
			$db_server = $this->translate_db_server($solution['agency']['agency_database_host']);
			$db_identifier = $this->translate_db_identifier($solution['agency']['agency_type']);
			$freshbooks_clientId = $solution['agency']['agency_freshbooks_clientid'];
			$post_invoices = $solution['agency']['agency_post_invoices'];
			$billing_plan = $solution['agency']['agency_billing_plan'];
			$billing_status_active = $solution['agency']['agency_billing_status_active'];
			$solution_service_fee = $solution['agency']['agency_solution_service_fee'];
			$minimum_commitment = $solution['agency']['agency_minimum_commitment'];
			$price_breaks = $solution['agency']['agency_pb_details'];
			$sendto = $solution['agency']['agency_freshbooks_sendto_list'];
			$additional_instances_cost = $solution['agency']['agency_additional_instances_cost'];
			$additional_instances_litmus = $solution['agency']['agency_additional_instances_litmus'];
			$direct_relationship = $solution['agency']['agency_direct_relationship'];
			$vat = $solution['agency']['agency_vat'];
			$litmus = $solution['agency']['agency_litmus_allocation'];
			$discount = $solution['agency']['agency_solution_service_fee_discount'];

			# For this solution, get the total activity for all their clients
			if (isset($solution['data'])) {
				foreach($solution['data'] as $solution_client)
				{
					if($data_view == 'SUMMARY')
					{
						if($current_key != $solution_client['company_billing_stats_period'])
						{
							# Create a new entry if the month has changed
							$current_key = $solution_client['company_billing_stats_period'];
							$this->set_name($agency_type_totals[$current_key], $current_key, true);
						}						
						$this->add_stats($agency_type_totals[$current_key], $solution_client);
					}
					else if($data_view == 'CLIENT')
					{						
						$agency_type_totals[$current_key]['solution_totals'][$solution_name]['solution_name'] = $solution_name;
						$this->set_name($agency_type_totals[$current_key]['solution_totals'][$solution_name], $solution_name);

						$agency_type_totals[$current_server_key]['solution_totals'][$solution_name]['solution_name'] = $solution_name;
						$this->set_name($agency_type_totals[$current_server_key]['solution_totals'][$solution_name], $solution_name);
						
						$agency_type_totals[$current_key]['solution_totals'][$solution_name]['agency_type'] = $solution['agency']['agency_type'];
						$this->add_stats($agency_type_totals[$current_key]['solution_totals'][$solution_name],$solution_client);

						$agency_type_totals[$current_server_key]['solution_totals'][$solution_name]['agency_type'] = $solution['agency']['agency_type'];
						$this->add_stats($agency_type_totals[$current_server_key]['solution_totals'][$solution_name],$solution_client);
					}
				}

				if($data_view == 'CLIENT') {
					$this->add_stats($agency_type_totals[$current_key], $agency_type_totals[$current_key]['solution_totals'][$solution_name]);
					$this->add_stats($agency_type_totals[$current_server_key], $agency_type_totals[$current_key]['solution_totals'][$solution_name]);
				}
				
			}
			elseif($data_view == 'CLIENT')
			{
				$this->set_name($agency_type_totals[$current_key]['solution_totals'][$solution_name], $solution_name);
				$this->set_name($agency_type_totals[$current_server_key]['solution_totals'][$solution_name], $solution_name);
			}	
			
			if($data_view == 'CLIENT')
			{
				# Add the total activity for the solution for the total for the current key
				$agency_type_totals['RESELLERS']['solution_totals'][$current_key]['name'] = $current_key;
				$this->add_stats($agency_type_totals['RESELLERS']['solution_totals'][$current_key], $agency_type_totals[$current_key]['solution_totals'][$solution_name]);
			}
			
			if (isset($agency_type_totals[$current_key]['solution_totals'][$solution_name])) {
				$agency_type_totals[$current_key]['solution_totals'][$solution_name]['control_panel_url'] = $control_panel_url;
				$agency_type_totals[$current_key]['solution_totals'][$solution_name]['database_server'] = $db_server;
				$agency_type_totals[$current_key]['solution_totals'][$solution_name]['agency_id'] = $agency_id;
				
				# Add billing details if we have direct relationship
				if ($direct_relationship == 'Y') {
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['freshbooks_clientId'] = $freshbooks_clientId;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_billing_plan'] = $billing_plan;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_billing_status_active'] = $billing_status_active;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_solution_service_fee'] = $solution_service_fee;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_minimum_commitment'] = $minimum_commitment;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_pb_details'] = $price_breaks;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_vat'] = $vat;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_litmus_allocation'] = $litmus;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_additional_instances_cost'] = $additional_instances_cost;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_discount'] = $discount;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_freshbooks_sendto_list'] = $sendto;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_post_invoices'] = $post_invoices;
					if ($agency_type_totals[$current_key]['name'] != 'Instiller') {
						$agency_type_totals[$current_key]['billing_details'] = $agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details'];
					}
				} else {
					$url = substr($control_panel_url,stripos($control_panel_url,'//')+2);
					$agency_type_totals[$current_key]['additional_instances'][$url]['url'] = $url;
					$agency_type_totals[$current_key]['additional_instances'][$url]['name'] = $solution_name;					
					$agency_type_totals[$current_key]['additional_instances'][$url]['litmus'] = $agency_type_totals[$current_key]['solution_totals'][$solution_name]['inbox_inspections'];
					$agency_type_totals[$current_key]['additional_instances'][$url]['litmus_allocation'] = $additional_instances_litmus;
					if (isset($agency_type_totals[$current_key]['solution_totals'][$solution_name]['total_transactions'])) $agency_type_totals[$current_key]['additional_instances'][$url]['transactions'] = $agency_type_totals[$current_key]['solution_totals'][$solution_name]['total_transactions'];
					else $agency_type_totals[$current_key]['additional_instances'][$url]['transactions'] = 0;
					if (!isset($agency_type_totals[$current_key]['litmus_instances_total_allocations'])) $agency_type_totals[$current_key]['litmus_instances_total_allocations'] = $agency_type_totals[$current_key]['additional_instances'][$url]['litmus_allocation'];
					else $agency_type_totals[$current_key]['litmus_instances_total_allocations'] = $agency_type_totals[$current_key]['litmus_instances_total_allocations'] + $agency_type_totals[$current_key]['additional_instances'][$url]['litmus_allocation'];
				}
			}
			if (isset($agency_type_totals[$current_server_key]['solution_totals'][$solution_name])) {
				$agency_type_totals[$current_server_key]['solution_totals'][$solution_name]['control_panel_url'] = $control_panel_url;
				$agency_type_totals[$current_server_key]['solution_totals'][$solution_name]['database_server'] = $db_server;
				$agency_type_totals[$current_server_key]['solution_totals'][$solution_name]['agency_id'] = $agency_id;
				$agency_type_totals[$current_server_key]['solution_totals'][$solution_name]['agency'] = $db_identifier;
			}
		}

		if($data_view == 'CLIENT')
		{
			foreach($agency_type_totals['RESELLERS']['solution_totals'] as $agency_type_total){
				$this->add_stats($agency_type_totals['RESELLERS'], $agency_type_total);
			}
		}	
		else if($data_view == 'SUMMARY')
		{		
			$tmp_array = explode(',', str_replace("'",'',$date_period));
			$tmp_array = array_flip($tmp_array);
			foreach ($tmp_array AS $key => &$item) {
				$item = array();
				$this->set_name($item, $key);
			}		
			$agency_type_totals = array_merge($tmp_array, $agency_type_totals);
		}
		
		# Initialise all instances array
		$agency_type_totals['All'] = $agency_type_totals['RESELLERS'];
		$agency_type_totals['All']['dataset'] = 'solution';
		$agency_type_totals['All']['name'] = 'All';
		$agency_type_totals['All']['solution_totals'] = array();

		foreach ($agency_type_totals as $current_key => $solution) {
			if ($current_key != 'RESELLERS' && $current_key != 'All') {
				$agency_type_totals['All']['solution_totals'] = array_merge($agency_type_totals['All']['solution_totals'], $solution['solution_totals']);
			} 
		}
		return $agency_type_totals;
	}

	
	/*
	 * Function to all fetch billing log records from DB
	 * @returns an array
	 */
	function get_billing_history()
	{
		# Get all billing log for display
		$sql = " SELECT * FROM billing_log ORDER BY billing_id DESC ";
		$records = $this->execsql($sql);

		# Initilise array and go throug all records
		$results = array();

		while (($record = $records->FetchRow()) !== false) {
			$results[$record['billing_id']] = $record;
			# Get results and decode billing process details
			# Details include all invoices details stored as json data in DB
			$results[$record['billing_id']]['billing_process_details_decoded'] = objectToArray(json_decode($record['billing_process_details']));
		}

		return $results;
	}

	/*
	 * Function to fetch billing log records from DB
	 * @returns an array
	 */
	function load_billing_log_details($billing_id)
	{
		# Get billing log for display
		$sql = " SELECT * FROM billing_log WHERE billing_id = " . $billing_id . " LIMIT 1";
		$records = $this->execsql($sql);

		# Get results and decode billing process details
		# Details include all invoices details stored as json data in DB
		$record = $records->FetchRow();
		$record['billing_process_details_decoded'] = objectToArray(json_decode($record['billing_process_details']));

		return $record;
	}

	/*
	 * Function to fetch campaign event log records from DB
	 * @returns an array
	 */
	function load_campaign_event_log_details($campaign_event_id)
	{
		# Get campaign event log for display
		$sql = " SELECT * FROM campaign_events_log WHERE campaign_event_id = " . $campaign_event_id . " LIMIT 1";
		$records = $this->execsql($sql);

		# Get results and decode campaign event details
		# Details include all contacts, their opt-in prefs and event triggers
		$record = $records->FetchRow();
		$record['campaign_event_trigger_details'] = json_decode($record['campaign_event_trigger_details'],true);

		# Initialise results
		$results = array();
		$results['contacts'] = 0;
		$results['triggered'] = 0;
		$results['not_triggered'] = 0;

		# Check if there are records just to make sure
		if (isset($record['campaign_event_trigger_details']) && count($record['campaign_event_trigger_details']) > 0) {
			
			# Loop through all records to analyse how many event was/was not triggered based on user prefs 
			foreach ($record['campaign_event_trigger_details'] as $contact) {
				$results['contacts']++;
				if ($contact['agency_contact_campaign_event_triggered'] == 'Y') $results['triggered']++;
				if ($contact['agency_contact_campaign_event_triggered'] == 'N') $results['not_triggered']++;
			}
		}

		# Assign high level results to render in pop-up
		$record['campaign_event_high_level_results'] = $results;

		# Return record
		return $record;
	}

	
	/*
	 * Function to download an invoicing report correspoding to a billing log
	 * @returns a zip file containing a billing a csv report
	 */
	function download_billing_report($billing_id)
	{
		# Load details for a billing log
		$records = $this->load_billing_log_details($billing_id);

		# First create up a file handle to temporarily store the data  
		$temp_filename = $records['billing_reference'] . '_billing_log.csv';
		$temp_filepath = sys_get_temp_dir () . DIRECTORY_SEPARATOR . $temp_filename;		 
		$fh = fopen( $temp_filepath, 'w+');
		
		# Generate the header row		
		fputcsv($fh, array('Agency','Invoice Number', 'Amount' , 'Invoice Status', 'Create Attempts', 'Sent by Email' , 'Sent by Post' , 'Error', 'Additional Information'));				
		
		# Create the data rows
		# Go through each invoices stored in results array
		foreach($records['billing_process_details_decoded']['results'] as $invoice)
		{
			# Analyse invoice contents
			$invoice_number = 'N/A';
			if ($invoice['invoice_number'] != '') $invoice_number = $invoice['invoice_number'];
			$error = 'N/A';
			if ($invoice['invoice_status'] == 'error') $error = $invoice['invoice_error_messages'];
			if ($invoice['email_status'] == 'error') $error = $invoice['email_error_message'];
			if ($invoice['post_status'] == 'error') $error = $invoice['post_error_messages'];
			$amount = '';
			if ($invoice['amount'] != '') $amount =  $invoice['amount'];
			$add_info = '';
			if (isset($invoice['additional_info'])) $add_info = $invoice['additional_info'];

			fputcsv($fh, array($invoice['clientName'], $invoice_number, $amount, $invoice['invoice_status'], $invoice['create_attempts'], $invoice['sent_by_email'], $invoice['sent_by_post'], $error, $add_info));
		}
		
		# Finished writing, close the file
		fclose($fh);						
		
		# Store the csv in a zip file using a unique id as there  
		# may be multiple zip files being generated concurrently		
		# on the same server
		$zip_filename = uniqid() . '.zip';
		$zip_file_path = sys_get_temp_dir () . DIRECTORY_SEPARATOR . $zip_filename;
		
		$zip = new ZipArchive ( );
		$zip->open ( $zip_file_path, ZipArchive::CREATE );
		$zip->addFile ($temp_filepath, $temp_filename);
		$zip->close ();
		
		$zip_data = file_get_contents ( $zip_file_path );
		unlink ( $zip_file_path );
		unlink ( $temp_filepath);
		
		$zip_filename = $records['billing_reference'].'_billing_log.zip';
		
		// Setup the file header to download the generated zip file
        header("Content-type: application/octet-stream");
        header("Cache-control: private"); // fix for IE
        header("Content-Disposition: filename=$zip_filename");
        header("Content-Length: " . strlen($zip_data));
        header("Pragma: public");
        
        # Make sure there is nothing in the output buffer which could corrupt the ZIP archive
        ob_clean();
        
		echo $zip_data;
	}


	/*
	 * Function to download the campaign event report correspoding to a campaign event log
	 * @returns a zip file containing a campaign a csv report
	 */
	function download_campaign_event_report($campaign_event_id)
	{
		# Load details for a campaign event log
		$records = $this->load_campaign_event_log_details($campaign_event_id);

		# First create up a file handle to temporarily store the data  
		$temp_filename = $records['campaign_event_reference'] . '_campaign_event_log.csv';
		$temp_filepath = sys_get_temp_dir () . DIRECTORY_SEPARATOR . $temp_filename;		 
		$fh = fopen( $temp_filepath, 'w+');
		
		# Generate the header row		
		fputcsv($fh, array('Name','Email', 'Company' , 'Event Triggered', 'Opt-In Global', 'Opt-In List'));				
		
		# Create the data rows
		# Go through each data in campaign_event_trigger_details array
		foreach($records['campaign_event_trigger_details'] as $contact)
		{
			fputcsv($fh, array($contact['agency_contact_name'],$contact['agency_contact_email'],$contact['agency_name'],$contact['agency_contact_campaign_event_triggered'],$contact['agency_contact_opt_in_pref']['global'],$contact['agency_contact_opt_in_pref']['list']));
		}
		
		# Finished writing, close the file
		fclose($fh);						
		
		# Store the csv in a zip file using a unique id as there  
		# may be multiple zip files being generated concurrently		
		# on the same server
		$zip_filename = uniqid() . '.zip';
		$zip_file_path = sys_get_temp_dir () . DIRECTORY_SEPARATOR . $zip_filename;
		
		$zip = new ZipArchive ( );
		$zip->open ( $zip_file_path, ZipArchive::CREATE );
		$zip->addFile ($temp_filepath, $temp_filename);
		$zip->close ();
		
		$zip_data = file_get_contents ( $zip_file_path );
		unlink ( $zip_file_path );
		unlink ( $temp_filepath);
		
		$zip_filename = $records['campaign_event_reference'] . '_campaign_event_log.zip';
		
		// Setup the file header to download the generated zip file
        header("Content-type: application/octet-stream");
        header("Cache-control: private"); // fix for IE
        header("Content-Disposition: filename=$zip_filename");
        header("Content-Length: " . strlen($zip_data));
        header("Pragma: public");
        
        # Make sure there is nothing in the output buffer which could corrupt the ZIP archive
        ob_clean();
        
		echo $zip_data;
	}


	/*
	 * Gets the e-mail activity as an array.
	 * This is a strip down version of get_email_solution_activity()
	 * *** FOR SCHEDULER USE ***
	 * 
	 * Array structure:
	 * $return_array[] = 
	 * 'CLIENT' data view:  the key is the profile type, for example Instiller, Response Genius, Inbox Warriors.  An additional total is inserted as a profile type.
	 * 'SUMMARY' data view: the key is each month
	 * $return_array[agency_type]['sent_emails']
	 * $return_array[agency_type]['events_triggered']
	 * $return_array[agency_type]['requests_forwarded']
	 * $return_array[agency_type]['api_impressions']
	 * 'CLIENT' data view only:
	 * $return_array[agency_type]['solution_totals'][solution_name] - contains the totals for each solution, similar to each profile type
	 * 
	 * @param agency_type the type of profile to search for, e.g. EMAIL_SOLUTIONS
	 * @param date_period an array of date values
	 * @param data_view - 'CLIENT' view (default) returns information to each individual client
	 * 
	 * @return array
	 */
	function get_agencies_solution_activity($agencies, $agency_type, $date_period, $data_view = 'CLIENT')
	{
		# Initialise solution activity array as empty shell
		$agency_type_totals = array();
		$agency_types = $this->get_all_agency_types();
		
		foreach ($agency_types as $agency) {
			if ($agency['identifier'] == 'EMAIL_SOLUTIONS') {
				$key = 'RESELLERS';
				$name = 'Resellers';
			} else {
				$key = $agency['description'];
				$name = $agency['description'];
			}
			$agency_type_totals[$key]['dataset'] = '';
			$agency_type_totals[$key]['name'] = $name;
			$agency_type_totals[$key]['sent_emails'] = 0;	
			$agency_type_totals[$key]['events_triggered'] = 0;	
			$agency_type_totals[$key]['requests_forwarded'] = 0;	
			$agency_type_totals[$key]['api_impressions'] = 0;	
			$agency_type_totals[$key]['inbox_inspections'] = 0;	
			$agency_type_totals[$key]['total_litmus_test_cost'] = 0;	
			$agency_type_totals[$key]['total_transactions'] = 0;
			$agency_type_totals[$key]['sent_emails_percentage_formatted'] = 0;
			$agency_type_totals[$key]['events_triggered_percentage_formatted'] = 0;
			$agency_type_totals[$key]['requests_forwarded_percentage_formatted'] = 0;	
			$agency_type_totals[$key]['api_impressions_percentage_formatted'] = 0;	
			$agency_type_totals[$key]['solution_totals'] = array();
		}
		
		$sql = "SELECT a.company_id, a.company_name,   " . 
				"SUM(company_billing_stats_test_emails + company_billing_stats_email_messages + company_billing_stats_event_emails) AS sent_emails,  " . 
				"SUM(company_billing_stats_event_triggers) AS events_triggered,   " . 
				"SUM(company_billing_stats_event_forwards) AS requests_forwarded,   " . 
				"SUM(company_billing_stats_api_impressions) AS api_impressions,   " .
				"SUM(company_billing_stats_inbox_inspections) AS inbox_inspections   " . 
				"FROM company a LEFT JOIN company_billing_stats b ON (a.company_id = b.company_id)  " . 					
				"WHERE b.company_billing_stats_period LIKE '" . $date_period . "%' " . 
				"GROUP BY company_id " . 
				"ORDER BY sent_emails DESC, api_impressions DESC, a.company_name;";			
		
		$activity = $this->exec_agency_sql2($agencies, $agency_type, $sql);

		$sql = " SELECT resellers_litmus_test_cost, resellers_litmus_test_default_cost, resellers_litmus_test_default_cost_currency FROM resellers LIMIT 1 ";
		
		$litmus_costs = $this->exec_agency_sql2($agencies, $agency_type, $sql);

		# Sum up all of the activity for each solution
		$agency_type_totals['RESELLERS']['dataset'] = 'summary';
		$current_key = '';
		
		# Add up all of the providers totals for a yearly total
		$this->set_name($agency_type_totals['RESELLERS'], 'Resellers');

		foreach($activity as $solution)
		{
			$resellers_litmus_test_cost = $litmus_costs[$solution['agency']['agency_id']]['data'][0]['resellers_litmus_test_cost'];

			if($current_key != $this->translate_db_identifier($solution['agency']['agency_type']))
			{
				$current_key = $this->translate_db_identifier($solution['agency']['agency_type']);
				$this->set_name($agency_type_totals[$current_key], $current_key);
				$agency_type_totals[$current_key]['dataset'] = 'solution';
			}
			
			$agency_id = $solution['agency']['agency_id'];
			$solution_name = $solution['agency']['agency_name'];
			$control_panel_url = $solution['agency']['agency_control_panel_url'];
			$db_server = $this->translate_db_server($solution['agency']['agency_database_host']);
			$db_identifier = $this->translate_db_identifier($solution['agency']['agency_type']);
			$freshbooks_clientId = $solution['agency']['agency_freshbooks_clientid'];
			$post_invoices = $solution['agency']['agency_post_invoices'];
			$billing_plan = $solution['agency']['agency_billing_plan'];
			$billing_status_active = $solution['agency']['agency_billing_status_active'];
			$solution_service_fee = $solution['agency']['agency_solution_service_fee'];
			$minimum_commitment = $solution['agency']['agency_minimum_commitment'];
			$price_breaks = $solution['agency']['agency_pb_details'];
			$sendto = $solution['agency']['agency_freshbooks_sendto_list'];
			$additional_instances_cost = $solution['agency']['agency_additional_instances_cost'];
			$additional_instances_litmus = $solution['agency']['agency_additional_instances_litmus'];
			$direct_relationship = $solution['agency']['agency_direct_relationship'];
			$vat = $solution['agency']['agency_vat'];
			$litmus = $solution['agency']['agency_litmus_allocation'];
			$discount = $solution['agency']['agency_solution_service_fee_discount'];

			# For this solution, get the total activity for all their clients
			if (isset($solution['data'])) {
				foreach($solution['data'] as $solution_client)
				{
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['solution_name'] = $solution_name;
					$this->set_name($agency_type_totals[$current_key]['solution_totals'][$solution_name], $solution_name);
					
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['agency_type'] = $solution['agency']['agency_type'];
					$this->add_stats($agency_type_totals[$current_key]['solution_totals'][$solution_name],$solution_client);
				}

				$this->add_stats($agency_type_totals[$current_key], $agency_type_totals[$current_key]['solution_totals'][$solution_name]);
			}

			$this->set_name($agency_type_totals[$current_key]['solution_totals'][$solution_name], $solution_name);

			# Add the total activity for the solution for the total for the current key
			$agency_type_totals['RESELLERS']['solution_totals'][$current_key]['name'] = $current_key;
			$this->add_stats($agency_type_totals['RESELLERS']['solution_totals'][$current_key], $agency_type_totals[$current_key]['solution_totals'][$solution_name]);

			
			if (isset($agency_type_totals[$current_key]['solution_totals'][$solution_name])) {
				$agency_type_totals[$current_key]['solution_totals'][$solution_name]['control_panel_url'] = $control_panel_url;
				$agency_type_totals[$current_key]['solution_totals'][$solution_name]['database_server'] = $db_server;
				$agency_type_totals[$current_key]['solution_totals'][$solution_name]['agency_id'] = $agency_id;
				
				# Add billing details if we have direct relationship
				if ($direct_relationship == 'Y') {
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['freshbooks_clientId'] = $freshbooks_clientId;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_billing_plan'] = $billing_plan;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_billing_status_active'] = $billing_status_active;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_solution_service_fee'] = $solution_service_fee;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_minimum_commitment'] = $minimum_commitment;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_pb_details'] = $price_breaks;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_vat'] = $vat;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_litmus_allocation'] = $litmus;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_additional_instances_cost'] = $additional_instances_cost;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_discount'] = $discount;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_freshbooks_sendto_list'] = $sendto;
					$agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details']['agency_post_invoices'] = $post_invoices;
					if ($agency_type_totals[$current_key]['name'] != 'Instiller') {
						$agency_type_totals[$current_key]['billing_details'] = $agency_type_totals[$current_key]['solution_totals'][$solution_name]['billing_details'];
					}
				} else {
					$url = substr($control_panel_url,stripos($control_panel_url,'//')+2);
					$agency_type_totals[$current_key]['additional_instances'][$url]['url'] = $url;
					$agency_type_totals[$current_key]['additional_instances'][$url]['name'] = $solution_name;					
					$agency_type_totals[$current_key]['additional_instances'][$url]['litmus'] = $agency_type_totals[$current_key]['solution_totals'][$solution_name]['inbox_inspections'];
					$agency_type_totals[$current_key]['additional_instances'][$url]['litmus_allocation'] = $additional_instances_litmus;
					if (isset($agency_type_totals[$current_key]['solution_totals'][$solution_name]['total_transactions'])) $agency_type_totals[$current_key]['additional_instances'][$url]['transactions'] = $agency_type_totals[$current_key]['solution_totals'][$solution_name]['total_transactions'];
					else $agency_type_totals[$current_key]['additional_instances'][$url]['transactions'] = 0;
					if (!isset($agency_type_totals[$current_key]['litmus_instances_total_allocations'])) $agency_type_totals[$current_key]['litmus_instances_total_allocations'] = $agency_type_totals[$current_key]['additional_instances'][$url]['litmus_allocation'];
					else $agency_type_totals[$current_key]['litmus_instances_total_allocations'] = $agency_type_totals[$current_key]['litmus_instances_total_allocations'] + $agency_type_totals[$current_key]['additional_instances'][$url]['litmus_allocation'];
				}
			}
		}


		foreach($agency_type_totals['RESELLERS']['solution_totals'] as $agency_type_total){
			$this->add_stats($agency_type_totals['RESELLERS'], $agency_type_total);
		}

		# Initialise all instances array
		$agency_type_totals['All'] = $agency_type_totals['RESELLERS'];
		$agency_type_totals['All']['dataset'] = 'solution';
		$agency_type_totals['All']['name'] = 'All';
		$agency_type_totals['All']['solution_totals'] = array();

		foreach ($agency_type_totals as $current_key => $solution) {
			if ($current_key != 'RESELLERS' && $current_key != 'All') {
				$agency_type_totals['All']['solution_totals'] = array_merge($agency_type_totals['All']['solution_totals'], $solution['solution_totals']);
			} 
		}
		return $agency_type_totals;
	}

	/*
	 * Adds the stats of the values array to the first parameter
	 * @return nothing
	 */
	function add_stats(&$array, $values)
	{
		if(!isset($array['sent_emails'])){
			$array['sent_emails'] = 0;
			$array['events_triggered'] = 0;
			$array['requests_forwarded'] = 0;
			$array['api_impressions'] = 0;
			$array['inbox_inspections'] = 0;
			$array['total_litmus_test_cost'] = 0;
			$array['total_transactions'] = 0;
		}
				
		$array['sent_emails'] 			+= isset($values['sent_emails']) ? $values['sent_emails'] : 0;
		$array['events_triggered'] 		+= isset($values['events_triggered']) ? $values['events_triggered'] : 0;
		$array['requests_forwarded'] 	+= isset($values['requests_forwarded']) ? $values['requests_forwarded'] : 0;
		$array['api_impressions'] 		+= isset($values['api_impressions']) ? $values['api_impressions'] : 0;
		$array['inbox_inspections'] 	+= isset($values['inbox_inspections']) ? $values['inbox_inspections'] : 0;
		$array['total_litmus_test_cost'] = $array['inbox_inspections'];
		$array['total_transactions'] = 	$array['sent_emails'] + $array['events_triggered'] + $array['requests_forwarded'] + $array['api_impressions'];
		
		# Initialise the numbers to convert to percentages
		$numbers = array();
		$numbers[] = $array['sent_emails'];
		$numbers[] = $array['events_triggered'];
		$numbers[] = $array['requests_forwarded'];
		$numbers[] = $array['api_impressions'];
		
		# Convert numbers to total to 100%
		$percent = $this->convert_to_percent($numbers);

		# Assign formatted values
		$array['sent_emails_percentage_formatted'] = $percent[0];
		$array['events_triggered_percentage_formatted'] = $percent[1];
		$array['requests_forwarded_percentage_formatted'] = $percent[2];
		$array['api_impressions_percentage_formatted'] = $percent[3];
	}
	

	/*
	 * Converts an array of numbers to percent that adds up to 100%
	 * @returns an array
	 */
	function convert_to_percent($numbers)
	{
	    $result = array();
	    $total = array_sum($numbers);

	    foreach($numbers as $number){
	    	if ($number == 0) {
	    		$result[] = 0;
	    	} else {
	    		$result[] = round(($number/$total) * 100, 1);
	    	}
	    }

	    $sum = array_sum($result);

	    if(100 !== $sum){
	        $maxKeys = array_keys($result, max($result));
	        $result[$maxKeys[0]] = 100 - ($sum - max($result));
	    }
	    return $result;
	}
	
	/*
	 * Sets the name element for an array
	 * @returns nothing
	 */
	function set_name(&$array, $name, $quote_name = false)
	{		
		if($quote_name == true) $array['name'] = $this->quote($name);
		else $array['name'] = $name;
		
		if(!isset($array['sent_emails'])){
			$array['sent_emails'] = 0;
			$array['events_triggered'] = 0;
			$array['requests_forwarded'] = 0;
			$array['api_impressions'] = 0;
			$array['inbox_inspections'] = 0;
		}
	}

	
	/*
	 * Returns an array containing the months in the past year
	 * @returns array
	 */
	function get_past_months($no_months)
	{
		# Prepare the months to be used in the activity panel
		$activity_months = array(); 
	    $first_year = date( 'Y' );
	    $first_month = date( 'm' );
		for ($i = 0; $i < $no_months; $i++) {
        	# Apply the offset to the month
			$month = mktime( 0, 0, 0, $first_month - $i, 1, $first_year );
			$activity_month = array(); 
			$activity_month['year'] = date('Y',$month);
			$activity_month['month'] = date('m',$month);
			$activity_month['description'] = date('M Y',$month);
			$activity_month['selected'] = ($i == 0 ? true : false);
			$activity_months[] = $activity_month;
		}
		return $activity_months;
	}
	
	function get_agency_logins_sql() {
		$sql = "SELECT
				  a.company_users_id,
				  a.company_users_email,
				  a.company_users_name,
				  a.company_users_type,
				  b.company_name
				FROM
				  company_user a,
				  company b
				WHERE a.company_id = b.company_id
				  AND b.company_account_type = 'AGENCY'
				  AND a.company_users_email NOT LIKE 'support@instiller.co.uk'
				ORDER BY a.company_users_email ";

		return $sql;
	}
	
	
	function list_agency_logins($instance_type = 'ALL', $direct_relationship = 'Y') {		 

		$sql = $this->get_agency_logins_sql();
		
		return $this->get_agency_logins($instance_type, $direct_relationship, $sql);
	}
	
	function download_csv($contacts)
	{
		
		$agencies = load_model('agencies');
		$dr_agencies = $agencies->get_all_agencies('direct');

		
		# first create up a file handle to temporarily store the data  
		$temp_filename = 'current_agency_contacts.csv';
		$temp_filepath = sys_get_temp_dir () . DIRECTORY_SEPARATOR . $temp_filename;		 
		$fh = fopen( $temp_filepath, 'w+');

		# Generate the header row		
		fputcsv($fh, array('email','first_name','full_name','solution_url', 'billing_plan'));		

		# Loop through all the agency with direct relationships
		foreach ($dr_agencies['agency_list'] as $agency) {
			
			# Make sure that we do not include instiller and kingpin logins
			if ($agency['agency_name'] != 'Instiller' && $agency['agency_name'] != 'Kingpin') {
				
				# Check if contacts are monitored for this agency
				# If it is, records should be found in the database
				if (isset($contacts[$agency['agency_id']]) && count($contacts[$agency['agency_id']]) > 0) {
					
					# Loop through each of the agency logins
					foreach ($contacts[$agency['agency_id']] as $id => $login) {
						
						# If ID is stored in db as 0 or negative, user is a freshbooks contact only
						if ($id > 0) {
							
							# Also, make sure that in-house emails are excluded
							$email_components = explode('@', $login['agency_contact_email']);
							$email_domain = $email_components[1];

							if (strtolower(trim($email_domain)) != 'instiller.co.uk' ) {
								
								fputcsv($fh, array($login['agency_contact_email'], $login['agency_contact_first_name'], $login['agency_contact_name'], $login['agency_control_panel_url'], $login['agency_billing_plan']));
							
							}
						
						}

					}

				}

			}
		
		}

		# finished writing, close the file
		fclose($fh);						
		
		# store the csv in a zip file using a unique id as there  
		# may be multiple zip files being generated concurrently		
		# on the same server
		$zip_filename = uniqid() . '.zip';
		$zip_file_path = sys_get_temp_dir () . DIRECTORY_SEPARATOR . $zip_filename;
		
		$zip = new ZipArchive ( );
		$zip->open ( $zip_file_path, ZipArchive::CREATE );
		$zip->addFile ($temp_filepath, $temp_filename);
		$zip->close ();
		
		$zip_data = file_get_contents ( $zip_file_path );
		unlink ( $zip_file_path );
		unlink ( $temp_filepath);
		
		$zip_filename = 'current_agency_contacts.zip';
		
		// Setup the file header to download the generated zip file
        header("Content-type: application/octet-stream");
        header("Cache-control: private"); // fix for IE
        header("Content-Disposition: filename=$zip_filename");
        header("Content-Length: " . strlen($zip_data));
        header("Pragma: public");
        
        # Make sure there is nothing in the output buffer which could corrupt the ZIP archive
        ob_clean();
        
		echo $zip_data;
	}
	
	/*
	 * This function builds a list of all distinct click tracking domains that are being used for each solution 
	 * and then identifies any that are duplicated across more than one solution
	 */
	function get_duplicate_click_tracking_domains() {
				
		$sql = "(SELECT DISTINCT 
				  company_email_profile_agent_domain_name AS 'click_tracking_domain',
				  'Email Profile' AS 'type'
				FROM company_email_profiles) 
				UNION
				(SELECT 
				  resellers_default_tracking_domain AS 'click_tracking_domain',
				  'Default Click Domain' AS 'type'
				FROM resellers)";
		$results = $this->exec_profile_sql('EMAIL_SOLUTIONS', $sql);
		
		#debug($results);
		
		# Populate a list of what solution each click tracking domain is used on
		# Build the array in a way that is easy to check for duplicate domains
		$click_tracking_domains = array();
		foreach($results as $solution) {
			if(isset($solution['data'])) {
				foreach($solution['data'] as $click_tracker) {
					# Add this solution to the list that has used that domain
					$click_tracking_domain = array();
					$click_tracking_domain['database_name'] = $solution['profile']['profile_database_name'];
					$click_tracking_domain['click_tracking_domain'] = $click_tracker['click_tracking_domain'];
					$click_tracking_domain['type'] = $click_tracker['type'];
					$key = $click_tracking_domain['click_tracking_domain'];
					$click_tracking_domains[$key][] = $click_tracking_domain;	
				}
			}
		}	

		#debug($click_tracking_domains);
		
		$duplicate_domains = array();
		# Go through the array and identify any click tracking domains that are used on more than one solution
		foreach($click_tracking_domains as $click_tracking_domain => $entries) {
			# Work out which databases this domain is used on
			$databases = array();
			foreach($entries as $entry) {
				$databases[$entry['database_name']] = $entry;
			}
			# If it is used across more than one database, add it to the list of duplicates
			if(sizeof($databases) > 1) {
				$duplicate_domains[$click_tracking_domain] = $entries;
			}
		}	
		#debug($duplicate_domains);
		
		return $duplicate_domains;
	}
	
	/*
	 * This function builds a list of all distinct click tracking domains that are being used for each solution 
	 * and then identifies any that are duplicated across more than one solution
	 */
	function click_domain_search($domain) {
		$sql = "(SELECT DISTINCT 
				  company_email_profile_agent_domain_name AS 'click_tracking_domain',
				  'Email Profile' AS 'type'
				FROM  company_email_profiles 
				WHERE company_email_profile_agent_domain_name LIKE ".$this->quote('%'.$domain.'%').")
				UNION
				(SELECT 
				  resellers_default_tracking_domain AS 'click_tracking_domain',
				  'Default Click Domain' AS 'type'
				FROM  resellers
				WHERE resellers_default_tracking_domain LIKE ".$this->quote('%'.$domain.'%').")";
		$results = $this->exec_profile_sql('EMAIL_SOLUTIONS', $sql);
		
		$search_results = array();
		foreach($results as $solution) {
			if(isset($solution['data'])) {
				foreach($solution['data'] as $click_tracking_domain) {					
					$search_result = array();
					$search_result['click_tracking_domain'] = $click_tracking_domain['click_tracking_domain'];
					$search_result['type'] = $click_tracking_domain['type'];
					$search_result['database_name'] = $solution['profile']['profile_database_name'];
					$search_results[] = $search_result;
				}
			}
		}
			
		return $search_results;
	}
		
	function sending_domain_search($domain) {
		$sql = "(SELECT DISTINCT 
				  company_email_profile_from_address AS 'from_address',
				  'Email Profile' AS 'type'
				FROM  company_email_profiles
				WHERE company_email_profile_from_address LIKE ".$this->quote('%'.$domain.'%')." )
				UNION
				(SELECT 
				  resellers_default_from_email_address AS 'from_address',
				  'Default From Address' AS 'type'
				FROM  resellers
				WHERE resellers_default_from_email_address LIKE ".$this->quote('%'.$domain.'%')." )";
		$results = $this->exec_profile_sql('EMAIL_SOLUTIONS', $sql);
		
		$sending_domains = array();
		
		foreach($results as $solution) {
			if(isset($solution['data'])) {
				foreach($solution['data'] as $from_address) {	
					# Get the sending domain from the email address
					$sending_domain = '';
					$parts = explode('@',$from_address['from_address']);
					$sending_domain = array();
					$sending_domain['name'] = $parts[1];
					$sending_domain['type'] = $from_address['type'];
					$sending_domain['database_name'] = $solution['profile']['profile_database_name'];
					$key = $sending_domain['name'].$sending_domain['type'];
					$sending_domains[$key] = $sending_domain;
				}
			}
		}

		return $sending_domains;
	}
	
	/**
	 * 
	 * Takes some sql and runs it on each database that has a profile.  
	 * Currently just dumps it out in a debug, should return an array to be displayed on the screen later.
	 * @param String $sql
	 */
	function execute_global_sql($sql, $profile = 'EMAIL_SOLUTIONS', $merge_results = false) 
	{	
		# Intialise the results 
		$results = array();
		$results['summary'] = array();
		$results['data'] = array();
		
		# Grab the results from all of the solutions		
		$query_results = $this->exec_agency_sql($profile, $sql, true);
		if($query_results == false) return false;
		
		// Convert the results into a format that can be shown more easily on the page
		
		// Compile an array of what databases were searched
		$databases_searched = array();
		foreach($query_results as $solution) {
			# Create the summary item 
			$summary = array();
			$summary['database'] = $solution['agency']['agency_database_name'];
			$summary['solution_name'] = $solution['agency']['agency_name'];
			if (isset($solution['data']) && count($solution['data']) > 0) $summary['results'] = count($solution['data']);
			else $summary['results'] = 0;
			$results['summary'][$solution['agency']['agency_database_name']] = $summary;

			# Just dump in the data to be viewed
			$data = array();
			$data['database'] = $solution['agency']['agency_database_name'];
			$data['solution_name'] = $solution['agency']['agency_name'];
			$data['present'] = false;
			if (isset($solution['data']) && count($solution['data']) > 0) {
				$data['present'] = true;
				$data['results'] = $solution['data'];
			}
			$results['data'][$solution['agency']['agency_database_name']] = $data;
		}

		return $results;
		
		
		// First check if there are any results at all
		$results_found = false;
		$solutions_with_data = array();
		foreach($results as $solution) {
			if(isset($solution['data'])) {
				$results_found = true;
				
				$solutions_with_data[] = $solution;
			} 
		}
		
		$return_array['results_found'] = $results_found;
		
		// Work out the column headings
		if($results_found == true) {
			// Get all the keys for this data row
			$headings = array_keys($solutions_with_data[0]['data'][0]);
			$headings = array_filter($headings,'is_string');
			array_unshift($headings,'agency_name');
			$return_array['headings'] = $headings;
			
			$merge_identical_results = true;
			$processed_results = array();
			// Create the results that are going to be shown on the screen
			foreach($solutions_with_data as $solution) {
				foreach($solution['data'] as $data_row) {
					$agency_name = $solution['agency']['agency_name'];	
					$processed_result['database_names'] = array();				
					$processed_result['database_names'][] = $agency_name;
					$data  = $this->key_filter($data_row);
					foreach($data as &$cell) {
						$cell = htmlentities($cell);
					}
					$processed_result['data'] = $data;
					$databases_searched[$agency_name]['rows']++;
					
					
					$processed_results[] = $processed_result;
				}
			}
						
			$return_array['processed_results'] = $processed_results;
		} 
		
		
		
		$return_array['databases_searched'] = $databases_searched;
		return $return_array;
	}
	
	/*
	 * This function takes a row, and filters out the duplicate cells
	 */
	function key_filter($row) {
		$return_row = array();
		foreach($row as $key => $cell) {
			if(is_string($key)) {
				$return_row[$key] = $cell;
			}
		}
		return $return_row;
	}
	
	function kill_eyetap_process($db_host, $process_id) {
			
		$sql = "KILL " . $this->quote($process_id);
		
		// Find the right connection
		$profile = array();
		$profile['profile_database_host'] = $db_host;
		$profile['profile_database_name'] = '';
		
		$connection = $this->select_db_connection($profile, true);
		$connection->execute($sql);
	}

	function get_top_email_clients($start_date, $end_date, $record_count = 10) {
		# Load the aggregated analysis records for the campaigns
		$total_email_clients = 0;
		$client_analysis = array();
		$sql = " SELECT email_spooler_analysis_data 
				 FROM email_spooler_analysis a, email_spooler b
				 WHERE a.email_spooler_analysis_type = 'EMAIL_CLIENT' 
				 AND a.email_spooler_id = b.email_spooler_id 
				 AND b.email_spooler_delivery_date <= " . $this->quote($end_date . ' 23:59:59') . "
				 AND b.email_spooler_delivery_date >= " . $this->quote($start_date . ' 00:00:00');	

		$email_clients = $this->exec_profile_sql('EMAIL_SOLUTIONS', $sql);

		#file_put_contents('/project/file_uploads/device_leaderboard_serialized.dat', serialize($email_clients));

		#$email_clients = unserialize(file_get_contents('/project/file_uploads/device_leaderboard_serialized.dat'));

		$client_analysis = array();
		$total_email_clients = 0;

		foreach($email_clients as $solution) {
			if (isset($solution['data'])) {
				foreach ($solution['data'] as $records) {					
					if (count($records) > 0) {
						foreach($records as $record) {

							# Unpack the analysis data for this campaign and check we have something to include
				 			$data = unserialize($record);
				 			if (is_array($data) && count($data) > 0) {
								# Loop through the client analysed for this campaign 			
				 				foreach ($data as $name => $client) {
									# Check to make sure not 0 this will hide other clients = 0
									if ($client['total'] > 0) {
										# Check to see if we need to add the core information 
										# for this client within the analysis collection 
										if (!isset($client_analysis[$name])) {
											$new_client = array();
											$new_client['client_name'] = $name;
											$new_client['total'] = 0;
											$new_client['type'] = $client['type'];
											# Add the client to the cache
											$client_analysis[$name] = $new_client;
										}

										# On this pass just accumulate the totals then percentages on the next
										$total_email_clients += $client['total'];
										$client_analysis[$name]['total'] += $client['total'];
									}
								}
				 			}
						}
					}
				}
			}
		}

 		# Run through the email clients and work out the percentages
 		foreach ($client_analysis as $name => $client) {
 			if ($total_email_clients == 0) $client_analysis[$name]['percentage'] = 0;
 			else $client_analysis[$name]['percentage'] = round(($client_analysis[$name]['total'] / $total_email_clients) * 100,2);
 		}

 		# Store the client analysis in the array
 		uasort($client_analysis, array($this, 'sort_email_clients')); 		

		$results = array();
		$counter = 0;
 		# just grab the last $record_count results
 		foreach ($client_analysis as $client) {
 			$counter++;
 			$results[] = $client;

 			if ($counter >= $record_count) 
 				break;
 		}
   		
   		return $results;
	}

	function get_client_type($start_date, $end_date) {
		# Load the aggregated analysis records for the campaigns
		$sql = " SELECT email_spooler_analysis_data 
				 FROM email_spooler_analysis a, email_spooler b
				 WHERE a.email_spooler_analysis_type = 'EMAIL_CLIENT_FLAGS' 
				 AND a.email_spooler_id = b.email_spooler_id 
				 AND b.email_spooler_delivery_date <= " . $this->quote($end_date . ' 23:59:59') . "
				 AND b.email_spooler_delivery_date >= " . $this->quote($start_date . ' 00:00:00');

		$solutions = $this->exec_profile_sql('EMAIL_SOLUTIONS', $sql);

#		$solutions = unserialize(file_get_contents('/project/file_uploads/tmp_client_type.dat'));

   		# We should load all of the email client analysis to power the new section 
		$client_analysis = array();
		$client_analysis[4] = array('system_contact_analysis_flag_id' => 4, 'system_contact_analysis_flag_name' => 'Desktop email client', 'total' => 0);
		$client_analysis[5] = array('system_contact_analysis_flag_id' => 5, 'system_contact_analysis_flag_name' => 'Mobile email client', 'total' => 0);
		$client_analysis[6] = array('system_contact_analysis_flag_id' => 6, 'system_contact_analysis_flag_name' => 'Web email client', 'total' => 0);
		$client_analysis[7] = array('system_contact_analysis_flag_id' => 7, 'system_contact_analysis_flag_name' => 'Other', 'total' => 0);		
		 		
		foreach ($solutions as $solution) {
			if (isset($solution['data'])) {
		 		foreach ($solution['data'] as $record) {
		 			# Unpack the analysis data for this campaign and check we have something to include
		 			$data = unserialize($record['email_spooler_analysis_data']);
		 			if (is_array($data) && count($data) > 0) {
		 				# Loop through and accumulate the totals for each 
		 				foreach ($data as $data_item) {
		 					$client_analysis[$data_item['system_contact_analysis_flag_id']]['total'] += $data_item['users'];
		 				}
		 			}
		 		}
		 	}
	 	}
 		
 		# Now we should calculate the percentages for the client type usage
		$total_users = $client_analysis[4]['total'] + $client_analysis[5]['total'] + $client_analysis[6]['total'] + $client_analysis[7]['total'];
		if ($total_users == 0) {
			$client_analysis[4]['percentage'] = 0;
			$client_analysis[5]['percentage'] = 0;
			$client_analysis[6]['percentage'] = 0;
			$client_analysis[7]['percentage'] = 0;			
		}  else {
			$client_analysis[4]['percentage'] = round(($client_analysis[4]['total'] / $total_users) * 100);
			$client_analysis[5]['percentage'] = round(($client_analysis[5]['total'] / $total_users) * 100);
			$client_analysis[6]['percentage'] = round(($client_analysis[6]['total'] / $total_users) * 100);
			$client_analysis[7]['percentage'] = round(($client_analysis[7]['total'] / $total_users) * 100);
		}


		uasort($client_analysis, array($this, 'sort_email_clients'));

		return $client_analysis;
	}


	function get_conversion_clients($start_date, $end_date) {
		# Load the aggregated analysis records for the campaigns
		$sql = " SELECT email_spooler_analysis_data 
				 FROM email_spooler_analysis a, email_spooler b
				 WHERE a.email_spooler_analysis_type = 'CONVERSION_CLIENT_FLAGS' 
				 AND a.email_spooler_id = b.email_spooler_id 
				 AND b.email_spooler_delivery_date <= " . $this->quote($end_date . ' 23:59:59') . "
				 AND b.email_spooler_delivery_date >= " . $this->quote($start_date . ' 00:00:00');

		$solutions = $this->exec_profile_sql('EMAIL_SOLUTIONS', $sql);

		#file_put_contents('/project/file_uploads/tmp_conversion.dat', serialize($email_clients));

		#die('done');

		#$solutions = unserialize(file_get_contents('/project/file_uploads/tmp_conversion.dat'));

   		# We should load all of the email client analysis to power the new section 
		$client_analysis = array();
		$client_analysis[4] = array('system_contact_analysis_flag_id' => 4, 'system_contact_analysis_flag_name' => 'Desktop email client', 'total' => 0);
		$client_analysis[5] = array('system_contact_analysis_flag_id' => 5, 'system_contact_analysis_flag_name' => 'Mobile email client', 'total' => 0);
		$client_analysis[6] = array('system_contact_analysis_flag_id' => 6, 'system_contact_analysis_flag_name' => 'Web email client', 'total' => 0);
		$client_analysis[7] = array('system_contact_analysis_flag_id' => 7, 'system_contact_analysis_flag_name' => 'Other', 'total' => 0);		
		 		
		foreach ($solutions as $solution) {
			if (isset($solution['data'])) {
		 		foreach ($solution['data'] as $record) {
		 			# Unpack the analysis data for this campaign and check we have something to include
		 			$data = unserialize($record['email_spooler_analysis_data']);
		 			if (is_array($data) && count($data) > 0) {
		 				# Loop through and accumulate the totals for each 
		 				foreach ($data as $data_item) {
		 					$client_analysis[$data_item['system_contact_analysis_flag_id']]['total'] += $data_item['users'];
		 				}
		 			}
		 		}
		 	}
	 	}	 

 		# Now we should calculate the percentages for the client type usage
		$total_users = $client_analysis[4]['total'] + $client_analysis[5]['total'] + $client_analysis[6]['total'] + $client_analysis[7]['total'];
		if ($total_users == 0) {
			$client_analysis[4]['percentage'] = 0;
			$client_analysis[5]['percentage'] = 0;
			$client_analysis[6]['percentage'] = 0;
			$client_analysis[7]['percentage'] = 0;			
		}  else {
			$client_analysis[4]['percentage'] = round(($client_analysis[4]['total'] / $total_users) * 100);
			$client_analysis[5]['percentage'] = round(($client_analysis[5]['total'] / $total_users) * 100);
			$client_analysis[6]['percentage'] = round(($client_analysis[6]['total'] / $total_users) * 100);
			$client_analysis[7]['percentage'] = round(($client_analysis[7]['total'] / $total_users) * 100);
		}

		uasort($client_analysis, array($this, 'sort_email_clients'));

		return $client_analysis;
	}

	/**
	 * Generic sorting routine to swtich the user records into 
	 * the correct order based on percentages with total fallback. 
	 * @param unknown_type $a
	 * @param unknown_type $b
	 */
	function sort_email_clients($a, $b) 
	{
		# If both are not selected then no comparison is needed
		if ($a['percentage'] > $b['percentage']) return -1; 
		elseif ($a['percentage'] < $b['percentage']) return 1; 
		else {
			if (!isset($a['total'])) debug($a);
			if ($a['total'] > $b['total']) return -1; 
			else return 1; 
		}
	}

	function update_ssl_records($agencies = array())
	{
		
		if (count($agencies) < 1) {

			# Load agency model
			# Create the agencies model
			$instances = load_model('agencies');

			# Get all active solutions
			$data = $instances->get_all_agencies('active');

			# Assign agencies for processing
			$agencies = $data['agency_list'];
		}
		
		# Initialise alerts
		$alerts = array();

		foreach ($agencies as &$agency) {

			# Re-set ssl error to NULL for now
			# Error will be updated later when curl is run
			$agency['agency_ssl_error'] = NULL;

			# Get ssl information via cURL
			$fp = tmpfile();
			$ch = curl_init();
		    
		    curl_setopt($ch, CURLOPT_URL, $agency['agency_control_panel_url']);
			curl_setopt($ch, CURLOPT_NOBODY, 1 ); # Make it a HEAD request to save on bandwidth
			curl_setopt($ch, CURLOPT_HEADER, 1); # Turn off headers in the response
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); # Follow redirections using the Location header
			curl_setopt($ch, CURLOPT_MAXREDIRS, 5 ); # Set the maximum number of redirections to 5
			curl_setopt($ch, CURLOPT_TIMEOUT, 10);	# We need a conservative timeout otherwise validation could take ages 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); # Grab the content output

			curl_setopt($ch, CURLOPT_VERBOSE, 1); # Need to enable verbose logging to get cert info
			curl_setopt($ch, CURLOPT_CERTINFO, 1);	# Capture certificate information 
			curl_setopt($ch, CURLOPT_STDERR, $fp);	# Capture the verbose output in temp file

			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); # Verify the SSL Peer
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); # Verify the SSL Host
			curl_setopt($ch, CURLOPT_SSLVERSION, 3); # Force to SSL version 3

			curl_exec($ch);

			# If there are any errors, set error to store in db
			curl_errno($ch)==0 or $agency['agency_ssl_error'] = curl_error($ch);

			fseek($fp, 0);
			
			$result = '';
			
			while (strlen($result .= fread($fp,8192)) == 8192);

			$data = array();

			$temp_data = explode('*', $result);

			foreach ($temp_data as $temp) {

				$temp = strtoupper($temp);

				if (strpos($temp, 'EXPIRE DATE:') !== false) {

					# Re-set ssl expire date to this
					$agency['agency_ssl_renewal_date'] = date("Y-m-d H:i:s", strtotime(substr($temp, strrpos($temp, 'EXPIRE DATE:') + 12)));
				}

				if (strpos($temp, 'ISSUER:') !== false) {
					
					$provider = 'OWN';

					if ($agency['agency_ssl_managed'] == 'Y') {
						if (strpos($temp, 'GODADDY') !== false) $provider = 'GODADDY';
						if (strpos($temp, 'RAPIDSSL') !== false) $provider = 'RAPIDSSL';
					}

					# Re-set provider the up-to-date information
					$agency['agency_ssl_provider'] = $provider;
				}

			}

			# Calculate ssl certificate expiry according to current information
			$ssl_days_remaining = floor((strtotime($agency['agency_ssl_renewal_date'])-strtotime(date("Y-m-d",time())))/(60*60*24));
			
			# If days remaining is less less than 1, flagged as expired
			if ($ssl_days_remaining <= 0) $agency['agency_ssl_error'] = "Peer's Certificate is expired.";

			# Push agency data to alerts array if ssl remaining days is 14,7 and already expired
			if (($ssl_days_remaining == 14 || $ssl_days_remaining == 7 || $ssl_days_remaining <= 1) && $agency['agency_ssl_provider'] != 'OWN') {
				$agency['ssl_days_remaining'] = $ssl_days_remaining;
				$alerts[] = $agency;
			}	
			
			fclose($fp);

			# Build sql to update db with up-to-date ssl information 
			$sql = " UPDATE agencies SET " . 
				   " agency_ssl_renewal_date = " . $this->quote($agency['agency_ssl_renewal_date']) . " , " .
				   " agency_ssl_provider = " . $this->quote($agency['agency_ssl_provider']) . " , " .
				   " agency_ssl_error = " . $this->quote($agency['agency_ssl_error']) .
				   " WHERE agency_id = " . $this->quote($agency['agency_id']);
			
			# Save up-to-date data in db
			$this->execsql($sql);
		}

		# Return agencies with ssl certs that is about to run out
		return $alerts;
	}

}
?>