<?php

class profiles extends model_base_class
{
	# Class properties
	public $profile_id;
	public $profile_name;
	public $profile_type;	
	public $profile_date_created;
	public $profile_enabled;
	public $profile_database_host;
	public $profile_database_name;
	public $profile_control_panel_url;	
	public $profile_direct_relationship;
	public $profile_freshdesk_custid;
	public $setup_freshdesk;
	public $agency_logins;
	public $instiller_logins;
	public $agency_contact_id;
	public $agency_contact_email;
	public $agency_contact_name;
	public $agency_contact_campaign_event_triggered;
	public $agency_contact_date_created;
	public $freshdesk_login_created;
	public $freshdesk_id;
	public $agency_login_created;
	public $profile_contacts_ignored;


	/**
	 * This function will list all of the configured profiles
	 *
	 * @return unknown
	 */
	function profile_search($smart_filter_location) {
		# Build the query to search
		$sql = " SELECT * FROM profiles ORDER BY profile_type, profile_name ";
		# Return the constructed SQL
		return $sql;
	}

	function handle_micro_edit($profile_id) {

		# Initialise the results of the edit 
		$result = array(); 
		$result['saved'] = true;
		$result['error_message'] = '';
		$result['refresh_required'] = false;

		# Check to see which of the fields is included within the variables
		if (isset($this->variables['profile_name'])) {
			
			# Update the profile name
			$sql = 	" UPDATE profiles SET " .
					" profile_name = " . $this->quote($this->variables['profile_name']) .   
					" WHERE profile_id = " . $this->quote($profile_id);
			$this->execsql($sql);

			$this->load_profile($profile_id);
			
			if (!is_null($this->profile_freshdesk_custid) && $this->in_freshdesk('customers', $this->profile_freshdesk_custid, 'id')) {
				
				# If customer exist in Freshdesk, update profile name
				$jsondata = array (
				'customer[name]' => $this->profile_name
				);

				$this->call_freshdesk_API("customers", "PUT", $jsondata);	
			
			} else {

				# Make sure the Freshdesk customer id is set to null if not found
				$sql = "UPDATE profiles SET " .
					   "profile_freshdesk_custid = NULL WHERE " .
					   "profile_id = " . $this->quote($this->profile_id);
				
				$this->execsql($sql);
			
			}
		}
		
		if (isset($this->variables['profile_type'])) {
			
			# Update the profile type
			$sql = 	" UPDATE profiles SET " .
					" profile_type = " . $this->quote($this->variables['profile_type']) .   
					" WHERE profile_id = " . $this->quote($profile_id);
			$this->execsql($sql);
		}
		
		if (isset($this->variables['profile_database_host'])) {
			
			# Update the profile database host
			$sql = 	" UPDATE profiles SET " .
					" profile_database_host = " . $this->quote($this->variables['profile_database_host']) .   
					" WHERE profile_id = " . $this->quote($profile_id);
			$this->execsql($sql);
		}
		
		if (isset($this->variables['profile_database_name'])) {
			
			# Update the profile database name
			$sql = 	" UPDATE profiles SET " .
					" profile_database_name = " . $this->quote($this->variables['profile_database_name']) .   
					" WHERE profile_id = " . $this->quote($profile_id);
			$this->execsql($sql);
		}
		
		if (isset($this->variables['profile_control_panel_url'])) {
			
			# Update the profile control panel url
			$sql = 	" UPDATE profiles SET " .
					" profile_control_panel_url = " . $this->quote($this->variables['profile_control_panel_url']) .   
					" WHERE profile_id = " . $this->quote($profile_id);
			$this->execsql($sql);
		}
		
		if (isset($this->variables['field']) && $this->variables['field'] == 'profile_direct_relationship') {
			
			if ($this->variables['value'] == 'Y') {
				$profile_contacts_ignored = 'N';
				if (isset($this->variables['freshdesk_custid']) && $this->variables['freshdesk_custid'] > 1) {
					$profile_freshdesk_custid = $this->variables['freshdesk_custid'];
				} else {
					$profile_freshdesk_custid = NULL;
				}
			} else {
				$profile_contacts_ignored = 'Y';
				if (isset($this->variables['freshdesk_custid']) && $this->variables['freshdesk_custid'] > 1) {
					if ($this->in_freshdesk('customers', $this->variables['freshdesk_custid'], 'id')) {
						$this->profile_freshdesk_custid = $this->variables['freshdesk_custid'];
						$this->call_freshdesk_API('customers','DELETE');
					}
				} 
				$profile_freshdesk_custid = NULL;
			}

			# Update the profile direct relationship
			$sql = 	" UPDATE profiles SET " .
					$this->variables['field'] . " = " . $this->quote($this->variables['value']) . " , " .
					" profile_contacts_ignored = " . $this->quote($profile_contacts_ignored) . " , " . 
					" profile_freshdesk_custid = " . $this->quote($profile_freshdesk_custid) .
					" WHERE profile_id = " . $this->quote($profile_id);
			
			$this->execsql($sql);

			$result['profile_direct_relationship'] = $this->variables['value'];
			$result['profile_contacts_ignored'] = $profile_contacts_ignored;
			if ($profile_freshdesk_custid == NULL) $result['agency_in_freshdesk'] = 'N';
			else $result['agency_in_freshdesk'] = 'Y';
		}

		if (isset($this->variables['field']) && $this->variables['field'] == 'profile_contacts_ignored') {
			
			# Update the profile contacts ignored
			$sql = 	" UPDATE profiles SET " .
					$this->variables['field'] . " = " . $this->quote($this->variables['value']) .   
					" WHERE profile_id = " . $this->quote($profile_id);
			$this->execsql($sql);

			if ($this->variables['field'] == 'profile_contacts_ignored') $result['profile_contacts_ignored'] = $this->variables['value'];
		}

		if (isset($this->variables['field']) && $this->variables['field'] == 'profile_freshdesk_custid') {

			# Load profile
			$this->load_profile($this->variables['profile_id']);

			# Process customer in Freshdesk 
			if ($this->variables['value'] == 'Y') {
				
				$jsondata = array (
					'customer[name]' => $this->profile_name
				);
				
				$response = $this->call_freshdesk_API("customers", "POST", $jsondata);
				$result['performed_action'] = "created";
				$result['agency_in_freshdesk'] = 'Y';
				$result['customer_id'] = $response->customer->id;

			} else {
				
				if ($this->in_freshdesk('customers', $this->profile_freshdesk_custid, 'id')) {
					
					# Delete if agency exist is freshdesk 
					$this->call_freshdesk_API("customers", "DELETE");
					
				}
				$result['performed_action'] = "deleted";
				$result['agency_in_freshdesk'] = 'N';
				$result['customer_id'] = -1;

				# Delete all logins associated with agency
				$this->delete_freshdesk_logins($this->profile_freshdesk_custid);
			}
		}

		if (isset($this->variables['field']) && $this->variables['field'] == 'freshdesk_id') {

			# Restore deleted/Create new Freshdesk contact
			if ($this->variables['value'] == 'Y') {
				if ($this->variables['id_status'] == 'Deleted') {
					# Restore contact
					$jsondata = array (
						'user[customer_id]' => trim($this->variables['customer_id']),
						'user[client_manager]' => 'true',
						'user[deleted]' => 'false'
					);
					# Call Freshdesk API - put
					$this->call_freshdesk_API("contacts", "PUT", $jsondata, $this->variables['id']);

					# Get updated Freshdesk login
					$response = $this->call_freshdesk_API("contacts", "GET", $jsondata, $this->variables['id']);
					
					$result['freshdesk_id'] = $this->variables['id'];
					$result['customer_id'] = $this->variables['customer_id'];
					$result['performed_action'] = 'restored';
					if ($response->user->active == 'TRUE') $result['id_status'] = "Verified";
					else $result['id_status'] = "Unverified";

				} else {
					# Create new contact
					$jsondata = array (
						'user[name]' => rawurldecode(trim($this->variables['name'])),
						'user[email]' => trim($this->variables['email']),
						'user[customer_id]' => trim($this->variables['customer_id']),
						'user[client_manager]' => 'true'
					);
					# Call Freshdesk API - post
					$response = $this->call_freshdesk_API("contacts", "POST", $jsondata, $this->variables['id']);

					$result['freshdesk_id'] = $response->user->id;
					$result['customer_id'] = $response->user->customer_id;
					$result['performed_action'] = 'created';
					if ($response->user->active == 'TRUE') $result['id_status'] = "Verified";
					else $result['id_status'] = "Unverified";
				}

				# Update agency contact freshdesk id in database
				$sql = 	" UPDATE agency_contacts SET " .
						$this->variables['field'] . " = " . $this->quote($result['freshdesk_id']) . " , " . 
						" freshdesk_customer_id = " . $this->quote($result['customer_id']) .
						" WHERE agency_contact_id = " . $this->quote($this->variables['agency_contact_id']);
				$this->execsql($sql);

			}
			# Delete Freshdesk Contact
			else {
				$jsondata = "";
				# Call Freshdesk API - delete
				$this->call_freshdesk_API("contacts", "DELETE", $jsondata, $this->variables['id']);

				$result['freshdesk_id'] = $this->variables['id'];
				$result['customer_id'] = $this->variables['customer_id'];
				$result['performed_action'] = 'deleted';
				$result['id_status'] = "Deleted";
			}
		} 
		
		# return the results of the array
		return $result;
		
	}

	
	/**
	 * Function to delete all associated Freshdesk logins to an agency 
	 *
	 * @return unknown
	 */
	function delete_freshdesk_logins($customer_id) 
	{
		# Delete all associated Freshdesk logins to this agency
		foreach ($this->agency_logins as $login) {
			# Delete all active logins
			# Deleted logins are dissociated from the agency
			if ($login['id_status'] != 'Deleted' && $login['agency_custid'] == $customer_id) {
				$this->call_freshdesk_API('contacts','DELETE','',$login['freshdesk_id']);
			}
		}

		return true;
	}


	/**
	 * Function to do API calls to freshdesk
	 *
	 * @return unknown
	 */
	function call_freshdesk_API($call_type, $process, $jsondata = "",$id = "") {

		# Initialise Freshdesk credentials and customer data
		$email = FRESHDESK_LOGIN_EMAIL;
		$password = FRESHDESK_LOGIN_PASSWORD;

		$header[] = "Content-type: multipart/form-data";
		 
		if ($call_type == 'customers') {
			if (!is_null($this->profile_freshdesk_custid)) {
				$id = "/" . $this->profile_freshdesk_custid;
			} else {
				$id = "";
			}
		} 
		
		if ($call_type == 'contacts') {
			if ($id != -1) {
				$id = "/" . $id;
			} else {
				$id = "";
			}
		}

		$url = FRESHDESK_DOMAIN . "/" . $call_type . $id . ".json";

		$ch = curl_init ($url);
		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $process);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsondata);
		 
		curl_setopt($ch, CURLOPT_USERPWD, "$email:$password");
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_HEADER, false);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		
		$server_output = curl_exec ($ch);
		
		$response = json_decode($server_output);
		$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close ($ch);

		# Update database records
		if ($http_status == 201 && $process == 'POST') {
			if ($call_type == 'customers') {
				# Store Freshdesk customer_id in profiles 
				$sql = "UPDATE profiles SET " .
					   "profile_freshdesk_custid = " . $this->quote($response->customer->id) . " WHERE " .
					   "profile_id = " . $this->quote($this->profile_id);
				$this->execsql($sql);

				return $response;
			} 			
		} 
		if ($http_status == 200 && $process == 'DELETE') {
			
			if ($call_type == 'customers') {
				# Delete customer id Freshdesk in the profiles table
				$sql = "UPDATE profiles SET " .
					   "profile_freshdesk_custid = NULL WHERE " .
					   "profile_id = " . $this->quote($this->profile_id);
				$this->execsql($sql);
			} 
		}
		
		if ($call_type == 'contacts') return $response;
		
		return true;	
	}

	/**
	 * Function to check if customer is setup in Freshdesk
	 *
	 * @return customer id
	 */
	function in_freshdesk($search_type = 'contacts', $search_value, $search_value_type = 'email')
	{
		# Initialise Freshdesk log-in credentials
		$email = FRESHDESK_LOGIN_EMAIL;
		$password = FRESHDESK_LOGIN_PASSWORD;
		
		$header[] = "Content-type: multipart/form-data";

		# Construct url
		if ($search_value_type == 'email') {
			$condition = "email is " . $search_value;
			$url = FRESHDESK_DOMAIN . "/" . $search_type . ".json?query=" . urlencode($condition) . "&state=all";
		} else {
			$url = FRESHDESK_DOMAIN . "/" . $search_type . "/" . $search_value . ".json?state=all";
		}
		
		# Make the API call
		$ch = curl_init ($url);

		curl_setopt($ch, CURLOPT_HTTPGET, true);
		 
		curl_setopt($ch, CURLOPT_USERPWD, "$email:$password");
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_HEADER, false);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		
		$server_output = curl_exec ($ch);
		$response = json_decode($server_output);

		if (count($response) < 1 || isset($response->errors)) return false;
		if ($search_value_type == 'email') return $response;
		else return true;
	}


	function get_all_profiles($filter) {
		
		# Build the query to search
		$sql = " SELECT * FROM profiles ";

		$filter_description = "All";

		if ($filter == 'direct') {
			$sql .= " WHERE profile_direct_relationship = 'Y' AND profile_enabled = 'Y' ";
			$filter_description = "Direct Relationship";
		}
		if ($filter == 'indirect') {
			$sql .= " WHERE profile_direct_relationship = 'N' AND profile_enabled = 'Y' ";
			$filter_description = "Indirect Relationship";
		}
		if ($filter == 'deleted') {
			$sql .= " WHERE profile_enabled = 'N' ";
			$filter_description = "Deleted";
		}

		$sql .= " ORDER BY profile_name ";
		
		$records = $this->execsql($sql);

		# Initialise profiles list
		$profiles_data = array();
		$profiles_data['profiles_list'] = array();

		while (($record = $records->FetchRow()) !== false) {

			# Assign solution owners to each profile for display
			if (strtoupper($record['profile_type']) == 'INSTILLER') {
				$record['solution_owner'] = 'Instiller';
			} elseif (strtoupper($record['profile_type']) == 'INBOX_WARRIORS') {
				$record['solution_owner'] = 'Inbox Warriors';
			} elseif (strtoupper($record['profile_type']) == 'PROFUSION') {
				$record['solution_owner'] = 'Profusion';			
			} elseif (strtoupper($record['profile_type']) == 'BLACKSUN') {
				$record['solution_owner'] = 'Black Sun';
			} elseif (strtoupper($record['profile_type']) == 'BLUETOAD') {
				$record['solution_owner'] = 'Bluetoad';
			} elseif (strtoupper($record['profile_type']) == 'ELOCATION') {
				$record['solution_owner'] = 'Elocation';
			} elseif (strtoupper($record['profile_type']) == 'RESPONSE_GENIUS') {
				$record['solution_owner'] = 'Response Genius';
			} else {
				$record['solution_owner'] = 'Instiller';			
			}

			# Add updated profile records to array
			$profiles_data['profiles_list'][] = $record;
		}
		
		$profiles_data['filter'] = $filter;
		$profiles_data['filter_description'] = $filter_description;

		# Return profile list
		return $profiles_data;
	}

	function get_all_profiles_and_logins() {
		die('here');
		# Get all Freshdesk logins records
		$freshdesk_logins = $this->get_all_freshdesk_logins();

		# Build the query to search
		$sql = " SELECT * FROM profiles ORDER BY profile_name ";
		
		$records = $this->execsql($sql);

		# Initialise profile list
		$profile_list = array();
		
		while (($record = $records->FetchRow()) !== false) {
			# Get all logins
			$reporting = load_model('reporting');
			$sql = $reporting->get_agency_logins_sql();

			$connection = $this->select_db_connection($record);
			$connection->SetFetchMode(ADODB_FETCH_ASSOC);

			# Load the results from this database
			$run_sql = str_replace('CONTROL_PANEL_URL',$record['profile_control_panel_url'], $sql);
			$login_records = $connection->execute($run_sql);

			if($login_records === false) {
				neteffekt_error_handler('SQL Error',$connection->errorMsg());
			}

			while (($login = $login_records->FetchRow()) !== false) {

				if ($login['company_users_type'] != 'NETEFFEKT') {

					$sql = "SELECT * FROM agency_contacts " .
					   "WHERE agency_contact_email = " . $this->quote($login['company_users_email']) . " AND " .
					   "profile_id = " . $this->quote($record['profile_id']);

					$a = $this->execsql($sql);
					$b = array();
					while (($aa = $a->FetchRow()) !== false) {
						$b[]=$aa;
					}
					
					# Initialise contact
					$contact = array();

					$contact['profile_id'] = $record['profile_id'];
					$contact['company_users_email'] = trim($login['company_users_email']);
					$contact['company_users_name'] = $login['company_users_name'];
					$contact['campaign_event_triggered'] = 'N';

					if (isset($freshdesk_logins[trim($login['company_users_email'])]['freshdesk_id'])) {
						$contact['freshdesk_id'] = $freshdesk_logins[trim($login['company_users_email'])]['freshdesk_id'];
						$contact['freshdesk_customer_id'] = $freshdesk_logins[trim($login['company_users_email'])]['customer_id'];
					} else {
						$contact['freshdesk_id'] = NULL;
						$contact['freshdesk_customer_id'] = $record['profile_freshdesk_custid'];
					}

					if (count($b) < 1) {
						$this->add_new_agency_contact($contact);
					} else {
						$contact['agency_contact_id'] = $b[0]['agency_contact_id'];
						$this->update_agency_contact($contact);
					}

					$record['login_list'][] = $login;
				}
				
			}
			# Add updated profile records to array
			$profile_list[] = $record;
		}
		debug($profile_list);
		# Return profile list
		return $profile_list;
	}

	
	/**
	 * This function will load the properties of the selected profile into the object
	 *
	 * @param int $profile_id
	 * @return boolean
	 */
	function load_profile($profile_id) 
	{
		# Handle creating a shell for new records
		if ($profile_id == 'new') {
			return true;
		}

		# Get all Freshdesk logins records
		$freshdesk_logins = $this->get_all_freshdesk_logins();

		# Get all logins for this profile
		$agency_contacts = $this->get_agency_contacts($profile_id);
		
		# Query the database for the record
		$sql = "SELECT * FROM profiles " .
		       "WHERE profile_id = " . $profile_id;

		# check to make sure a record could be found
		$records = $this->execsql($sql);

		if (($record = $records->fetchRow()) !== false) {
			#debug($record);
			$reporting = load_model('reporting');
			$sql = $reporting->get_agency_logins_sql();

			$connection = $this->select_db_connection($record);
			$connection->SetFetchMode(ADODB_FETCH_ASSOC);

			# Load the results from this database
			$run_sql = str_replace('CONTROL_PANEL_URL',$record['profile_control_panel_url'], $sql);
			$login_records = $connection->execute($run_sql);

			if($login_records === false) {
				neteffekt_error_handler('SQL Error',$connection->errorMsg());
			}
			
			# Process agency logins
			while (($login = $login_records->FetchRow()) !== false) {
				# Make sure that adding to the array isn't going to cause any memory problems
				# If it will, then die with an error
				if(memory_get_usage(true) >= $this->return_bytes(ini_get('memory_limit')) - $this->return_bytes("1M")) {
					neteffekt_error_handler('SQL Error', 'Processing SQL results close to exceeding memory limit.');
				}
				
				if ($login['company_users_type'] != 'NETEFFEKT') {

					# Build up $login details
					$login['profile_id'] = $record['profile_id'];

					# Check for if login is in Freshdesk
					if (isset($freshdesk_logins[trim($login['company_users_email'])])) {
						$login['freshdesk_id'] = $freshdesk_logins[trim($login['company_users_email'])]['freshdesk_id'];
						$login['freshdesk_login_created'] = 'Y';
						
						if ($freshdesk_logins[trim($login['company_users_email'])]['deleted'] == TRUE) $login['freshdesk_login_created'] = 'N';
						
						$login['active'] = $freshdesk_logins[trim($login['company_users_email'])]['active'];
						$login['deleted'] = $freshdesk_logins[trim($login['company_users_email'])]['deleted'];

						if ($login['deleted'] == 'TRUE') $login['agency_custid'] = $record['profile_freshdesk_custid'];
						else $login['agency_custid'] = $freshdesk_logins[trim($login['company_users_email'])]['customer_id'];

					} else {
						$login['freshdesk_id'] = NULL;
						$login['freshdesk_login_created'] = 'N';
						$login['agency_custid'] = $record['profile_freshdesk_custid'];
						$login['active'] = FALSE;
						$login['deleted'] = FALSE;
					}
					# Define login status 
					if (!is_null($login['freshdesk_id'])) {
						if ($login['deleted'] == TRUE) {
							$login['id_status'] = 'Deleted';
						} else {
							if ($login['active'] == TRUE) $login['id_status'] = 'Verified';
							else $login['id_status'] = 'Unverified';
						}
					} else {
						$login['id_status'] = 'Unknown';
					}

					# Check if login is in the eyetap database 
					if (isset($agency_contacts[trim($login['company_users_email'])])) {
						$login['campaign_event_triggered'] = $agency_contacts[trim($login['company_users_email'])]['agency_contact_campaign_event_triggered'];
						$login['agency_contact_id'] = $agency_contacts[trim($login['company_users_email'])]['agency_contact_id'];
					} else {
						$login['campaign_event_triggered'] = 'N';
						
						# Add new contact in database and get contact id for the UI
						$login['agency_contact_id'] = $this->add_new_agency_contact($login);
						
						# Trigger email campaign for new logins
						if ($this->trigger_instiller_email_campaigns($login['company_users_email'],'agency',$login['agency_contact_id'])) $login['campaign_event_triggered'] = 'Y';
					}
					$record['agency_logins'][trim($login['company_users_email'])] = $login;
				}
				
			}
			# Load the properties of the object
			$this->load_from_buffer($record);
			return true;
		}
	}

	/**
	 * This function will retrieve all agency contact stored in the database
	 *
	 * @return freshdesk logins
	 */
	function get_agency_contacts($profile_id)
	{
		# Initilise contacts
		$contacts = array();

		# Query the database for the records
		$sql = "SELECT * FROM agency_contacts " .
		       "WHERE profile_id = " . $profile_id;

		$records = $this->execsql($sql);

		while (($contact = $records->FetchRow()) !== false) {
			$contacts[trim($contact['agency_contact_email'])] = $contact;
		}

		# Return all contacts belonging to this profile
		return $contacts;

	}


	/**
	 * This function will retrieves all Freshdesk logins including verified, unverified and deleted logins
	 *
	 * @return freshdesk logins
	 */
	function get_all_freshdesk_logins()
	{
		# Initialise logins array
		$logins = array();

		# Initialise Freshdesk credentials
		$email = FRESHDESK_LOGIN_EMAIL;
		$password = FRESHDESK_LOGIN_PASSWORD;
		
		$header[] = "Content-type: multipart/form-data";
		$state = "all";
		
		# Get all verified and unverified logins
		for ($i = 1; ; $i++) {
		     
			# Construct url to retrieve logins per page
			# Freshdesk limit each query to 50 results per page
			# Freshdesk have no option to override the 50 result limits - used loop to work around this limitation.
			$url = 'https://neteffekt.freshdesk.com/contacts.json?state=' . $state . '&page=' . $i;
			
			# Initilise and make API call
			$ch = curl_init ($url);

			curl_setopt($ch, CURLOPT_HTTPGET, true);
			 
			curl_setopt($ch, CURLOPT_USERPWD, "$email:$password");
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_HEADER, false);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			
			$server_output = curl_exec ($ch);
			$response = json_decode($server_output);
			
			# Process response for for display in UI
			foreach ($response as $login) {

				# Initialise fd login for processing
				$fd_login = array();

				$fd_login['email'] = trim($login->user->email);
				$fd_login['name'] = $login->user->name;
				$fd_login['freshdesk_id'] = $login->user->id;
				$fd_login['customer_id'] = $login->user->customer_id;
				$fd_login['active'] = $login->user->active;
				$fd_login['deleted'] = $login->user->deleted;
				$fd_login['created'] = $login->user->created_at;
				$fd_login['updated'] = $login->user->updated_at;
				
				$logins[$fd_login['email']] = $fd_login;
			}
			
		   	# This conditions are set to retrieve active logins (verified/unverified) first 
		   	# and then the deleted ones
		   	if ($state == 'deleted' && count($response) < 50) {
		    	break;
		    }
		    if ($state == 'all' && count($response) < 50) {
		    	# Restarting the loop to retrieve deleted logins
		    	$i = 0;
		        $state = "deleted";
		    }
		}
		
		# Return processed logins
		return $logins;
	}

	
	/**
	 * This function will load the properties of the selected profile into the object
	 *
	 * @param int $profile_id
	 * @return boolean
	 */
	function load_agency_contact($agency_contact_id) {
		# Handle creating a shell for new records
		if ($agency_contact_id == 'new') {
			return true;
		}

		# Query the database for the record
		$sql = "SELECT * FROM agency_contacts " .
		       "WHERE agency_contact_id = " . $agency_contact_id;

		# check to make sure a record could be found
		$records = $this->execsql($sql);
		$record = $records->fetchRow();

		# Load the properties of the object
		$this->load_from_buffer($record);
		return true;
	}

	/*
	 * Update the direct relationship and enabled status of the agency
	 */
	function set_profile_status($field, $value, $profile_id) {
		$sql = "UPDATE profiles SET " . $field . " = " . $this->quote($value) . " WHERE " .
			   "profile_id = " . $this->quote($profile_id);
		$this->execsql($sql);
		return true;
	}

	/*
	 * Add new contact to database
	 */
	function add_new_agency_contact($new_contact) {

		# Insert record
		$sql =  " INSERT INTO agency_contacts" . 
				" (profile_id, agency_contact_email, agency_contact_name, agency_contact_campaign_event_triggered, freshdesk_id, freshdesk_customer_id) VALUES (" .
		       	$this->quote($new_contact['profile_id']) . "," . 
		      	$this->quote($new_contact['company_users_email']) . "," . 
		      	$this->quote($new_contact['company_users_name']) . "," .
		      	$this->quote($new_contact['campaign_event_triggered']) . "," . 
		       	$this->quote($new_contact['freshdesk_id']) . "," . 
		      	$this->quote($new_contact['agency_custid']) . ")"; 
		
		$this->execsql($sql);
		
		return $this->insert_id();
	}

	/*
	 * Add update contact to database
	 */
	function update_agency_contact($contact) {

		# Update record
		$sql =  " UPDATE agency_contacts SET " . 
				" profile_id = " . $this->quote($contact['profile_id']) . "," . 
		      	" agency_contact_email = " . $this->quote($contact['company_users_email']) . "," . 
		      	" agency_contact_name = " . $this->quote($contact['company_users_name']) . "," .
		      	" agency_contact_campaign_event_triggered = " . $this->quote($contact['campaign_event_triggered']) . "," . 
		       	" freshdesk_id = " . $this->quote($contact['freshdesk_id']) . "," . 
		      	" freshdesk_customer_id = " . $this->quote($contact['freshdesk_customer_id']) . 
		      	" WHERE agency_contact_id = " . $this->quote($contact['agency_contact_id']); 

		$this->execsql($sql);
		
		return true;
	}

	/*
	 * Function to save agency contact
	 *
	 * @return new record
	 */
	function save_agency_contact()
	{
		# Check if contact already exists within the agency
		$sql = "SELECT COUNT(*) AS counter FROM agency_contacts " .
			   "WHERE agency_contact_email = " . $this->quote(trim($this->variables['agency_contact_email'])) . " AND " .
			   "profile_id = " . $this->quote($this->variables['profile_id']);
		
		$records = $this->execsql($sql);
		$record = $records->FetchRow();

		if ($record['counter'] > 0) {
			return false;
		} 

		# Initialise new agency contact
		$new_contact = array();

		$new_contact['company_users_email'] = $this->variables['agency_contact_email'];
		$new_contact['company_users_name'] = $this->variables['agency_contact_name'];
		$new_contact['profile_id'] = $this->variables['profile_id'];
		$new_contact['campaign_event_triggered'] = 'N';

		# Check if contact exists for this agency logins
		if (!isset($this->instiller_logins[trim($this->variables['agency_contact_email'])]) || is_null($this->instiller_logins[trim($this->variables['agency_contact_email'])])) {
			$new_contact['agency_login_created'] = 'N';
		} else {
			$new_contact['agency_login_created'] = 'Y';
		}

		# Check if user is in freshdesk 
		if (!$new_contact['freshdesk_id'] = $this->in_freshdesk('contacts', trim($this->variables['agency_contact_email']), 'email')) {
			$new_contact['freshdesk_id'] = -1;
			$new_contact['freshdesk_login_created'] = 'N';
		} else {
			$new_contact['freshdesk_login_created'] = 'Y';
		}

		if (isset($this->variables['trigger_campaign_event']) && $this->variables['trigger_campaign_event'] = 'Y') {
			
			$settings = load_model('email_event_settings');
			$email_event_settings = $settings->get_current_settings();

			# Trigger the event to send email to user
			$rest_details = $settings->get_instiller_api_keys($email_event_settings['email_event_settings_solution']);

			# Define the fields required for the rest request
			$fields = array();
			$fields['api_id'] = $rest_details['rest_api_id'];
			$fields['api_key'] = $rest_details['rest_api_key'];
			$fields['event_api_identifier'] = $email_event_settings['email_event_settings_api_identifier'];
			$fields['email_address'] = trim($this->variables['agency_contact_email']);
			$url = $rest_details['profile_control_panel_url'] . "/rest/users/trigger_event";

			# Make the request to the API
			$results = $settings->call_API('GET',$url, $fields);

			if ($results['valid'] === true) {
				$new_contact['campaign_event_triggered'] = 'Y';
			} 
		} 

		$this->login_list[trim($this->variables['agency_contact_email'])] = $this->add_new_agency_contact($new_contact);	

		return true;
	}	

	
	function trigger_instiller_email_campaigns($email,$category,$id)
	{
		$settings = load_model('email_event_settings');
		$email_event_settings = $settings->get_current_settings();

		# Trigger the event to send email to user
		$rest_details = $settings->get_instiller_api_keys($email_event_settings['email_event_settings_solution']);

		# Define the fields required for the rest request
		$fields = array();
		$fields['api_id'] = $rest_details['rest_api_id'];
		$fields['api_key'] = $rest_details['rest_api_key'];
		$fields['event_api_identifier'] = $email_event_settings['email_event_settings_api_identifier'];
		$fields['email_address'] = trim($email);
		$url = $rest_details['profile_control_panel_url'] . "/rest/users/trigger_event";

		# Make the request to the API
		$results = $settings->call_API('GET',$url, $fields);

		if ($results['valid'] === true) {
			if ($category == 'agency') {
				# Update agency contacts
				$sql = " UPDATE agency_contacts SET " .
					   " agency_contact_campaign_event_triggered = 'Y' "  . 
					   " WHERE agency_contact_id = " . $this->quote($id);
				$this->execsql($sql);
			}
		} 
		return true;
	}

	/**
	 * This function will load the buffer from the form submission and then update the
	 * database, a check will be made to ensure the data is unique
	 *
	 * @return unknown
	 */
	function save_profile()
	{
		# Load the properties from the form submission
		$this->load_from_buffer($this->variables);

		$check_id = -1;
		
		# Process if the profile is new
		if ($this->profile_id == 'new') {
			
			$sql = "SELECT COUNT(*) AS counter FROM profiles " .
				   "WHERE profile_name = " . $this->quote($this->profile_name) . " AND " .
				   "profile_id <> " . $check_id;
			
			$records = $this->execsql($sql);
			$record = $records->FetchRow();

			# Check if the profile has a unique name
			if ($record['counter'] > 0) {
				$this->error_message = 'Profile name is already in use, please enter a unique name';
				return false;
			}

			# Initialise profile contacts ingored to N
			$profile_contacts_ignored = 'N';
			
			# Reset value to Y if not in direct relationshionship with new agency
			if ($this->profile_direct_relationship == 'N') $profile_contacts_ignored = 'Y';

			# Add in the new record
			$sql = "INSERT INTO profiles (profile_name, profile_enabled, profile_direct_relationship, " .
			       "profile_database_host, profile_database_name, profile_date_created, profile_control_panel_url, profile_contacts_ignored, profile_type) VALUES (" .
			       $this->quote($this->profile_name) . "," . $this->quote($this->profile_enabled) . "," . $this->quote($this->profile_direct_relationship) . "," .
			       $this->quote($this->profile_database_host) . "," . $this->quote($this->profile_database_name) . "," .
			       "NOW()," . $this->quote($this->profile_control_panel_url) . ", " . $this->quote($profile_contacts_ignored) . ", " . $this->quote($this->profile_type) . ")";
			$this->execsql($sql);
			
			$this->profile_id = $this->insert_id();
			
			# Setup agency in Freshdesk
			if ($this->setup_freshdesk == 'Y' && $this->profile_direct_relationship == 'Y') {
				# Initialise json to attach with API call to Freshdesk
				$jsondata = array (
					'customer[name]' => $this->profile_name
				);
				$this->call_freshdesk_API("customers", "POST", $jsondata);
			}
		} 
		return true;
	}
	
	
	/**
	 * Function to delete a profile after it has been loaded from the buffer
	 *
	 * @return unknown
	 */
	function delete_profile()
	{
		# Double-check the correct information has been assigned to profile_id
		if($this->profile_id == 'new' || trim($this->profile_id == '') || !isset($this->profile_id)) {
			return false;
		}
		
		# Mark as deleted
		$sql = 	" UPDATE profiles SET profile_enabled = 'N' WHERE profile_id = " . $this->quote($this->profile_id);
		$this->execsql($sql);
		
		return true;
	}


	/**
	 * Utility function to create a copy of a profile
	 *
	 * @return boolean
	 */
	function create_copy() {
		# Load the info of the item we're copying
		$this->load_profile($this->variables['profile_id']);

		# make the class think this is a new record
		$this->variables['profile_id'] = 'new';

		# change the name to be a 'copy of...'
		$this->profile_name = 'Copy of ' .  $this->profile_name;

		# save the new record
		return $this->save_profile();
	}

	
	/*
	 * This function will connect to each notifire profile and get a campaign
	 * list using the type parameter
	 */
	function notifire_campaigns($campaign_status = '', $sort_type = 'SOLUTION') {

		$sql = "SELECT a.email_spooler_id AS campaign_id, " .
			   "a.email_spooler_description AS description, a.email_spooler_recipient_count AS recipient_count, " .
			   "a.email_spooler_delivery_date AS delivery_date, a.email_spooler_status AS status, b.company_name FROM " .
			   "email_spooler a, company b, email_spooler_build c WHERE " .
		       "a.email_spooler_message_type = 'CAMPAIGN' AND a.email_spooler_id = c.email_spooler_id and " .
			   "a.email_spooler_test_message = 'FALSE' AND " .
			   "a.company_id = b.company_id AND a.contacts_triggered_event_id IS NULL ";
		
		if ($campaign_status != 'QUEUED') {
			$sql .= " AND email_spooler_status <> 'QUEUED' ";
			
		} else {
			$sql .= " AND email_spooler_status = 'QUEUED' ";
		}
		
		$event_sql = "SELECT a.event_spooler_id AS campaign_id, " .
			   "a.event_spooler_description AS description, a.event_spooler_recipient_count AS recipient_count, " .
			   "a.event_spooler_delivery_date AS delivery_date, a.event_spooler_status AS status, b.company_name FROM " .
			   "event_spooler a, company b WHERE " .
		       "a.event_spooler_message_type = 'CAMPAIGN' AND " .
			   "a.company_id = b.company_id ";
		
		if ($campaign_status != 'QUEUED') {
			$event_sql .= " AND event_spooler_status <> 'QUEUED' AND a.event_spooler_delivery_date <= NOW() ";
			
		} else {
			$event_sql .= " AND (event_spooler_status = 'QUEUED' OR a.event_spooler_delivery_date > NOW()) ";
		}
		
		$sql .= " UNION " . $event_sql;
		
		
		if ($campaign_status != 'QUEUED') {
			$sql .= " ORDER BY delivery_date DESC LIMIT 0,6";
			
		} else {
			$sql .= " ORDER BY delivery_date ASC";
		}		

		$profiles = $this->exec_profile_sql('EMAIL_SOLUTIONS', $sql);
		#debug($profiles);
		if($sort_type == 'SOLUTION')
		{
			foreach($profiles as $profile){
				$total_recipients = 0;
				$profile_type = $this->translate_db_identifier($profile['profile']['profile_type']);
				$profile_name = $profile['profile']['profile_name'];
					
				if ($campaign_status != 'QUEUED') {
					$notifire_campaigns[$profile_type][$profile_name]['profile_name'] = '';				
					$notifire_campaigns[$profile_type][$profile_name]['profile'] = '';					
				}					
				if(isset($profile['data'])){
					foreach($profile['data'] as $campaign){	
						if($campaign['recipient_count'] > 0) {
									
							$notifire_campaigns[$profile_type][$profile_name]['profile_name'] = $profile_name;					
							$notifire_campaigns[$profile_type][$profile_name]['items'][] = array('campaign_client' => $campaign['company_name'],					
																				                   'campaign_date' => $campaign['delivery_date'],
																			                   	   'campaign_description' => $campaign['description'],
																				                   'campaign_recipients' => $campaign['recipient_count']);
							$total_recipients += $campaign['recipient_count'];
						}					
					}
					$notifire_campaigns[$profile_type][$profile_name]['profile'] = $_SESSION['profiles'][$profile['profile']['profile_id']];				
				} else {				
					if ($campaign_status != 'QUEUED') {
						$notifire_campaigns[$profile_type][$profile_name]['profile_name'] = $profile_name;					
						$notifire_campaigns[$profile_type][$profile_name]['profile'] = $_SESSION['profiles'][$profile['profile']['profile_id']];					
					}
				}
				
				if ($campaign_status == 'QUEUED' && $total_recipients > 0) {
					$notifire_campaigns[$profile_type][$profile_name]['queue_recipients'] = number_format($total_recipients, 0, '.', ',');
				}
				
				
			}
			
			# Get a count of the queued event triggered messages
			if ($campaign_status == 'QUEUED') {
				$sql = "SELECT 'Event Triggered Messages' AS description, " . 
					   "SUM(email_spooler_recipient_count) AS recipient_count FROM " .
					   "email_spooler WHERE " .	
					   "email_spooler_message_type = 'TRIGGERED' AND " .
					   "email_spooler_status = 'QUEUED'";
				$profiles = $this->exec_profile_sql('EMAIL_SOLUTIONS', $sql);
				foreach($profiles as $profile){
					$profile_name = $profile['profile']['profile_name'];
					
					if(isset($profile['data'])){
						foreach($profile['data'] as $campaign){		
							if ($campaign['recipient_count'] > 0) {		
								$notifire_campaigns[$profile_type][$profile_name]['profile_name']  = $profile_name;					
								$notifire_campaigns[$profile_type][$profile_name]['event_queue'][] = array('description' => $campaign['description'],					
																						                          'count' => $campaign['recipient_count']);
							}
						}
						if (isset($notifire_campaigns[$profile_name]['profile_name'])) {
							$notifire_campaigns[$profile_type]['profile'] = $_SESSION['profiles'][$profile['profile']['profile_id']];
						}
					}				
				}		
			}
		}
		else
		{
			foreach($profiles as $profile)
			{
				$profile_type = $this->translate_db_identifier($profile['profile']['profile_type']);
				if(isset($profile['data']))
				{
					foreach($profile['data'] as $campaign)
					{
						$notifire_campaigns[$profile_type][$campaign['campaign_id']]['delivery_date'] = $campaign['delivery_date'];
						$notifire_campaigns[$profile_type][$campaign['campaign_id']]['recipient_count'] = $campaign['recipient_count'];
						$notifire_campaigns[$profile_type][$campaign['campaign_id']]['profile_name'] = $profile['profile']['profile_name'];
						$notifire_campaigns[$profile_type][$campaign['campaign_id']]['description'] = $campaign['description'];
						$notifire_campaigns[$profile_type][$campaign['campaign_id']]['profile_control_panel_url'] = $profile['profile']['profile_control_panel_url'];
					}
					
				}
				
				if(isset($notifire_campaigns[$profile_type]))
				{
					if($sort_type == 'RECIPIENTS')
					uasort($notifire_campaigns[$profile_type], 'profiles::campaign_size_compare');
					else
					uasort($notifire_campaigns[$profile_type], 'profiles::campaign_date_compare');
				}
			}
			
		}
		# Show the profiles in alphabetical order
		ksort($notifire_campaigns);
		return $notifire_campaigns;
	}
	
	function campaign_size_compare($a, $b)
	{
		return $b['recipient_count'] - $a['recipient_count'];
	}
	
	function campaign_date_compare($a, $b)
	{
		return strtotime($b['delivery_date']) - strtotime($a['delivery_date']);
	}
	
	/*
	 * This function will connect to each notifire profile and get a summary
	 * for a key number of stats
	 */
	function notifire_stats_summary() {		
		$notifire_stats_summary = array();
		
		$month_1 = 0;
		$month_1_description = '';
		$month_1_label = date('Y-m');
		$month_2 = 0;
		$month_2_description = '';
		$month_2_label = date('Y-m', mktime(0,0,0,date('m')-1,1,date('Y')));		
		$month_3 = 0;
		$month_3_description = '';	
		$month_3_label = date('Y-m', mktime(0,0,0,date('m')-2,1,date('Y')));
		
		$profile_totals = array();
		
		# Get the messages sent
		$sql = "SELECT SUM(company_billing_stats_test_emails + " .
					      "company_billing_stats_email_messages + " .
						  "company_billing_stats_event_emails + " .
						  "company_billing_stats_event_triggers + " .
						  "company_billing_stats_event_forwards + " .
			              "company_billing_stats_api_impressions) AS value, company_billing_stats_period FROM " .
			   "company_billing_stats WHERE " .
		
			   "(company_billing_stats_period LIKE '" . $month_1_label . "%' OR " .
			   " company_billing_stats_period LIKE '" . $month_2_label . "%' OR " .
			   " company_billing_stats_period LIKE '" . $month_3_label . "%') " .						
		
			   "GROUP BY company_billing_stats_period " .		
			   "ORDER BY company_billing_stats_period DESC";
		
		$profiles = $this->exec_profile_sql('EMAIL_SOLUTIONS',$sql);

		foreach($profiles as $profile){
			if(isset($profile['data'])){
				foreach($profile['data'] as $stat){
					# Keep adding to the totals					
					if (strpos($stat['company_billing_stats_period'], $month_1_label) === 0) {
						$month_1 += str_replace(',', '', $stat['value']);						
					} elseif (strpos($stat['company_billing_stats_period'], $month_2_label) === 0) {
						$month_2 += str_replace(',', '', $stat['value']);												
					} elseif (strpos($stat['company_billing_stats_period'], $month_3_label) === 0) {
						$month_3 += str_replace(',', '', $stat['value']);										
					}
				}
				$profile_totals['ACTIVITY_MONTH_1'][$profile['profile']['profile_name']] = $month_1;
				$profile_totals['ACTIVITY_MONTH_2'][$profile['profile']['profile_name']] = $month_2;
				$profile_totals['ACTIVITY_MONTH_3'][$profile['profile']['profile_name']] = $month_3;												
			}		
		}		
		$notifire_stats_summary['ACTIVITY_MONTH_1'] = array('description' => $month_1_label,
													  	    'value' => $month_1);		
		$notifire_stats_summary['ACTIVITY_MONTH_2'] = array('description' => $month_2_label,
													    	'value' => $month_2);		
		$notifire_stats_summary['ACTIVITY_MONTH_3'] = array('description' => $month_3_label,
													     	'value' => $month_3);		
		
		
				
		foreach ($profile_totals as $key => $value) {
			foreach ($value as $subkey => $subvalue) {
				$notifire_stats_summary[$key][$subkey] = $subvalue;
			}
		}

		foreach ($notifire_stats_summary as $key => $value) {
			$notifire_stats_summary[$key] = str_ireplace('sent ', '', $value);
		}
		
		$notifire_stats_summary['ACTIVITY_MONTH_1']['value'] = number_format($notifire_stats_summary['ACTIVITY_MONTH_1']['value'], 0, '.', ',');		
		$notifire_stats_summary['ACTIVITY_MONTH_2']['value'] = number_format($notifire_stats_summary['ACTIVITY_MONTH_2']['value'], 0, '.', ',');		
		$notifire_stats_summary['ACTIVITY_MONTH_3']['value'] = number_format($notifire_stats_summary['ACTIVITY_MONTH_3']['value'], 0, '.', ',');
				
		return $notifire_stats_summary;
	}	
	


	/*
	 * This function will connect to each notifire profile and get the 
	 * a list of imports
	 */
	function notifire_import_log($file_import_upload_status) {
		$sql = "SELECT file_import_reference, file_import_date, " .
			   "file_import_type, file_import_upload_status FROM " .
			   "file_imports WHERE " .
			   "file_import_upload_status = '" . $file_import_upload_status . "' " . 
	   		   "ORDER BY file_import_date DESC";
		$profiles = $this->exec_profile_sql('EMAIL_SOLUTIONS', $sql);
		
		$notifire_import_log = array();
		foreach($profiles as $profile){
			if(isset($profile['data'])){
				foreach($profile['data'] as $import){	
					$notifire_import_log[$import['file_import_reference']] = array('profile_name' => $profile['profile']['profile_name'],
																				   'reference' => $import['file_import_reference'],
																				   'date' => $import['file_import_date'],
																				   'url' => $profile['profile']['profile_control_panel_url'],
																				   'type' => $import['file_import_type'], 
																				   'status' => $import['file_import_upload_status']);
				}
			}
		}
		return $notifire_import_log;
	}	
	

	
}

?>