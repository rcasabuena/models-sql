<?php

class model_base_class extends core_item {
	
	# Orderpad generation properties
	public $error_message;
	public $error_icon;
	
	# Class constructor
	function __construct() {
		# Call the parent constructor, this is VERY IMPORTANT as this will ensure all
		# of the relevant properties and classes are created as part of the core
		# framework, this minimises the code needed at the application level
		parent::__construct ();
	
	}
	
	
	/**
	 * This is a generic load from buffer function which can be used to load the properties of
	 * any descendant class without having to explicitly load each property
	 *
	 * @param array $buffer
	 */
	function load_from_buffer($buffer) {
		# Loop through the properties on the class
		foreach ( $this as $key => $value ) {
			# Match on properties within the input buffer
			if (array_key_exists ( $key, $buffer )) {
				# Load the property
				$this->$key = $buffer[$key];
			}
		
		}
	}
	
	/**
	 * This function will run the specified query for all agencies for a given system type and 
	 * return the results in a muti-dimensional array - the database connections will also be 
	 * preserved and minisied to ensure performance is maintained 
	 *
	 * @param unknown_type $agency_type
	 * @param unknown_type $sql
	 */
	function exec_agency_sql($agency_type, $sql, $read_only = false)
	{		
		$_SESSION['agencies'] = $this->get_agencies();

		# Initialise the results
		$results = array();
				
		# Loop through all of the agencies
		foreach ($_SESSION['agencies'] as $agency) {

			# Ensure we have a connection to the right DB server 
			$connection = $this->select_db_connection($agency, $read_only);
			$connection->SetFetchMode(ADODB_FETCH_ASSOC);

			# Create a new result set to match this agency 
			$result_item = array();
			$result_item['agency'] = $agency;

			# Load the results from this database
			$run_sql = str_replace('CONTROL_PANEL_URL',$agency['agency_control_panel_url'], $sql);
			$records = $connection->execute($run_sql);

			if($records === false) {
				neteffekt_error_handler('SQL Error',$connection->errorMsg());
			}
			$current_row = 0;
			//debug(memory_get_usage()); 
			while (($record = $records->FetchRow()) !== false) {
				// Make sure that adding to the array isn't going to cause any memory problems
				// If it will, then die with an error
				if(memory_get_usage(true) >= $this->return_bytes(ini_get('memory_limit')) - $this->return_bytes("1M")) {
					neteffekt_error_handler('SQL Error', 'Processing SQL results close to exceeding memory limit.');
				}
				$result_item['data'][] = $record;
				$current_row++;	
			}
			# Add the result item into the collectionm 
			$results[$agency['agency_id']] = $result_item;
		}
		
		# Return the results collection
		return $results; 
	}

	/**
	 * This function will run the specified query for all agencies for a given system type and 
	 * return the results in a muti-dimensional array - the database connections will also be 
	 * preserved and minisied to ensure performance is maintained 
	 *
	 * @param unknown_type $agency_type
	 * @param unknown_type $sql
	 */
	function exec_agency_sql2($agencies, $agency_type, $sql, $read_only = false)
	{		
		# Initialise the results
		$results = array();
				
		# Loop through all of the agencies
		foreach ($agencies as $agency) {

			# Ensure we have a connection to the right DB server 
			$connection = $this->select_db_connection($agency, $read_only);
			$connection->SetFetchMode(ADODB_FETCH_ASSOC);

			# Create a new result set to match this agency 
			$result_item = array();
			$result_item['agency'] = $agency;

			# Load the results from this database
			$run_sql = str_replace('CONTROL_PANEL_URL',$agency['agency_control_panel_url'], $sql);
			$records = $connection->execute($run_sql);

			if($records === false) {
				neteffekt_error_handler('SQL Error',$connection->errorMsg());
			}
			
			
			$current_row = 0;
			//debug(memory_get_usage()); 
			while (($record = $records->FetchRow()) !== false) {
				// Make sure that adding to the array isn't going to cause any memory problems
				// If it will, then die with an error
				if(memory_get_usage(true) >= $this->return_bytes(ini_get('memory_limit')) - $this->return_bytes("1M")) {
					neteffekt_error_handler('SQL Error', 'Processing SQL results close to exceeding memory limit.');
				}
				$result_item['data'][] = $record;
				$current_row++;	
			}
			# Add the result item into the collectionm 
			$results[$agency['agency_id']] = $result_item;
		}
		
		# Return the results collection
		return $results; 
	}

	/**
	 * This function will run the specified query for each agency for a given system type and 
	 * return the results in a muti-dimensional array - the database connections will also be 
	 * preserved and minisied to ensure performance is maintained 
	 *
	 * @param unknown_type $agency_type
	 * @param unknown_type $sql
	 */
	function exec_solution_sql($agency_type, $solution, $sql, $read_only = false)
	{		
		$agencies = $this->get_agencies('INSTILLER');

		# Initialise the results
		$results = array();
				
		# Loop through all of the agencies
		foreach ($agencies as $agency) {

			# Ensure we have a connection to the right DB server 
			$connection = $this->select_db_connection($agency, $read_only);
			$connection->SetFetchMode(ADODB_FETCH_ASSOC);
			
			# Create a new result set to match this agency 
			$result_item = array();
			$result_item['agency'] = $agency;

			# Load the results from this database
			$run_sql = str_replace('CONTROL_PANEL_URL',$agency['agency_control_panel_url'], $sql);
			$records = $connection->execute($run_sql);

			if($records === false) {
				neteffekt_error_handler('SQL Error',$connection->errorMsg());
			}
			
			
			$current_row = 0;
			//debug(memory_get_usage()); 
			while (($record = $records->FetchRow()) !== false) {
				// Make sure that adding to the array isn't going to cause any memory problems
				// If it will, then die with an error
				if(memory_get_usage(true) >= $this->return_bytes(ini_get('memory_limit')) - $this->return_bytes("1M")) {
					neteffekt_error_handler('SQL Error', 'Processing SQL results close to exceeding memory limit.');
				}
				$result_item['data'][] = $record;
				$current_row++;	
			}
			# Add the result item into the collectionm 
			$results[$agency['agency_id']] = $result_item;
		}
		
		# Return the results collection
		return $results; 
	}

	/**
	 * This function will return a collection of agency logins  
	 * in a muti-dimensional array 
	 *
	 * @param unknown_type $agency_type
	 * @param unknown_type $sql
	 */
	function get_agency_logins($instance_type, $direct_relationship, $sql, $read_only = false)
	{		
		# Get all agencies
		$agencies = $this->get_agencies($direct_relationship);

		# Initialise the results
		$results = array();

		# Loop through all of the agencies
		foreach ($agencies as $agency) {
			#debug($agency);

			# Ensure we have a connection to the right DB server 
			$connection = $this->select_db_connection($agency, $read_only);
			$connection->SetFetchMode(ADODB_FETCH_ASSOC);
			
			# Create a new result set to match this agency 
			$result_item = array();
			$result_item['agency'] = $agency;

			# Load the results from this database
			$run_sql = str_replace('CONTROL_PANEL_URL', $agency['agency_control_panel_url'], $sql);
			$records = $connection->execute($run_sql);

			if($records === false) {
				neteffekt_error_handler('SQL Error',$connection->errorMsg());
			}
			$current_row = 0;
			//debug(memory_get_usage()); 
			while (($record = $records->FetchRow()) !== false) {
				// Make sure that adding to the array isn't going to cause any memory problems
				// If it will, then die with an error
				if(memory_get_usage(true) >= $this->return_bytes(ini_get('memory_limit')) - $this->return_bytes("1M")) {
					neteffekt_error_handler('SQL Error', 'Processing SQL results close to exceeding memory limit.');
				}
				$result_item['data'][] = $record;
				$current_row++;	
			}
			# Add the result item into the collectionm 
			$results[$agency['agency_id']] = $result_item;
		}

		# Return the results collection
		return $results; 
	}
	
	function return_bytes($val) {
	    $val = trim($val);
	    $last = strtolower($val[strlen($val)-1]);
	    switch($last) {
	        case 'g':
	            $val *= 1024;
	        case 'm':
	            $val *= 1024;
	        case 'k':
	            $val *= 1024;
	    }
	 
	    return $val;
	}
	
	

	/**
	 * Returns an array of agencies which can be used to loop through client systems save hitting the DB
	 * all of the time, this will also allow us to re-use the queries all of the time... 
	 */
	function get_agencies($direct_relationship = '') {
		# Initiaise the agencies collection 
		$agencies = array();
		
		$sql = "SELECT * FROM " .
			   "agencies WHERE " .
			   "agency_status = 'Active' ";
		
		if ($direct_relationship != '') {
			$sql .= " AND agency_direct_relationship = " . $this->quote($direct_relationship);
		}
		
		$sql .= " ORDER BY agency_name";
	
		$records = $this->execsql($sql);
		while (($record = $records->FetchRow()) !== false) {
			# Add the agency into the collection 
			$agencies[$record['agency_id']] = $record;
		}

		# Return the collection of agencies 
		return $agencies;
	}

	/**
	 * This function will open new DB connections as required and will attempt to 
	 * ensure the connection is reused to provide a performance boost
	 *
	 * @param unknown_type $agency
	 * @return unknown
	 */
	function select_db_connection($agency, $read_only = false)
	{
		# Firstly ensure the database connection collection is present
		if (!isset($GLOBALS['DBCONNECTIONS'])) {
			$GLOBALS['DBCONNECTIONS'] = array(); 
		}

		$read_only_suffix = '';
		if($read_only == true) {
			$read_only_suffix = '_readonly';
		}
		
		# Check to see if we already have a connection to the server hosting 
		# this database - if we have we can reuse the connection 
		if (isset($GLOBALS['DBCONNECTIONS'][$agency['agency_database_host'].$read_only_suffix])) {
			# Ensure we use the right database on this host 
			$connection = $GLOBALS['DBCONNECTIONS'][$agency['agency_database_host'].$read_only_suffix]; 
			$connection->execute(" use " . $agency['agency_database_name']);
			return $connection;
		} else {
			# This is the first connection to this server  
			$connection = NewADOConnection(DATABASE_DRIVER);
			$connection->clientFlags = 65536;
			# Connect to the database
			if($read_only == false) {
				if (!$connection->Connect($agency['agency_database_host'], DATABASE_USER, DATABASE_PASSWORD)) {
					# We have been unable to connect to the database we need to handle
					# the error using the the standard error dialog
					//$heading = 'Database Connection Error - ' . $connection->errorNo();
					//$message = $connection->errorMsg();
				}
			} else {
				$connection->Connect($agency['agency_database_host'], EYETAP_READ_ONLY_USER, EYETAP_READ_ONLY_PASSWORD);
			}
			# Ensure we are connected to the right DB
			$connection->execute(" use " . $agency['agency_database_name']);	
			
			// Mark whether the connection is read only or not
			
			$GLOBALS['DBCONNECTIONS'][$agency['agency_database_host'].$read_only_suffix] = $connection;	

			return $connection;
		}
	}
	
	function notifire_api_request($url, $xml) {    
		if (!in_array("curl", get_loaded_extensions())) {
	    	throw new sfException('Could not load cURL libraries. Make sure PHP is compiled with cURL');
		}
				      
		$post_data = 'XMLDATA=' . urlencode(trim($xml));

        $ch = curl_init( );
        curl_setopt( $ch, CURLOPT_URL, $url . '/api.php');
        curl_setopt( $ch, CURLOPT_POST, 1 );
        curl_setopt( $ch, CURLOPT_HEADER, 0 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $post_data );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        
		$xml_response = curl_exec( $ch );
		
		curl_close($ch);

		return $xml_response;
	}
	
	function custom_script_request($url, $variables, $type = 'POST') {
		if (!in_array("curl", get_loaded_extensions())) {
	    	throw new sfException('Could not load cURL libraries. Make sure PHP is compiled with cURL');
		}
		
		if ($type == 'POST') {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, TRUE);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION,TRUE); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_URL, $url);
		 	curl_setopt($ch, CURLOPT_POSTFIELDS, $variables);
		} else {
			# turn the variables into a query string
			$query_string = http_build_query($variables);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_POST, FALSE);
			curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, TRUE);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION,TRUE); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);			
			curl_setopt($ch, CURLOPT_URL, $url . '?' . $query_string);		 			
		}
        
		$script_response = curl_exec( $ch );
		
		curl_close($ch);

		return $script_response;
	}
	
	/*
	 * Translates identifiers used in the database to something nice to display on the screen
	 * 
	 * @return String
	 */
	function translate_db_identifier($identifier) {
		switch($identifier)
		{
			case 'NETEFFEKT_US':
				return 'Response Genius';
			case 'NETEFFEKT':
				return 'Instiller';
				
			# If none of the cases are matched, remove underscores and tidy up the capitalisation
			default:
				$identifier = strtolower($identifier);
				$identifier = str_replace("_"," ",$identifier);
				$identifier = ucwords($identifier);
				return $identifier;
		}
	}

	/*
	 * Translates identifiers used in the database to something nice to display on the screen
	 * 
	 * @return String
	 */
	function translate_db_server($identifier) {
		switch($identifier)
		{
			case 'bunsen.neteffekt.co.uk':
				return 'Bunsen / Beaker';
			
			case 'hogthrob.neteffekt.co.uk':
				return 'Hogthrob / Strangepork';
			
			case 'animal.neteffekt.co.uk':
				return 'Animal / Rowlf';

			case 'statler.neteffekt.co.uk':
				return 'Statler / Waldorf';
		}
	}


	/**
	 * Wrapper function for making cURL requests to REST API
	 * 
	 * @param string $method
	 * @param string $url
	 * @param array $data
	 * @return mixed
	 */
	function call_API($method, $url, $data = false, $response_type = 'application/json')
	{
	    // Initialise the cURL
	    $curl = curl_init();

	    // Initialise the results of the API request 
	    $results = array();
	    $results['valid'] = false;


	    // Select the request method to configure cURL
	    if ($method == 'POST') {
	        curl_setopt($curl, CURLOPT_POST, 1);
	        if ($data) {
	            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	        }
	    } else {
	        // Add any post data to the query string URL
	        if ($data) {
	            $url = sprintf("%s?%s", $url, http_build_query($data));
	        }
	    }

	    // Define the common cURL options
	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($curl, CURLOPT_TIMEOUT, 10); 

	    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 


	    // Make sure the cURL request can be run  
	    if (($rest_response = curl_exec($curl)) === false) {
	        // Return the error message for error handling 
	        $results['http_code'] = false;
	        $results['error'] = curl_error($curl);
	        return $results;
	    } else {
	        // We need to examine the response to see if if succeeded
	        $request_info = curl_getinfo($curl);
	        $results['http_code'] =  $request_info['http_code'];

	        // Decode the JSON response if required 
	        if ($response_type == 'application/json') $results['response'] = json_decode($rest_response, true);
	        else $results['response'] = $rest_response;

	        // Check the response code all valid requests return a 200 
	        if ($results['http_code'] == 200) {
	            // Mark the response as valid
	            $results['valid'] = true;
	        }
	    }

	    // Close the cURL connection
	    curl_close($curl);

	    // Return the API request results
	    return $results;
	}
	

}

?>